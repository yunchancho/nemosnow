#!/bin/bash
set -e 
set -o pipefail

profile_conf_dir="/etc/profile.d"
sysctl_conf_dir="/etc/sysctl.d"
unlimited_conf="core-unlimited.sh"
core_pattern_conf="60-core-pattern.conf"

reboot_need=0

set() {
	if [ -e "$profile_conf_dir/$unlimited_conf" ]; then
		# check existing unlimited conf file is different..
		echo "$unlimited_conf is already exists.."
		cmp --silent "$profile_conf_dir/$unlimited_conf" "$unlimited_conf"
    
    if [ "$?" != 0 ]; then
      reboot_need=1
		  cp $unlimited_conf $profile_conf_dir
    fi
	else 
		reboot_need=1
		cp $unlimited_conf $profile_conf_dir
	fi

	echo $reboot_need

	cp $core_pattern_conf $sysctl_conf_dir
	sysctl --system
}

unset() {
	if [ -e "$profile_conf_dir/$unlimited_conf" ]; then
		rm -rf $profile_conf_dir/$unlimited_conf
		reboot_need=1
	fi
}

die() {
	echo "Usage: $0 [--set|--unset]"
	exit 1
}

if [ -z "$1" ]; then
	die
fi

if [ "$1" == "--set" ]; then
	set
elif [ "$1" == "--unset" ]; then
	unset
else
	die
fi

if [ $reboot_need == 1 ]; then
	echo "reboot"
	#reboot
fi

exit 0
