const { MonitorManager } = require('./monitor-manager')
const { CoredumpManager } = require('./coredump-manager')

let mm = new MonitorManager()
let cd  = new CoredumpManager()
async function coroutine() {
	try {
		await mm.initialize()
		await cd.initialize()

		await mm.run()
		await cd.run()
	} catch (err) {
		console.log(err)
	}
}

coroutine()
