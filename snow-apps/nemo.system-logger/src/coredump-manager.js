const path = require('path')
const { exec } = require('child_process')
const Promise = require('bluebird')
const mongo = require('mongodb');
const fs = Promise.promisifyAll(require('fs'))
const MongoClient = Promise.promisifyAll(mongo).MongoClient;
const meta = require('./meta.json')
const nodePackage = require('../manifest.json')

class CoredumpManager {
	constructor() {
		this._db = null
		this._coredump = false
	}

	initialize() {
		async function coroutine(self) {
			self._db = await MongoClient.connectAsync(meta.nemodbPath)
			let docs = await self._db.collection(nodePackage.pkgname).find({}).toArrayAsync()

			if (docs.length) {
				self._coredump = docs[0].config.coredump? true: false
				console.log(self._coredump)
			}
			
			await new Promise((resolve, reject) => {
				let command = `./${meta.coredumpScriptFile} ${self._coredump? '--set': '--unset'}`
				let cwd = meta.configsDir
				exec(command, { cwd }, (err, stdout, stderr) => {
					if (err) {
						console.log(stderr)
						return resolve(err)
					}
					console.log(stdout)
					resolve(0)
				})
			})

			let swKeyFilePath = path.join(meta.snowSoftwareKeyFile)
			let swKey = await fs.readFileAsync(swKeyFilePath, { encoding: 'utf8' })
			self._swKey = swKey.trim()
		}

		return coroutine(this)
	}

	run() {
		// watch core dump directory, and send them to aws
		// remove old core files (1 week?)
		return Promise.resolve()
	}

}

module.exports = { CoredumpManager }
