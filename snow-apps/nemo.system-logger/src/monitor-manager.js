const http = require('http')
const path = require('path')
const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))
const si = require('systeminformation')
const mongo = require('mongodb');
const MongoClient = Promise.promisifyAll(mongo).MongoClient;

const meta = require('./meta.json')
const nodePackage = require('../manifest.json')

// We will log the follwings
// cpu, gpu, memory, disk, network
const reservedTargets = [
	['cpu', getCpu],
	['gpu', getGpu],
	['memory', getMemory],
	['fs', getFsStats],
	['disk', getDisk],
	['network', getNetwork],
	['temperature', getTemperature]
]

class MonitorManager {
	constructor() {
    this._config = null
		this._timer = null
		this._db = null
		this._swKey = null
	}

	initialize() {
		async function coroutine(self) {
			self._db = await MongoClient.connectAsync(meta.nemodbPath)
			let docs = await self._db.collection(nodePackage.pkgname).find({}).toArrayAsync()

			if (docs.length) {
				self._config = docs[0].config
			} else {
				self._config = {
					interval: 3000,
					targets: {
						cpu: true, gpu: true, memory: true, fs: true, disk: true, network: true, temperature: true
					}
				}
			}
			let swKeyFilePath = path.join(meta.snowSoftwareKeyFile)
			let swKey = await fs.readFileAsync(swKeyFilePath, { encoding: 'utf8' })
			self._swKey = swKey.trim()
			console.log(`${self._swKey} device is monitored`)
		}

		return coroutine(this)
	}

	run() {
		this._timer = setInterval(monitor, this._config.interval, this._swKey, this._config)
    return Promise.resolve()
	}
}

function monitor(swKey, config) {
	let log = { swKey }
	async function coroutine() {
		for (let target of Object.keys(config.targets).filter(key => config.targets[key])) {
			let getter = reservedTargets.filter(rt => rt[0] === target)[0][1]
			let data = await getter()
			log = Object.assign(log, data)
		}

		await writeLog(log)
	}

	return coroutine()
}

function writeLog(log) {
	const options = {
		hostname: '127.0.0.1',
		port: 8888,
		path: `/${nodePackage.pkgname}`,
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		}
	}

	console.log(JSON.stringify(log, null, 2))
	return new Promise((resolve, reject) => {
		let req = http.request(options, (res) => {
			res.on('end', () => {
				console.log('res end..')
				resolve()
			})
		})
		req.on('error', (err) => {
			console.log(`failed to write log: ${err}`)
			reject(err)
		})

		req.write(JSON.stringify(log))
		req.end()
	})
}

function getCpu() {
	async function co() {
		let load = await si.currentLoad()
		let cpu = await si.cpu()
		return {
			cpuAvgLoad: load.avgload,
			cpuCurrentLoad: load.currentload,
			cpuCoreNum: cpu.cores
		}
	}
	return co()
}

function getGpu() {
	return Promise.resolve(1)
}

function getMemory() {
	async function co() {
		let data = await si.mem()
		return {
			memTotal: data.total,
			memUsed: data.used
		}
	}
	return co()
}

function getDisk() {
	async function co() {
		let data = await si.disksIO()
		return {
			diskRioSec: data.rIO_sec,
			diskWioSec: data.wIO_sec
		}
	}
	return co()
}

function getFsStats() {
	async function co() {
		let fsSize = await si.fsSize()
		let fsStats = await si.fsStats()

		let fsTotal = 0
		let fsUsed = 0

		for (let fss of fsSize) {
			fsTotal += fss.size
			fsUsed += fss.used
		}

		return {
			fsTotal, fsUsed,
			fsRxSec: fsStats.rx_sec,
			fsWxSec: fsStats.wx_sec,
		}
	}
	return co()
}

function getNetwork() {
	async function co() {
		let ni = await si.networkInterfaceDefault()
		let data = await si.networkStats(ni)
		return {
			networkRxSec: data.rx_sec,
			networkTxSec: data.tx_sec
		}
	}
	return co()
}

function getTemperature() {
	async function co() {
		let data = await si.cpuTemperature()
		return {
			cpuMainTemp: data.main,
			cpuMaxTemp: data.max
		}
	}
	return co()
}

module.exports = { MonitorManager }
