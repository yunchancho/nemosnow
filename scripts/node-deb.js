#!/usr/bin/node

const fs = require('fs')
const exec = require('child_process').exec
const spawn = require('child_process').spawn

const outputDir = './outs'

let targets = [
	{ 
		name: 'snow-agent',
	  objects: ['node_modules', 'package.json', 'src']
	},
	{ 
		name: 'snow-packages',
	  objects: ['node_modules', 'package.json', 'src']
	},
	{ 
		name: 'snow-contents',
	  objects: ['node_modules', 'package.json', 'src']
	},
	{ 
		name: 'snow-bigdata',
	  objects: ['node_modules', 'package.json', 'src', 'configs']
	},
	{ 
		name: 'snow-localdynamo',
	  objects: ['node_modules', 'package.json', 'src', 'java', 'scripts'],
		scripts: [
	 		{ name:'scripts/prepare-db.sh', args: [] }
		]
	}
]

function spawnProcess(param) {
	return new Promise((resolve, reject) => {
		param.option.stdio = 'inherit'
		let child = spawn(param.command, param.args, param.option)
		child.on('close', (code) => {
			if (code !== 0) {
				reject()
			}
			resolve()
		})
	})
}

function debianize(target) {
	async function co() {
		let cwd = `../snow-cores/${target.name}`

		let param = {}
		param = {
			command: 'npm',
			args: [ 'install' ],
			option: { cwd }
		}
		await spawnProcess(param)

		if (target.scripts) {
			for (let script of target.scripts) {
				param = {
					command: `./${script.name}`,
					args: script.args,
					option: { cwd }
				}
				await spawnProcess(param)
			}
		}
		param = {
			command: 'node-deb',
			args: [ '--', ...target.objects ],
			option: { cwd } 
		}
		await spawnProcess(param)

		param = {
			command: 'mv',
			args: [ `${cwd}/\*.deb`, outputDir ],
			option: { shell: true }
		}
		await spawnProcess(param)
	}

	return co()
}

async function start() {
	try {
		fs.mkdirSync(outputDir)
	} catch (err) {
		//console.log(err)
	}

	if (process.argv[2]) {
		targets = targets.filter(target => target.name.includes(process.argv[2]))
	}

	for (let [index, target] of targets.entries()) {
		try {
			console.log(`[${index}] ${target.name} packaging...`)
			await debianize(target)
		} catch (err) {
			console.log(err.stack)
		}
	}

	console.log(`${targets.length} software of infrastructure are packaged`)
}

// start packaging
start()


