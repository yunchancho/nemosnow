#/bin/bash
 
# This script makes devices run FINE platform on cloud base infrastructure.
# Maintained by Yunchan Cho <yunchan.cho@nemoux.net>

infra_pkgs=('nemosnow-agent' 'nemosnow-backend')
platform_pkgs=(
  'nemoprofile' 'nemocore' 'nemoover' 'nemoapps' 'nemofonts' 'nemo.yoyo' 'nemo.art'
  'nemo.clock' 'nemo.explorer' 'nemo.image' 'nemo.keyboard' 'nemo.pdf' 'nemo.player'
  'nemo.stat' 'nemo.text' 'nemo.weather' 'nemo.filebrowser'
  'nemo.chrome' 'nemo.brave' 'nemo.electron' 'nemo.web-container'
  'nemo.samba' 'nemo.system-logger'
)

nemo_ppa="/etc/apt/sources.list.d/nemo.list"
nemo_aptconf="/etc/apt/apt.conf.d/99nemo"

while getopts "m:h" opt
do
  case $opt in
    m) device_mode=$OPTARG
      ;;
    h) help ;;
    ?) help ;;
  esac
done

if [ "$device_mode" != "local" ]; then
  device_mode="cloud"
fi

print_step() {
  RED='\e[0;31m'
  BLU='\e[0;34m'
  GRN='\e[0;32m'
  BOL='\e[1m'
  BLI='\e[5m'
  NOC='\e[0m'

  if [ "$3" != "noline" ]; then
    echo "--------------------------------------------------------------------------"
  fi

  if [ "$1" == "result" ]; then
    message="${GRN} $2 ${NOC}"
  elif [ "$1" == "error" ]; then
    message="${RED} $2 ${NOC}"
  elif [ "$1" == "title" ]; then
    message="${BOL} $2 ${NOC}"
  else
    message="${BLU} $2 ${NOC}"
  fi
  echo -e "\n$message\n"
}

trim_leading_spaces() {
  if [ "$1" == "file" ]; then
    echo "$(cat $2 | sed 's/^[\t ]*//g')" > "$2"
  else
    echo $(sed 's/^[\t ]*//g' <<< "$2")
  fi
}

check_root() {
  if [ "$EUID" -ne 0 ]; then
    error="
      This script requires superuser access to install nemo essential packages.
      Please run as root.
    "
    print_step "error" "$(trim_leading_spaces "string" "$error")"
    exit -1
  fi
}

initialize() {
  print_step "step" "initialize..."
  # remove existing nemo.list including s3 scheme.
  if [ -e $nemo_ppa ]; then
    rm $nemo_ppa
  fi

  default_aptconf="
    APT::Get::AllowUnauthenticated \"true\";
    Acquire::Languages \"none\";
    Acquire::Check-Valid-Until \"0\";
    Dpkg::Options { \"--force-confnew\"; }
  "
  echo "$default_aptconf" > "$nemo_aptconf"
  trim_leading_spaces "file" "$nemo_aptconf"

  # install s3 transport method
  s3_method_ppa="/etc/apt/sources.list.d/s3-method.list"
  apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 379CE192D401AB61
  echo "deb http://dl.bintray.com/lucidsoftware/apt/ lucid main" > "$s3_method_ppa"
  apt-get update && apt-get install -y apt-boto-s3
  rm "$s3_method_ppa"
}

register_metadata() {
  print_step "step" "register meta data..."
  # set basic metadata
  awsdir="/root/.aws"
  awskeys="credentials"
  awsconf="config"
  default_awskeys="
    [default]
    aws_access_key_id=AKIAJKLPTRHM5R5RUOBA
    aws_secret_access_key=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
  "
  default_awsconf="
    [default]
    region=ap-northeast-2
    output=json
  "
  nemo_list="
    deb [trusted=yes] s3://s3-ap-northeast-2.amazonaws.com/nemosnow-packages system/
    deb [trusted=yes] s3://s3-ap-northeast-2.amazonaws.com/nemosnow-packages shell/
    deb [trusted=yes] s3://s3-ap-northeast-2.amazonaws.com/nemosnow-packages theme/
    deb [trusted=yes] s3://s3-ap-northeast-2.amazonaws.com/nemosnow-packages application/
  "

  mkdir -p "$awsdir"
  echo "$default_awskeys" > "$awsdir/$awskeys"
  echo "$default_awsconf" > "$awsdir/$awsconf"
  echo "$nemo_list" > "$nemo_ppa"

  trim_leading_spaces "file" "$awsdir/$awskeys"
  trim_leading_spaces "file" "$awsdir/$awsconf"
  echo "AWS default keys have been created."

  trim_leading_spaces "file" "$nemo_ppa"
  echo "Special PPA for nemo apt server have been added."
}

install_packages() {
  print_step "step" "install essential packages..."
  apt-get update && apt-get purge --auto-remove -y ${infra_pkgs[@]} ${platform_pkgs[@]} 
  apt-get install -y ${infra_pkgs[@]} ${platform_pkgs[@]} 
  if [ "$?" != 0 ]; then
    print_step "error" "essential packages installation failed..."
    exit -1
  fi
}

install_misc() {
  print_step "step" "install miscellaneous things..."
  bash /usr/share/nemoprofile/install.sh
  if [ "$?" != 0 ]; then
    print_step "error" "miscellaneous things installation failed..."
    exit -1
  fi
  #/usr/share/nemoprofile/select_profile.sh
}

download_contents() {
  # rsync default contents with s3
  s3_contents_src="s3://nemosnow-resources/"
  s3_contents_dest="/opt/contents/default"

  aws s3 sync "$s3_contents_src" "$s3_contents_dest" --delete
}

download_packages() {
  # download all packages in case of local mode
  s3_packages_src="s3://nemosnow-packages"
  s3_packages_dest="/opt/pkgrepo"
  s3_packages_ppas=("system" "shell" "theme" "application")

  cd "$s3_packages_dest"
  for ppa in "${s3_packages_ppas[@]}"; do
    aws s3 cp "$s3_packages_src"/"$ppa" "$s3_packages_dest"/"$ppa" --recursive
    dpkg-scanpackages "$ppa"/ /dev/null | gzip -9c > "$ppa"/Packages.gz
  done
  cd -
}

cleanup() {
  # run this explicitly for unexpected dpkg status
  dpkg --configure -a
}

print_step "title" "Infrastructure Installation for usage of FINE platform" "noline"

# run tasks sequencially
check_root
initialize
register_metadata
install_packages
install_misc
download_contents
if [ "$device_mode" == "local" ]; then
  download_packages
fi
cleanup

result_message="
  Fine platform and infra installation finished!
  You need to reboot system to use it normally.
"
print_step "result" "$(trim_leading_spaces "string" "$result_message")"
