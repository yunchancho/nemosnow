#!/bin/bash
set -e
set -o pipefail

RED='\e[0;31m'
BLU='\e[0;34m'
GRN='\e[0;32m'
BOL='\e[1m'
BLI='\e[5m'
NOC='\e[0m'

help() {
  echo "Usage: $0 [movedir]"
}

die() {
  echo -e "error: $@" >&2
  exit 1
}

#if [ $# -ne 2 ]; then help exit 0; fi
# move passed directory path
if ! [ -z "$1" ]; then
  if [ -e "$1" ]; then
    cd $1
  else 
    die "[$1] directory doesn't exists."
  fi
fi

declare -r pkg_meta_dir="debian"
declare -r pkg_include_list_file="${pkg_meta_dir}/list" 
declare -r manifest_file="manifest.json" 
declare -r outs_dir="outs"
declare -r infrapkg_type="shell" # this could be changed

# All the variables that are injected into templates
system_arch="amd64" # this value should be differentiated by build target
package_name=$(jq -r '.pkgname' $manifest_file)
package_version=$(jq -r '.version' $manifest_file)
package_description=$(jq -r '.description' $manifest_file)
package_maintainer=$(jq -r '.maintainer' $manifest_file)
package_type=$(jq -r '.type' $manifest_file)

if [ $package_type == $infrapkg_type ]; then
  package_workspace="/usr/share"
  package_exec="bin/$package_name"
else 
  package_workspace="/opt/pkgs"
  package_exec="exec"
fi

template_control=$pkg_meta_dir/control
template_postinst=$pkg_meta_dir/postinst
template_postrm=$pkg_meta_dir/postrm
template_prerm=$pkg_meta_dir/prerm
template_environ=$pkg_meta_dir/environ
template_systemd=$pkg_meta_dir/systemd.service
template_array=($template_control $template_postinst $template_postrm $template_prerm $template_environ $template_systemd)

deb_dir="${package_name}_${package_version}_${system_arch}"
deb_debian_dir="$deb_dir/DEBIAN"
deb_systemd_dir="$deb_dir/etc/systemd/system"
deb_environ_dir="$deb_dir/etc/default/"
deb_package_dir="$deb_dir/$package_workspace/$package_name"
deb_infrabin_dir="$deb_dir/usr/bin"

# files included are validated
echo "Checking included files into debian file exists..."
cat $pkg_include_list_file | while read file; do 
if [ "$file" == "" ]; then
  continue
elif ! [ -e "$file" ]; then
  die "${RED}File does not exist: '$file'. Aborting${NOC}"
  #echo -e "${RED}File does not exist: '$file'. Please Check this.${NOC}"
fi
echo "  $file exists."
done

echo "Checking essential information exists..."
if [[ -z "$package_name" || -z "$package_version" || \
  -z "$package_description" || -z "$package_maintainer" ]]; then
die "Essential information is missing..."
fi
if [ -e "$deb_dir" ]; then
  rm -rf "$deb_dir"
fi

mkdir -p "$deb_debian_dir" "$deb_package_dir" "$deb_infrabin_dir"

escape() {
  sed -e 's/[]\/$*.^|[]/\\&/g' -e 's/&/\\&/g' <<< "$@"
}

echo "Rendering debian config files..."
render_template() {
  : ${1:?'Template file was not defined'}
  : ${2:?'Target file was not defined'}
  declare -r source_file="$1"
  declare -r target_file="$2"
  echo "  $source_file => $target_file"
  sed < "$source_file" \
    -e "s/###system_arch###/$(escape $system_arch)/g" \
    -e "s/###package_workspace###/$(escape $package_workspace)/g" \
    -e "s/###package_name###/$package_name/g" \
    -e "s/###package_version###/$package_version/g" \
    -e "s/###package_description###/$package_description/g" \
    -e "s/###package_maintainer###/$package_maintainer/g" \
    > "$target_file"
}

for template in "${template_array[@]}"; do
  if [ "$template" == "$template_control" ] && ! [ -e "$template" ]; then
    die "debian control doesn't exist"
  elif ! [ -e "$template" ]; then
    echo "  $template (x)"
    continue
  fi

  if [ "$template" == "$template_environ" ]; then
    mkdir -p "$deb_environ_dir"
    render_template "$template" "$deb_environ_dir/$package_name"
  elif [ "$template" == "$template_systemd" ]; then
    mkdir -p "$deb_systemd_dir"	
    render_template "$template" "$deb_systemd_dir/$package_name.service"
  else 
    render_template "$template" "$deb_debian_dir/${template##*/}"
  fi
done

echo "Copying original files to debian package directory..."
cat $pkg_include_list_file | while read file; do 
if ! [ -e "$file" ]; then
  continue
fi

echo "  $file => $deb_package_dir/$file"

target_dir="$deb_package_dir"/"$(dirname $file)"
mkdir -p "$target_dir"
cp -r "$file" "$target_dir"
done

chmod -R 0755 "$deb_dir/DEBIAN"

if [ -e "$deb_dir/$package_workspace/$package_name/$package_exec" ]; then
  chmod -R 0755 "$deb_dir/$package_workspace/$package_name/$package_exec"
  if [ $package_type == $infrapkg_type ]; then
    # TODO infrapkg's exec variable is including its upper dirname. It's a little ugly..
    ln -s "$package_workspace/$package_name/$package_exec" "$deb_dir/usr/bin/$package_name"
  fi
fi

if hash md5sum 2>/dev/null; then
  find "$deb_dir" -path "$deb_debian_dir" -prune -o -type f -print0 | xargs -0 md5sum >> "$deb_debian_dir/md5sums"
else 
  die "failed to find md5 checksum program"
fi

echo "Making debian package file for ${package_name}..."
chmod -R 0755 "$deb_dir"
fakeroot dpkg-deb --build "$deb_dir" > '/dev/null'
rm -rf $deb_dir 
if ! [ -z "$1" ]; then
  cd $OLDPWD
fi

mkdir -p $outs_dir
if [ -z "$1" ]; then
  mv ${deb_dir}*.deb $outs_dir
else
  mv $1/${deb_dir}*.deb $outs_dir
fi

echo "finished!"

exit 0
