#!/bin/bash

RED='\e[0;31m'
BLU='\e[0;34m'
GRN='\e[0;32m'
BOL='\e[1m'
BLI='\e[5m'
NOC='\e[0m'

manifest_file="manifest.json"
app_specific_list_file="list.txt"
exec_file="exec"
common_files_path="./template"
copylist=('abc' 'def')

help() {
  echo "usage: $0 -s [script] -d [dir]"
  echo "  -h            help message"
  echo "  -s  SCRIPT    script file path for debian packaging (debianize.sh)"
  echo "  -d  DIR       directory path for packaging (scan recursively base on this dir)"
}

while getopts "s:d:h" opt
do
  case $opt in
    s) debian_script=$OPTARG
      ;;
    d) scan_dir=$OPTARG
      ;;
    h) help ;;
    ?) help ;;
  esac
done

if [[ -z "$debian_script" || -z "$scan_dir" ]]; then
  help
  exit -1
fi

debianize() {
  # install npm dependancies
  if [[ -e "$1/package.json" ]]; then
    cd $1
    npm run build
    if [ $@ != 0 ]; then
      npm install
    fi
    cd -
  fi

  bash $debian_script $1
}

update_package_filelist() {
  if [ ! -d "$1" ]; then
    return -1
  fi

  if [ -e "$1/$app_specific_list_file" ]; then
    cat "$1/$app_specific_list_file" >> "$1/debian/list"
  fi
}

make_package() {
  echo -e "${BLU}make package: \"$1\" has manifest file${NOC}"

  debianize $1
  if [ $? == 0 ]; then
    echo -e "${GRN}succeed to make package: \"$1\"${NOC}"
  else
    echo -e "${RED}failed to make package: \"$1\"${NOC}"
  fi
}

scan() {
  for i in "$1"/*; do
    #echo "$i"
    if [ -d "$i" ]; then
      if [ ! -f "$i"/$manifest_file ]; then
        ls -d $i/ | while read directory
        do
          scan $directory
        done
        continue
      fi
      make_package $i
    elif [[ -f "$i" && "$i" == "$1/$manifest_file" ]]; then
      make_package $1
    fi
  done
}

scan $scan_dir
