#!/bin/bash

help() {
  echo "usage: $0 -l [ppa_path] -m [hash_list]"
  echo "  -h  help message"
  echo "  -l  nemo_ppa_path nemo ppa file's absolute path"
  echo "  -m  md5 hash value list of all nemo indexes on cloud. delimiter is comma"
}

while getopts "l:m:h" opt
do
  case $opt in
    l) ppa_path=$OPTARG
      ;;
    m) hash_list=$OPTARG
      ;;
    h) help ;;
    ?) help ;;
  esac
done

if [[ -z "$ppa_path" || -z "$hash_list" ]]; then
  help
  exit 1
fi

index_cache_dir="/var/lib/apt/lists"

IFS=',' read -ra cur_index_hashes <<< "$hash_list"
echo "${cur_index_hashes[@]}"

while IFS=' ' read -ra ppa; do
  IFS='/' read -ra items <<< ${ppa[1]}

  index_cache_filename="${items[2]}_${items[3]}_${ppa[2]/\//}_Packages"
  echo "start $index_cache_filename"

  index_hash=$(cat "$index_cache_dir/$index_cache_filename" | md5sum | cut -d ' ' -f 1)

  match_flag=0
  for i in "${cur_index_hashes[@]}"; do
    if [ "$i" == "$index_hash" ]; then
      match_flag=1
      continue
    fi
  done

  if [ $match_flag == 0 ]; then
    echo "$index_cache_filename doesn't have matched md5 hash on aws"
    echo "$index_hash"
    exit 1
  fi
done < "$ppa_path"

exit 0
