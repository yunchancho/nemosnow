const { exec } = require('child_process')
const handlerType = "devinfo"

const fnMap = {
  listSounds,
  listTouches,
  listDisplayResolutions,
  listScreenshots,
  listIpaddrs,
  runCustomCommand
}

function listSounds(params) {
  // TODO this is dummy values.
  // We need to implement this as right way.
  return Promise.resolve({
    devices: [ 'builtin', 'sound1', 'sound2' ]
  })
}

function listTouches(params) {
  // TODO this is dummy values.
  // We need to implement this as right way.
  return Promise.resolve({
    devices: [ 'builtin', 'touch1', 'touch2' ]
  })
}

function listDisplayResolutions(params) {
  // TODO this is dummy values.
  // We need to implement this as right way.
  return Promise.resolve({
    devices: [ '1920x1080', '1024x720', '800x600' ]
  })
}

function listIpaddrs(params) {
  return new Promise((resolve, reject) => {
    exec('hostname -I', (err, stdout, stderr) => {
      if (err) {
        return reject({ result: false, ipaddrs: [] })
      }

      resolve({ result: true, ipaddrs: stdout.trim().split(' ') })
    })
  })
}

function runCustomCommand(params) {
  return new Promise((resolve, reject) => {
    exec(params.customCommand, (err, stdout, stderr) => {
      if (err) {
        return reject({ result: false, output: stderr })
      }

      resolve({ result: true, output: stdout })
    })
  })
}

function listScreenshots(params) {
  let shots = [
    {
      thumbnail: {
        x: 0, y: 0,
        width: 192, height: 108,
        url: "s3://nemosnow-backups/<customerId>/<swKey>/screenshots/2017-05-12/thumbnails/screen-0-0_20170412-092011.png"
      },
      screenshot: {
        x: 0, y: 0,
        width: 1920, height: 1080,
        url: "s3://nemosnow-backups/<customerId>/<swKey>/screenshots/2017-05-12/screen-0-0_20170412-092011.png"
      }
    },
    {
      thumbnail: {
        x: 192, y: 0,
        width: 192, height: 108,
        url: "s3://nemosnow-backups/<customerId>/<swKey>/screenshots/2017-05-12/thumbnails/screen-0-1_20170412-092011.png"
      },
      screenshot: {
        x: 1920, y: 0,
        width: 1920, height: 1080,
        url: "s3://nemosnow-backups/<customerId>/<swKey>/screenshots/2017-05-12/screen-0-1_20170412-092011.png"
      }
    }
  ]

  return Promise.resolve(shots)
}

module.exports = (payload) => {
  let co = async function () {
    let response = {}

    try {
      if (payload.type && (payload.type !== handlerType))  {
        return {}
      } else if (!payload.command || !fnMap[payload.command]) {
        return {}
      } else { 
        response = await fnMap[payload.command](payload.params)
      } 
    } catch (err) {
      response.result = false
    }

    response.__reqid__ =  payload.__reqid__
    return response 
  }

  return co()
}
