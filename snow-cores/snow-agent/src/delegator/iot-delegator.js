const co = require("co");
const Promise = require("bluebird");
const EventEmitter = require('events');
const proc = require('child_process');

const devInfoHandler = require('./devinfo-handler')
const powerCtrlHandler = require('./powerctrl-handler')

class IotDelegator extends EventEmitter {
	constructor(thingName) {
		super();
		this._topics = [
			{ 
				sub: thingName + '-devinfo-request',
				pub: thingName + '-devinfo-response',
				handler: devInfoHandler
			},
			{ 
				sub: thingName + '-powerctrl-request',
				pub: thingName + '-powerctrl-response',
				handler: powerCtrlHandler
			}
		];
	}

	get topics() {
		return this._topics;
	}

	applyConfig(delta) {
		let coroutine = co.wrap(function* (self) {
			try {
				// TODO 
				// do something according to delta
				// just for test
				this.emit('PutCollection', '---nemoshell---');
			} catch (err) {
				throw err;
			}
			return true;
		});

		return coroutine(this);
	}

	getReportedConfig(delta) {
		console.log('get reported config');
		const syncCoroutine = co.wrap(function* () {
			let config = {
				state: {
					reported: {}
				}
			};

			try {

				if (delta) {
					// in case of delta handling, remove desired object, updating current config with delta.
					config.state.desired = null;
				}

				// TODO AWS limits state json size to 8KB
				config.state.reported = delta? delta: { "existing": "THIS SHOULD BE FILLED WITH CURRENT REPORT ON REAL DEVICE" }
			} catch (err) {
				console.log("failed to commit device config");
				throw err;
			}
			return config;
		});

		return syncCoroutine();
	}
}

module.exports = {
	IotDelegator
}
