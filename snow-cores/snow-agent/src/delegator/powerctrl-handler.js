const { exec } = require('child_process')
const handlerType = "powerctrl"

const defaultTimeoutMin = 0

const fnMap = {
  powerOn,
  powerOff,
  powerCancel,
  reboot,
  echo 
}

function powerOn(params) {
  return Promise.resolve({ result: true })
}

function powerOff(params) {
  let timeoutMin = params.timeoutMin || defaultTimeoutMin
	return controlPower('--poweroff', timeoutMin)
}

function powerCancel(params) {
  exec(`shutdown -c`)
  return Promise.resolve({ result: true })
}

function reboot(params) {
  let timeoutMin = params.timeoutMin || defaultTimeoutMin
	return controlPower('--reboot', timeoutMin)
}

function echo(params) {
  return Promise.resolve(params)
}

function controlPower(command, timeoutMin) {
	return new Promise((resolve, reject) => {
    console.log('run ', command)
    setTimeout(() => { exec(`shutdown -t ${timeoutMin} ${command}`) }, 1000)
		resolve({ result: true })
	})
}

module.exports = (payload) => {
  let co = async function () {
    let result = {}

    if (payload.type && (payload.type !== handlerType))  {
      return {}
    } else if (!payload.command || !fnMap[payload.command]) {
      return {}
    } else { 
      result = await fnMap[payload.command](payload.params)
    } 

    result.__reqid__ =  payload.__reqid__
    return result
  };

  return co()
}
