const co = require('co')
const fs = require('fs')
const { exec } = require('child_process')

const { ArgsManager } = require('./lib/args-manager')
const { TaskManager } = require("./lib/task-manager")
const { IotDelegator } = require('./delegator/iot-delegator')
const util = require('./lib/util')
const meta = require('./meta.json')

let argsManager = new ArgsManager(process.argv.slice(2))

let timeout = 0
if (!argsManager.initialize()) {
  console.log("Failed to initialize arguments")
  process.exit(1)
}

let args = argsManager.args

co(function* () {
  if (!argsManager.thingName) {
    try {
      let path = `${args.snowDir}/${args.snowSoftwareKeyFile}`
      let swKey = fs.readFileSync(path, { encoding: 'utf8' })
      args.thingName = swKey.trim()

      path = `${args.snowDir}/${args.snowSoftwareModeFile}`
      let swMode = fs.readFileSync(path, { encoding: 'utf8' })
      args.launchType = swMode.trim()
    } catch (err) {
      console.log('s/w key or mode doesn\'t exist', err)
      process.exit(0)
    }
  }

  // clean up some stuffs that causes this agent be blocked or disabled
  yield util.cleanUpInfra()

  try {
    if (args.launchType === 'cloud') {
      yield util.checkInternetAvailable()
    }
    yield util.requestShowSplashscreenToNemoshell({ terminated: false })
  } catch (err) {
    console.log(err)
    let intervalCount = 0
    timeout = setInterval(() => { 
      intervalCount++
      console.log('interval count: ', intervalCount)
      co(function* () {
        yield util.checkInternetAvailable()
        clearInterval(timeout)

        yield util.requestShowSplashscreenToNemoshell({ terminated: false })
        yield runTasks()
      })
      .catch(err => {
        console.log(err)
        if (intervalCount === meta.minishell.cloudWaitCountByThemeShow) {
          util.requestShowThemeToNemoshell({ terminated: false })
        }
      })
    }, meta.minishell.cloudWaitInterval)

    return
  }

  yield runTasks()

}).catch(err => {
  console.log(err)
  util.requestShowThemeToNemoshell({ terminated: false })
})

function runTasks() {
  return (async function run() {
    let delegator = { iot: new IotDelegator(args.thingName), localdb: {}, remotedb: {} }
    let taskManager = new TaskManager(args, delegator)

    await taskManager.initialize()
    const result = await taskManager.run()

    try {
      if (result.restart.system) {
        exec('reboot')
      } else if (result.restart.minishell) {
        exec(`systemctl restart ${meta.minishell.systemdServiceName}`)
      } else {
        await util.requestShowThemeToNemoshell({ terminated: false })
      }
    } catch (err) {
      // Maybe this case would be run on console manually
      // For a while, we will permit this for development
      console.log(err)
    }
  }())
}

process.on('uncaughtException', (err) => {
  console.log('uncaughtException: ', err)
  process.exit(1)
})

process.on('SIGTERM', () => {
  console.log('SIGTERM recieved!')
  process.exit(1)
})

process.on('SIGHUP', () => {
  console.log('SIGHUP recieved!')
  process.exit(1)
})
