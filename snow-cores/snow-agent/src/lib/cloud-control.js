const co = require('co')
const AWS = require('aws-sdk')
const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))
const systeminfo = require('systeminformation')
const util = require('./util')
const meta = require('../meta')

const allowedStatuses = ['activated', 'deactivated']

AWS.config.update({
	region: meta.awsRegion
})

function getAccessSecrets() {
	return co(function* () {
		let swKey = yield fs.readFileAsync(meta.swKeyFilePath, { encoding: 'utf8' })
		swKey = swKey.trim()

		let secrets = yield invokeLambda(meta.lambdaControlDeviceFuncName, { api: 'getAccessSecrets', swKey })
		return secrets
	})
}

function syncTableWithOtherDevices() {
	return co(function* () {
		let swKey = yield fs.readFileAsync(meta.swKeyFilePath, { encoding: 'utf8' })
		swKey = swKey.trim()

		let secrets = yield invokeLambda(meta.lambdaSyncDeviceTableFuncName, { api: 'syncDeviceTable', swKey })
		return secrets
	})
}

function invokeLambda(funcName, deviceInfo) {
	let coroutine = co.wrap(function* () {
    // check internet is available first
    yield util.checkInternetAvailable()
		let hw = yield systeminfo.system()

		deviceInfo.hwKey = hw.uuid
		let context = new Buffer(JSON.stringify(deviceInfo)).toString('base64')
		let params = {
			FunctionName: funcName,
			ClientContext: context
		}

		const lambda = new AWS.Lambda()
		return new Promise((resolve, reject) => {
			lambda.invoke(params, (err, data) => {
				if (err) {
					return reject(err)
				}
				if (!!data.FunctionError) {
					//console.log(`failed to verify device by server error: ${data.Payload}`)
					let payload = JSON.parse(data.Payload)
					return reject(JSON.parse(payload.errorMessage))
				}

				resolve(JSON.parse(data.Payload))
			})
		})
	})

	return coroutine()
}

module.exports = { getAccessSecrets, syncTableWithOtherDevices }
