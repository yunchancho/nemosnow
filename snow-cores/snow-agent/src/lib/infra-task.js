const fs = require('fs')
const path = require('path')
const { spawn, exec } = require('child_process')
const AWS = require('aws-sdk')
const _ = require('underscore')
const Promise = require('bluebird');
const meta = require('../meta.json')

let screenshotTimer = null
let thumbnailDirname = 'thumbnails'

function updatePackages (callerParam) {
	if (callerParam._launchType === 'cloud') {
    AWS.config.update({
      credentials: new AWS.Credentials(
        callerParam._args.secrets.keyId,
        callerParam._args.secrets.accessKey
      ),
      region: callerParam._args.secrets.region
    })
  }
  const dynamodb = Promise.promisifyAll(new AWS.DynamoDB())
  const dynamodoc = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient())

  async function co() {
    // update infra pkgs
		params = {
			TableName: callerParam._args.remoteRegistryTable,
			Key: { 'name': meta.remoteRegistryInfraKeyName }
		}
		doc = await dynamodoc.getAsync(params);
    const infraPkgs = doc.Item.infrapkgs || []
    console.log('infraPkgs: ', infraPkgs)

    let updatedInfraPkgs = await runPackageCLI(callerParam, {
      cmd: '--upgrade',
      arg: infraPkgs.map(i => i.pkgname).join(',')
    })

    // return array may include dependency nemo packages, not infra package
    let bootedPkgs = _.intersection(infraPkgs.filter(i => i.needToReboot).map(i => i.pkgname), updatedInfraPkgs)
    if (bootedPkgs.length) {
      // TODO we need to do something here if needs, before reboot
      // infra pkgs are exception for restart policy, and reboot immediately
      exec('reboot')

      //return { restart: { system: true } }
    }
  }

  return co()
}

function runPackageCLI(self, param, messagecb) {
	return new Promise((resolve, reject) => {
		let spawnArgs = [param.cmd];
		if (param.arg) {
			spawnArgs.push(param.arg);
		}

		let env = Object.assign({}, process.env)
		if (self._launchType === 'cloud') {
			env['AWS_ACCESS_KEY_ID'] = self._args.secrets.keyId
			env['AWS_SECRET_ACCESS_KEY'] = self._args.secrets.accessKey
		}

		let child = spawn(self._args.packageAgentPath, spawnArgs, {
			env: env,
		 	stdio: [ 'inherit', 'inherit', 'inherit', 'ipc' ]
		});

		let result = null;
		child.on('message', (message) => {
			result = message;
			if (messagecb) {
				messagecb(message);
			}
		});
		child.on('exit', (code) => {
			if (code === 0) {
				resolve(result);
			} else {
				reject();
			}
		});
	});
}

function uploadScreenshots (callerParam) {
	if (callerParam._launchType !== 'cloud') {
    return Promise.resolve()
  }

  async function co() {
    const docs = await callerParam._localdb.getCollection(meta.shellCollection)
    let shell = docs[0]

    // TODO check minimum limit e.g. 5000
    // this limit should be read default value of Registry
    if (shell.screenshot) {
      if (!shell.screenshot.interval || shell.screenshot.interval < 5000) {
        return Promise.resolve()
      }
    } else {
      return Promise.resolve()
    }

    if (screenshotTimer) {
      clearInterval(screenshotTimer)
    }

    // get screenshot metadata
    screenshotTimer = setInterval(() => {
      // upload screenshot files to S3
    }, shell.screenshot.interval)
  }

  return co()
}

function sendScreenshotsToS3(srcDir, destBucket, destDirKey) {
  const coroutine = co.wrap(function* () {
    fs.mkdirSync(path.join(srcDir, thumbnailDirname))

  })

  return coroutine()
}

module.exports = { updatePackages, uploadScreenshots }
