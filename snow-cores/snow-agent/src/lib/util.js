const fs = require('fs')
const Promise = require('bluebird')
const dns = Promise.promisifyAll(require('dns'))
const { exec, spawn } = require('child_process')
const { ShellManager } = require('./shell-manager')
const meta = require('../meta')

function checkInternetAvailable() {
  return (async () => {
    try {
		  await dns.resolveAsync(meta.cloudModeUrl) 
    } catch (err) {
      console.log('Internet is not available...')
      return Promise.reject(err)
    }
  })()
}

function notifyReadyToSystemd(param) {
  return new Promise((resolve, reject) => {
    exec(`systemd-notify --ready --status=${param.message}`, (err, stdout, stderr) => {
      if (err) {
        console.log(stderr)
      }
      console.log(stdout)
      let defaultParam = {
        showSplashscreen: false,
        terminated: false
      }
      setTimeout(launchMinishell, 3000, param || defaultParam)
      resolve()
    })
  })
}

function requestShowSplashscreenToNemoshell(param) {
  return (async () => {
    const shellManager = new ShellManager()
    await shellManager.initialize()
    await shellManager.stopTheme()
    await shellManager.stopSplashscreen()
    await shellManager.startSplashscreen()

    if (param.terminated) {
      // we should set exit code to zero to avoid agent respawned by systemd
      process.exit(0)
    }
  })()
}

function requestShowThemeToNemoshell(param) {
  return (async () => {
    const shellManager = new ShellManager()
    await shellManager.initialize()
    await shellManager.stopSplashscreen()
    await shellManager.stopTheme()
    await shellManager.startTheme()

    if (param.terminated) {
      // we should set exit code to zero to avoid agent respawned by systemd
      process.exit(0)
    }
  })()
}

function launchMinishell(param) {
  return (async () => {
    try {
      let isActive = await new Promise((resolve, reject) => {
        let command = `ps -ef | grep ${meta.minishell.systemdServiceName} | grep -v "grep" | wc -l`
        exec(command, (err, stdout, stderr) => {
          if (err) {
            return reject(err)
          }

          resolve(stdout)
        })
      })

      if (!parseInt(isActive)) {
        console.log('Spawn minishell explicitly')
        spawn(meta.minishell.systemdServiceName, [ '-d', 'nemodb', '-c', '_shell_', '-t', '_theme_' ] )
      }

      if (param.showSplashscreen) {
        console.log('show splashscreen in launchminishell!! directly')
        setTimeout(requestShowSplashscreenToNemoshell, 2000, param)
      }
    } catch (err) {
      console.log(err) 
    }
  })()
}

function cleanUpInfra() {
  return (async () => {
    // check mongod.lock
    if(fs.existsSync(meta.mongodLockFilePath)) {
      fs.unlinkSync(meta.mongodLockFilePath)
      await new Promise((resolve) => {
        exec('systemctl start mongodb', () => resolve())
      })
    }

    // remove dpkg lock 
    if(fs.existsSync(meta.aptLockFilePath)) {
      fs.unlinkSync(meta.aptLockFilePath)
    }

    // configure dpkg always
    await new Promise((resolve) => {
      exec('dpkg --configure -a', () => resolve())
    })

    // fix broken packages 
    await new Promise((resolve) => {
      exec('apt-get --fix-broken install', () => resolve())
    })
  })()
}

module.exports = { 
  checkInternetAvailable,
  notifyReadyToSystemd,
  requestShowSplashscreenToNemoshell,
  requestShowThemeToNemoshell,
  launchMinishell,
  cleanUpInfra
}
