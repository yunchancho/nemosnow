const co = require('co');
const Promise = require('bluebird');
const MongoClient = Promise.promisifyAll(require('mongodb')).MongoClient;
const _ = require('underscore');

const reservedCollections = [
	'system.indexes'
];

class LocalDbManager {
	constructor(param) {
		console.log('Local db instance created');
		this._param = param;
	}

	initialize() {
		const initCoroutine = co.wrap(function* (self) {
			try {
				self._db = yield MongoClient.connectAsync(self._param.dbPath);
			} catch (err) {
				throw 'failed to initialize db: ' + err.stack;
			}
			return true;
		});

		return initCoroutine(this);
	}

	listCollections(collectionName) {
		let coroutine = co.wrap(function* (self) {
			let filter = collectionName? { name: collectionName }: {};
			let list = yield self._db.listCollections(filter).toArrayAsync();
			let result = _.map(list, item => item.name)
				.filter(name => !_.contains(reservedCollections, name));
			return result;
		});

		return coroutine(this);
	}

	getCollection(collection) {
		return this._db.collection(collection).find({}).toArrayAsync();
	}

	getAllCollections(selectedCollections) {
		let collections = [];
		let coroutine = co.wrap(function* (self) {
			let list = [];
			if (selectedCollections) {
				list = selectedCollections;
			} else {
				list = yield self.listCollections();
			}
			for (let collection of list) {
        try {
          let docs = yield self.getCollection(collection);
          collections.push({ name: collection, value: docs });
        } catch (err) {
          console.log(err)
        }
			}

			return collections;
		});

		return coroutine(this);
	}

	setCollection(collection, value) {
		let coroutine = co.wrap(function* (self) {
			// remove all docs in this collection
			yield self._db.collection(collection).removeAsync();

			if (Array.isArray(value)) {
				for (let doc of value) {
					yield self._db.collection(collection).saveAsync(doc);
				}
			} else {
				yield self._db.collection(collection).saveAsync(value);
			}
		});

		return coroutine(this);
	}

	dropCollection(collection) {
		return this._db.dropCollectionAsync(collection);
	}

	createCollection(collection) {
		return this._db.createCollectionAsync(collection);
	}
}

module.exports = {
	LocalDbManager
}
