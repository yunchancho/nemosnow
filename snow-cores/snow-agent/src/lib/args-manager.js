const fs = require("fs");
const minimist = require("minimist");
const argSpec = require("./argspec.json");
const path = require("path");

function getDefaultClientId() {
	let id;
	if (process.env.DEVICE_ID) {
		id = process.env.DEVICE_ID.concat(Math.floor((Math.random() * 100000) + 1));
	} else {
		id = 'anonymous' + (Math.floor((Math.random() * 100000) + 1));
	}
	return id;
}

function printHelpMessage(spec) {
	let exec = path.basename(process.argv[1]);
	console.log("Usage: " + exec + " [OPTION...]");
	console.log("\nOptions\n");

	// option description
	for (let arg in spec) {
		let string = " -" + spec[arg].alias[0] + ", --";
		string += spec[arg].alias[1] + "=VALUE\t\t";
		string += spec[arg].description;
		console.log(string);
	}

	console.log("\nDefault Values:\n");
	// default value description
	for (let arg in spec) {
		if (spec[arg].default) {
			let string = " " + spec[arg].alias[1] + "\t\t";
			string += spec[arg].default;
			console.log(string);
		}
	}
	console.log("\n\n");
}

function handleUnknownArgs(spec) {
	console.error("Can't accept unknown arguments");
	printHelpMessage(spec);
	process.exit(1);
}

function getArgAlias(spec) {
	let alias = {};
	for (let arg in spec) {
		if (spec[arg].alias.length) {
			alias[arg] = spec[arg].alias;
		}
	}

	return alias;
}

function getArgDefault(spec) {
	let defaults = {};
	for (let arg in spec) {
		if (spec[arg].default) {
			defaults[arg] = spec[arg].default;
		}
	}

	return defaults;
}

function getArgFormats(spec) {
	let formats = {
		string: [],
		integer: [],
		boolean: []
	};

	for (let arg in spec) {
		let format = spec[arg].format;
		let alias = spec[arg].alias[1];

		if (format == "string") {
			formats.string.push(alias);
		} else if (format == "integer") {
			formats.integer.push(alias);
		} else if (format == "boolean") {
			formats.boolean.push(alias);
		} else {
			console.log(alias + "'s unknown format: " + format);
		}
	}

	return formats;
}

class ArgsManager {
	constructor(argsArr) {
		this._rawArgs = argsArr;
		this._args = {};
	}

	get args() {
		return this._args;
	}	

	set args(args) {
		if (typeof args !== "object") {
			throw new Error("Can't assign non object type to args");
		}
		this._args = args;
	}

	overwriteArgsByConfig(file) {
		if (!fs.existsSync(file)) {
			console.error('\n' + file + ' doesn\'t exist\n');
			return;
		}
		let config = JSON.parse(fs.readFileSync(file, 'utf8'));

		for (let prop in config) {
			this._args[prop] = config[prop];
		}
	}

	initialize() {
		let formats = getArgFormats(argSpec);
		let alias = getArgAlias(argSpec);
		let defaults = getArgDefault(argSpec);

		try {
			this._args = minimist(this._rawArgs, {
				string: formats.string,
				integer: formats.integer,
				boolean: formats.boolean,
				alias: alias,
				default: defaults,
				unknown: handleUnknownArgs
			});

			if (this._args.configFile) {
				this.overwriteArgsByConfig(this._args.configFile);
			}
		} catch (err) {
			console.log(err);
			return false;
		}

		// in case of help option
		if (this._args.help) {
			printHelpMessage(argSpec);
		}

		return true;
	}
}

module.exports = {
	ArgsManager: ArgsManager
};
