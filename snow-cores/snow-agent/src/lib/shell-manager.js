const net = require('net')
const meta = require('../meta.json')

const defaultSocketOptions = {
  path: meta.minishell.nemobusdSocketPath
}

class ShellManager {
  constructor(socketOptions) {
    this.socket = null
    this.socketOptions = socketOptions || defaultSocketOptions
  }

  initialize() {
    async function co(self) {
      return new Promise((resolve, reject) => {
        self.socket = net.connect(self.socketOptions)
        self.socket.on('connect', () => {
          console.log('connected to nemoshell')
          resolve()
        })
        self.socket.on('error', (error) => {
          console.log(error)
        })
        self.socket.on('end', () => console.log('disconnected from nemoshell'))
      })
    }
    return co(this)
  }

  startSplashscreen() {
    const param = {
      service: meta.minishell.splashscreenServiceName,
      request: meta.minishell.startRequest
    }
    console.log('start splash screen')
    return sendMessageToNemobusd(this.socket, param)
  }

  stopSplashscreen() {
    const param = {
      service: meta.minishell.splashscreenServiceName,
      request: meta.minishell.stopRequest
    }
    console.log('stop splash screen')
    return sendMessageToNemobusd(this.socket, param)
  }

  startTheme() {
    return (async function run(self) {
      const daemonParam = {
        service: meta.minishell.daemonServiceName,
        request: meta.minishell.startRequest
      }
      const backgroundParam = {
        service: meta.minishell.backgroundServiceName,
        request: meta.minishell.startRequest
      }

      await sendMessageToNemobusd(self.socket, daemonParam)
      await sendMessageToNemobusd(self.socket, backgroundParam)
      console.log('start theme')
    }(this))
  }

  stopTheme() {
    return (async function run(self) {
      const daemonParam = {
        service: meta.minishell.daemonServiceName,
        request: meta.minishell.stopRequest
      }
      const backgroundParam = {
        service: meta.minishell.backgroundServiceName,
        request: meta.minishell.stopRequest
      }

      await sendMessageToNemobusd(self.socket, daemonParam)
      await sendMessageToNemobusd(self.socket, backgroundParam)
      console.log('stop theme')
    }(this))
  }
}

function sendMessageToNemobusd(socket, param) {
  let payload = {
    from: meta.minishell.targetName,
    to: meta.minishell.targetName,
    command: {
      type: meta.minishell.requestCommandType,
      group: param.service,
      request: param.request,
    }
  }

  return new Promise((resolve, reject) => {
    socket.write(JSON.stringify(payload), () => {
      resolve()
    })
  })
}

module.exports = { ShellManager }
