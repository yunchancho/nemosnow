const path = require('path');
const co = require("co");
const Promise = require("bluebird");
const { exec } = require('child_process')
const { IotManager } = require('./iot-manager');
const { LocalDbManager } = require('./localdb-manager');
const { RemoteDbManager } = require('./remotedb-manager');
const basicTasks = require('./basic-tasks');
const infraTask = require('./infra-task')
const cronTask = require('./cron-task')
const cloudControl = require('./cloud-control')
const util = require('./util')
const meta = require('../meta.json')

// if you have special task for device, add here
class TaskManager {
	constructor(args, delegator) {
		this._args = args;
		this._launchType = args.launchType;
		this._delegator = delegator;
		this._tasks = [
      { name: 'Sync Package Indexes', func: basicTasks.syncPackageIndexes, param: { path: args.packageAgentPath } },
      { name: 'Sync Infra Packages', func: infraTask.updatePackages },
      { name: 'Sync Manifests', func: basicTasks.syncManifests, param: { path: args.packageAgentPath } },
      { name: 'Sync Collections', func: basicTasks.syncCollections, param: { path: args.packageAgentPath } },
      { name: 'Sync General Packages', func: basicTasks.syncPackages, param: { path: args.packageAgentPath } },
      { name: 'Sync Contents', func: basicTasks.syncContents, param: { path: args.contentAgentPath } },
      { name: 'Sync Bigdata', func: basicTasks.syncBigdata, param: { path: args.bigdataAgentPath } },
      { name: 'Cron PowerControl', func: cronTask.startPowerJobs }
		];
	}

	initialize() {
		let coroutine = co.wrap(function* (self) {
			let ret;
			try {
				try {
					if (self._args.launchType === 'cloud') {
						let secrets = yield cloudControl.getAccessSecrets()
						//console.log('cloud secret: ', secrets)
						self._args.secrets = secrets

						self._iot = new IotManager(createIotParam(self._args, self._delegator.iot));
						yield self._iot.initialize();
						registerDelegatorEventListeners(self);
					}
				} catch (err) {
					throw `Cloud mode is not allowed (${err.code})`
				}

				let initParam = {
					localdb: createLocalDbParam(self._args, self._delegator.localdb),	
					remotedb: createRemoteDbParam(self._args, self._delegator.remotedb)
				}
				self._localdb = new LocalDbManager(initParam.localdb);
				self._remotedb = new RemoteDbManager(initParam.remotedb);

				yield self._localdb.initialize();
				yield self._remotedb.initialize();
			} catch (err) {
				throw `fail to initialize: ${err}`;
			}

			return ret; 
		});

		return coroutine(this);
	}

	run() {
		let coroutine = co.wrap(function *(self) {
      let ret = {
        restart: {
          system: false,
          minishell: false
        }
      }
			//TODO how to handle tasks each other with dependancy 
			//we should run tasks sequencely? 
			for (let task of self._tasks) {
				let result = yield task.func(self, task.param)
        if (result && result.restart) {
          if (result.restart.system) {
            ret.restart.system = true
          }
          if (result.restart.minishell) {
            ret.restart.minishell = true
          }
        }
          
				console.log(`${task.name} task finished`)
			}

      return ret
		});

		return coroutine(this);
	}
}

function registerDelegatorEventListeners(self) {
	const delegator = self._delegator;
  /*
   * We can add custome event of delegate iot object
   *
	delegator.iot.on('CustomEvent', (args) => {
		console.log('CustomEvent: ', args);
	});

  */
}

function createIotParam(args, delegator) {

	let param = {
		thingName: args.thingName,
		option: {
			/* 
       * certificates base declaration
       *
			keyPath: path.join(args.awsIotDir, args.iotPrivateKey),
			certPath: path.join(args.awsIotDir, args.iotClientCert),
			caPath: path.join(args.awsIotDir, args.iotRootCA),
			*/
			clientId: `${args.thingName}_${args.clientIdPostfix}`,
			host: args.secrets.iot.host,
			region: args.secrets.iot.region,
			keepalive: args.secrets.iot.keepAlive,
			protocol: 'wss',
			accessKeyId: args.secrets.keyId,
			secretKey: args.secrets.accessKey
		},
		topics: []
	}

	for (let topic of delegator.topics) {
		param.topics.push({
			sub: topic.sub,
		 	pub: topic.pub,
		 	handler: topic.handler
		});
	}

	param.applyDelegator = delegator.applyConfig;
	param.reportDelegator = delegator.getReportedConfig;

	return param;
}

function createLocalDbParam(args, delegator) {
	let param = {
		thingName: args.thingName,
		dbPath: args.dbPath
	}

	return param;
}

function createRemoteDbParam(args, delegator) {
	let param = {
		thingName: args.thingName,
		region: args.awsRegion,
		endpoint: args.awsLocalEndpoint,
		launchType: args.launchType,
		secrets: args.secrets,
		localDynamoAgent: args.localDynamoAgentPath,
		localDynamoAgentPort: args.localDynamoAgentPort
	}

	return param;
}

module.exports = {
	TaskManager: TaskManager
}
