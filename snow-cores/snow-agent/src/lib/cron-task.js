const Promise = require('bluebird');
const MongoClient = Promise.promisifyAll(require('mongodb')).MongoClient;
const { exec } = require('child_process')
const { CronJob } = require('cron')
const { LocalDbManager } = require('./localdb-manager')
const moment = require('moment')
const meta = require('../meta.json')

let cronJob = {
  powers: []
}

const commandMap = { 
  powerOff, reboot, test
}

const days = [
  'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'
]
  
function powerOff() {
  console.log('start to power off')
	exec('shutdown --poweroff -t now')
}

function reboot() {
  console.log('start to reboot')
	exec('shutdown --reboot -t now')
}

function test() {
  console.log('--------------')
  console.log('cron schdule expired!')
  console.log('--------------')
}

function setCron(schedule) {
  if (!schedule.day && !schedule.date) {
    return false
  }

  let type = schedule.date? 'date': 'day'
  let jobs = []

  if(type === 'day' && days.indexOf(schedule.day.toLowerCase()) < 0) {
    return false
  }
  for (let key in schedule) {
    if (!Object.keys(commandMap).includes(key)) {
      continue
    }

    try {
      let values = [ '00' ] // default seconds
      values = values.concat(schedule[key].split(':').reverse())

      if (type === 'day') {
        values = values.concat([ '*', '*' ])
        values.push(days.indexOf(schedule.day.toLowerCase()))
      } else if (type === 'date') {
        values = values.concat(schedule.date.split(/-|_|\./).slice(1, 3).reverse())
        values.push('*')
      }

      // set cron style string: e.g.'00 30 11 * * 2' 
      const cronTime = values.join(' ')
      const job = new CronJob({
        cronTime,
        onTick: commandMap[key],
        start: false,
        //timeZone: ''
      })
      
      console.log(`${key} cronTime: ${cronTime}`)
      jobs.push(job)
    } catch (err) {
      console.log(err)
      continue
    }
  }

  return jobs
}

function startPowerJobs(callerParam, schedules) {
  async function co() {
    if (!schedules || !schedules.length) {
			const db = await MongoClient.connectAsync(meta.nemodbPath);
      schedules = await db.collection(meta.powerCollection).find({}).toArrayAsync()
    }

    stopPowerJobs()


    // preprocess days for high priority date
    let dates = schedules.filter(s => s.date && moment(s.date).isSame(moment(), 'day'))
    console.log('dates: ', dates)
    let today = moment().format('dddd')
    console.log('today: ', today)
    schedules = schedules.filter(s => {
      if (s.day) {
        if (!moment(s.date).isSame(moment(), 'day')) {
          delete s.date
        }
        if (dates.length) {
          return s.day.toLowerCase() !== today.toLowerCase()
        }
        return true
      }

      return moment(s.date).isSame(moment(), 'day')
    })
    console.log('changed schedules by date: ', schedules)

    for (let schedule of schedules) {
      let jobs = setCron(schedule)
      cronJob.powers = [ ...cronJob.powers, ...jobs ]
    }

    //console.log('cron jobs: ', JSON.stringify(cronJob.powers))
    console.log('cron jobs nums: ', cronJob.powers.length)
    for (let [ index, job ] of cronJob.powers.entries()) {
      job.start()
      console.log(`job${index} status: ${job.running}`)
    }
  }

  return co()
}

function stopPowerJobs() {
  for (let job of cronJob.powers) {
    job.stop()
  }
  cronJob.powers = []
  /* 
   * just for test
   *
  let job1 = new CronJob({
    cronTime: '00 * * * * *',
    onTick: function() {
      console.log('test job ticked');
    },
    start: true,
    //timeZone: 'Asia/Seoul',
  });
  cronJob.powers.push(job1)
  */

  return Promise.resolve()
}

module.exports = { startPowerJobs, stopPowerJobs }
