const childProcess = require('child_process');
const co = require("co");
const Promise = require("bluebird");
const _ = require("underscore");
const { thingShadow } = require("aws-iot-device-sdk");
const nemodbsPath = '/usr/bin/nemodbs';

const operationTimeout = 2000;

// private members
const privateMembers = new WeakMap();

class IotManager {
	constructor(param) {
		privateMembers.set(this, {
			param: param,
			thingShadow: {},
			topics: param.topics,
			thingName: param.thingName,
			reqStack: [],
			timeoutHandle: null
		});
	}

	initialize() {
		let members = privateMembers.get(this);
		const initCoroutine = co.wrap(function* (self) { 
			let result = null;
			try {
				yield _setMembers(self);
				yield _connectToAwsIot(self);
				let result = yield members.param.reportDelegator();
				yield _commitOperationToAws(self, "update", result);
				yield _registerShadowHandlers(self);
			} catch (err) {
				throw "failed to execute generator: " + err.stack;
			}

			return true;
		});

		return initCoroutine(this);
	}

	run() {
		return Promise.resolve();
	}
}

function _setMembers(self)
{
	let members = privateMembers.get(self);

	if (!_createShadow(self)) {
		return Promise.reject("failed to create shadow");
	}
	// TODO if there is delta on init, old config from db will be overwrited on AWS Iot
	// because current config is sent to aws iot in init.
	// so, it's important to register shadow event handler.
	// handlers should be registered after current config is first sent to aws.
	//_registerShadowHandlers(self);

	return Promise.resolve();
}

function _registerShadowHandlers(self)
{
	let members = privateMembers.get(self);
	let shadow = members.thingShadow;
	let name = members.thingName;

  shadow.on("connect", function() {
     console.log("success to connect aws iot");
  });

  shadow.on("close", function() {
     console.log("close");
     shadow.unregister(name);
  });

  shadow.on("reconnect", function() {
     console.log("success to reconnect aws iot");
  });

	shadow.on("offline", function() {
	  if (members.timeoutHandle !== null) {
	 	 clearTimeout(members.timeoutHandle);
	 	 members.timeoutHandle = null;
	  }

	  while (members.reqStack.length) {
	 	 members.reqStack.pop();
	  }
	  console.log("offline");
  });

  shadow.on("error", function(error) {
	  console.log("error: ", error);
  });

  shadow.on("status", function(thingName, stat, token, config) {
		let lastReq = members.reqStack.pop();
		if (lastReq.token === token) {
			console.log(thingName + "'s " + lastReq.op + " stat: " + stat);
		} else {
			console.log(thingName + "'s received token is not mismatched (status)");
		}
		//console.log(JSON.stringify(config, null, '\t'));
  });

	shadow.on('message', function(topic, payload) {
		let found = _.findWhere(members.topics, { sub: topic });
		if (!found) {
			console.log('invalid topic: ', topic);
			return;
		}

		co(function* () {
      console.log('shadow message: ', JSON.parse(payload.toString()))
			let response = yield found.handler(JSON.parse(payload.toString()));
      console.log('response: ', response)
			shadow.publish(found.pub, JSON.stringify(response));
		});
	});

  shadow.on("delta", function(thingName, config) {
		console.log(thingName + "'s delta: ", JSON.stringify(config));
		co(function* () {
			yield members.param.applyDelegator(config.state);
			let result = yield members.param.reportDelegator(config.state);
			yield _commitOperationToAws(self, "update", result);
		})
		.then(() => console.log("success to apply delta to device"))
		.catch(err => console.log("failed to apply delta to device: ", err.stack));
  });

  shadow.on("timeout", function(thingName, clientToken) {
		let lastReq = members.reqStack.pop();
		if (lastReq.token === token) {
			console.log(thingName + "'s " + lastReq.op + " request is timeout");
		} else {
			console.log(thingName + "'s received token is not mismatched (timeout)");
		}
		members.param.reportDelegator();
  });

	return Promise.resolve();
}

function _createShadow(self)
{
	let members = privateMembers.get(self);
	try {
		members.thingShadow = thingShadow(members.param.option);
	} catch (err) {
		console.log("failed to make aws thingshadow: ", err.stack);
		return false;
	}

	return true;
}

function _connectToAwsIot(self)
{
	let members = privateMembers.get(self);

	return new Promise(function (resolve, reject) {
		members.thingShadow.register(members.thingName, { 
			operationTimeout: operationTimeout
		}, function (err, failedTopics) {
			if (!err && !failedTopics) {
				console.log("thing registered as aws iot");
				// subscribe specific custom topics
				for (let topic of members.topics) {
					members.thingShadow.subscribe(topic.sub);
				}
				resolve();
			} else {
				reject(err);
			}
		});
	});
}

function _commitOperationToAws(self, operation, data)
{
	let members = privateMembers.get(self);
	let token = members.thingShadow[operation](members.thingName, data);

	if (token === null) {
		if (members.timeoutHandle !== null) {
			console.log("other operation not finished. This will be retried.. ");
			members.timeoutHandle = setTimeout(() =>
				 	_commitOperationToAws(self, operation, data),
				 operationTimeout * 2);
		}	
	} else {
		members.reqStack.push({ op: operation, token: token });
	}

	return Promise.resolve();
}

module.exports = {
	IotManager: IotManager
}

