function callTypeHandler(source, target, fn) {
  if (typeof source !== typeof target) {
    console.log('source and target are different type..')
    return false
  }

  let result = false
  const type = typeof source

  //console.log(`source type: ${type}, value: ${JSON.stringify(source)}`)

  if (type == 'array') {
    result = diffArray(source, target)
  } else if (type == 'object') {
    if (Array.isArray(source)) {
      result = diffArray(source, target)
    } else {
      result = diffObject(source, target)
    }
  } else {
    result = (source === target)
  }

  if (result && fn) {
    console.log('fn: ', fn)
    // additionally we need to check layer sequence is same 
    try {
      const fnResult = fn(source, target)
      if ('boolean' === typeof fnResult) {
        result = fnResult
      } 
    } catch (err) {
      console.log(err)
    }
  }

  return result
}

function diffObject(source, target) {
  if (typeof source !== 'object' && typeof source !== 'object') {
    return false
  }

  for (let key in source) {
    if (!target.hasOwnProperty(key)) {
      return false
    }
    if (!callTypeHandler(source[key], target[key])) {
      return false
    }
  }
  return true
}

function diffArray(source, target) {
  // this function doesn't care order of array which is different with exisiting openesource lib
  // Order check is not needed to nemosnow, but this option would be added later for general use
  if (!Array.isArray(source) || !Array.isArray(target)) {
    console.log('all args are not array type')
    return false
  }

  if (source.length !== target.length) {
    console.log('two array length is different')
    return false
  }

  //copy target array because it may be changed
  let cloneTarget = Object.assign([], target)
  for (let i of source) {
    let matched = false
    for (let j of cloneTarget) {
      if (typeof i !== typeof j) {
        continue
      }
      if (callTypeHandler(i, j)) {
        //console.log(`-- matched ${i} ${j}`)
        matched = true
        cloneTarget.splice(cloneTarget.indexOf(j), 1)
        break 
      }
    }
    if (!matched) {
      return false
    }
  }

  return true
}

function deepEqual(source, target, fn, option = {}) {
  return callTypeHandler(source, target, fn)
}

module.exports = deepEqual
