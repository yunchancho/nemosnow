const co = require("co");
const Promise = require("bluebird");
const fs = Promise.promisifyAll(require('fs'))
const AWS = require('aws-sdk');
const exec = require('child_process').exec
const meta = require('../meta.json')

const batchWriteLimit = 25

class RemoteDbManager {
	constructor(param) {
		this._param = param;
		this._table = param.thingName;
		this._projection = {
			attrs: ['#n', '#v'],
			name: { '#n': 'name', '#v': 'value' }
		}

	}
	initialize() {
		let type = this._param.launchType
		let self = this
		return co(function* () {
			if (type === 'cloud') {
				console.log('cloud mode')
				yield self.initializeCloudMode()
			} else {
				console.log('local mode')
				yield self.initializeLocalMode()
			}
		})
	}

	initializeLocalMode() {
		AWS.config.update({
			region: this._param.region,
			endpoint: this._param.endpoint
		});
		this.dynamodb = Promise.promisifyAll(new AWS.DynamoDB());
		this.dynamodoc = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient());

		const coroutine = co.wrap(function* (self) {
			// in case of local mode, make sure that local dynamodb server is running 
			yield new Promise((resolve, reject) => {
				let command = `${self._param.localDynamoAgent}`
				command += ` --start --port ${self._param.localDynamoAgentPort}`
				exec(command, (err, stdout, stderr) => {
					if (err) {
						return reject(err)
					}
					resolve(stdout)
				})
			})

			try {
				yield self.dynamodb.describeTableAsync({ TableName: self._table });
			} catch (err) {
				yield new Promise((resolve, reject) => {
					let command = `${self._param.localDynamoAgent}`
					command += ` --reset`
				 	command += ` --port ${self._param.localDynamoAgentPort}`
				 	command += ` --sw-key ${self._table}`
					exec(command, (err, stdout, stderr) => {
						if (err) {
							return reject(err)
						}
						resolve(stdout)
					})
				})
			}
			console.log('local remotedb initialized');
		})

		return coroutine(this);
	}

	initializeCloudMode() {
		AWS.config.update({
			credentials: new AWS.Credentials(this._param.secrets.keyId, this._param.secrets.accessKey),
			region: this._param.secrets.region
		});

		this.dynamodb = Promise.promisifyAll(new AWS.DynamoDB());
		this.dynamodoc = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient());

		const coroutine = co.wrap(function* (self) {
			try {
				yield self.dynamodb.describeTableAsync({ TableName: self._table });
			} catch (err) {
				console.log(err)
				throw `${self._table} device is invalid for cloud`;
			}
			console.log('cloud remotedb initialized');
			return true;
		});

		return coroutine(this);
	}

	getAll(option) {
		const param = { 
			TableName: this._table,
			ProjectionExpression: this._projection.attrs.join(','),
			ExpressionAttributeNames: this._projection.name
		};
		if (option && option.exceptList) {
			param.ScanFilter = {
				name: {
					ComparisonOperator: 'NE',
					AttributeValueList: option.exceptList
				}
			};
		}

		let coroutine = co.wrap(function* (self) {
			let docs = [];
			try {
				let result = yield self.dynamodoc.scanAsync(param);
				//console.log(`dynamodb result: ${JSON.stringify(result, null, 2)}`);

				for (let doc of result.Items) {
					docs.push(doc);
				}
			} catch (err) {
				throw err;
			}

			return docs;
		});


		return coroutine(this);
	}

	get(collection) {
		const param = { 
			TableName: this._table,
		 	Key: {
			 	name: collection
		 	},
			ProjectionExpression: this._projection.attrs.join(','),
			ExpressionAttributeNames: this._projection.name
		}

		let coroutine = co.wrap(function* (self) {
			let result;
			try {
				result = yield self.dynamodoc.getAsync(param);
			} catch (err) {
				throw err;
			}
      console.log(collection, 'remote get: ', result)

			return result.Item.value
		});

		return coroutine(this);
	}

	setAll(collectionObjs) {
		let coroutine = co.wrap(function* (self) {
      if (!collectionObjs.length) {
        return 'none'
      }

      // collectionObj must have 'name', 'value' attribute and their value
      let param = {
        RequestItems: {
        }
      }

      param.RequestItems[self._table] = [];
      for (let [ index, collection ] of collectionObjs.entries()) {
        let request = {
          PutRequest: {
            Item: collection
          }
        }

        if (index && (index % batchWriteLimit === 0)) {
          yield self.dynamodoc.batchWriteAsync(param);
          param.RequestItems[self._table] = []
        }
        param.RequestItems[self._table].push(request);
      }

      if (param.RequestItems[self._table].length) {
        // we need to run this as default
        yield self.dynamodoc.batchWriteAsync(param);
      }
    })

    return coroutine(this)
	}

	set(collection, docs) {
		const param = {
			TableName: this._table,
			Item: {
				name: collection,
				value: docs 
			}
		}

		let coroutine = co.wrap(function* (self) {
			let ret;
			try {
				ret = yield self.dynamodoc.putAsync(param);
			} catch (err) {
				throw err;
			}

			return ret; 
		});

		return coroutine(this);
	}

	unset(collection) {
		const param = { 
			TableName: this._table,
		 	Key: {
			 	name: collection
		 	}
		}

		let coroutine = co.wrap(function* (self) {
			let ret;
			try {
				ret = yield self.dynamodoc.deleteAsync(param);
			} catch (err) {
				throw err;
			}

			return ret; 
		});

		return coroutine(this);
	}

	unsetAll(collectionObjs) {
		let coroutine = co.wrap(function* (self) {
      if (collectionObjs && !collectionObjs.length) {
        return Promise.resolve()
      }

			let currentItems = yield self.getAll()
			if (!collectionObjs) {
				collectionObjs = currentItems
			}

			let param = {
				RequestItems: {
				}
			}

			param.RequestItems[self._table] = [];
			for (let [ index, collection ] of collectionObjs.entries()) {
        // we need to handle in case that removed obj doesn't exist already in dynamo
        if (!currentItems.map(i => i.name).includes(collection.name)) {
          continue
        }
				let request = {
					DeleteRequest: {
						Key: {
							name: collection.name
						}
					}
				}

        if (index && (index % batchWriteLimit === 0)) {
			    yield self.dynamodoc.batchWriteAsync(param);
          param.RequestItems[self._table] = []
        }
				param.RequestItems[self._table].push(request);
			}

      if (param.RequestItems[self._table].length) {
        // we need to run this as default
        yield self.dynamodoc.batchWriteAsync(param);
      }
		})

		return coroutine(this)
	}

	isExisted() {
		const param = { TableName: this._table }
		return dynamodb.describeTableAsync(param);
	}

  getInfraRegistry() {
    const param = {
      TableName: meta.remoteRegistryTableName,
      Key: {
        name: meta.remoteRegistryInfraKeyName
      }
    }

    let coroutine = co.wrap(function* (self) {
      let result;
      try {
        result = yield self.dynamodoc.getAsync(param);
      } catch (err) {
        throw err;
      }
      return result.Item
    });

    return coroutine(this);
  }
}

module.exports = {
	RemoteDbManager: RemoteDbManager
}
