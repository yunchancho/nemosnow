const co = require("co")
const _ = require('underscore')
const { spawn, exec } = require('child_process')
const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))
const AWS = require('aws-sdk')
const path = require('path')
const deepMerge = require('deepmerge')
const cloudControl = require('./cloud-control')
const deepEqual = require('./deep-equal')
const meta = require('../meta.json')
const defaultIotKeepAlive = 30

// these task functions are placed here just for seperating files
function syncManifests(self, param) {
	if (self._launchType === 'cloud') {
    AWS.config.update({
      credentials: new AWS.Credentials(
        self._args.secrets.keyId,
        self._args.secrets.accessKey
      ),
      region: self._args.secrets.region
    })
  }
	let dynamodb = Promise.promisifyAll(new AWS.DynamoDB());
	let dynamodoc = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient());

	let coroutine = co.wrap(function* () {
		const params = {
			TableName: self._args.remoteRegistryTable,
			Key: { 'name': meta.remoteRegistryDeviceKeyName }
		};
		let registry = yield dynamodoc.getAsync(params);

		if (!fs.existsSync(self._args.snowDir)) {
			fs.mkdirSync(self._args.snowDir);
		}

		let filename = path.join(self._args.snowDir, self._args.snowRegistryDefaultFile);
		let filevalue = JSON.stringify(registry.Item.defaultValue, null, 2);
		console.log(`registry device info: ${filevalue}`);

		yield fs.writeFileAsync(filename, filevalue, { encoding: 'utf8' });
		yield requestPackageTask(self, { agent: param.path, cmd: '--sync-manifests' });

	});

	return coroutine();
}

function restoreReservedConfigsFromUsb(self, param) {
	let coroutine = co.wrap(function* () {
		let collections = yield runPackageAgentRestoreUsbConfigs(self, param)
		for (let collection of collections) {
      if (!meta.reservedInfraCollections.includes(collection.name)) {
        continue
      }

			try {
				yield self._remotedb.unset(collection.name)
			} catch (err) {
				console.log(err)
			}
			yield self._remotedb.set(collection.name, collection.value)
		}
	})

	return coroutine()
}

function syncCollections(self, param) {
	console.log('sync collections');
	const localdb = self._localdb;
	const remotedb = self._remotedb;

	let coroutine = co.wrap(function* () {
		if (self._launchType === 'local') {
			try {
				const	deviceSettingPath = path.join(self._args.snowDir, self._args.snowDeviceSettingFile);
				if (fs.existsSync(deviceSettingPath)) {
					let deviceSetting = yield getDeviceSetting(self)
					if (deviceSetting.apis.restore.runOnBoot.config) {
						console.log('restore reserved configs from usb')
						try {
							yield restoreReservedConfigsFromUsb(self, param);
						} catch (err) {
							console.log(err)
						}
					}
				}
			} catch (err) {
				console.log('usb configs sync error: ', err)
			}
		}

    // Handling mandatory packages 
		// write local's collection on remote db newly
    let infraRegistry = yield remotedb.getInfraRegistry()
    let deviceCommonMandatoryPkgs = infraRegistry.mandatorypkgs || []
		let deviceSpecificMandatoryPkgs = yield remotedb.get(meta.mandatoryPkgsCollection)
    let remoteMandatoryPkgNames = [...deviceCommonMandatoryPkgs, ...deviceSpecificMandatoryPkgs].map(item => item.pkgname)
    remoteMandatoryPkgNames = [ ...new Set(remoteMandatoryPkgNames) ]

    // copy local platform pkg's manifest to remotedb
    let remoteList = yield remotedb.get(self._args.installedPkgsCollection)
    let localList = yield localdb.getCollection(self._args.installedPkgsCollection)

    let addList = localList.filter(doc =>  {
      if (remoteList.map(rd => rd.pkgname).includes(doc.pkgname) || !remoteMandatoryPkgNames.includes(doc.pkgname)) {
        return false
      }
      return true
    }).map(doc => _.omit(doc, ['_id']))

    if (addList.length) {
      yield remotedb.unset(self._args.installedPkgsCollection)
      yield remotedb.set(self._args.installedPkgsCollection, [...remoteList, ...addList])
      console.log('synced added pkgs: ', addList.map(doc => doc.pkgname))

      let platformCollections = yield localdb.getAllCollections(addList.map(doc => doc.pkgname))
      platformCollections = platformCollections.map(doc => {
        doc.value[0] = _.omit(doc.value[0], ['_id'])
        doc.value = doc.value[0]
        return doc
      })
			yield remotedb.setAll(platformCollections);

      // in only cloud case, this works
      if (self._launchType === 'cloud') {
        yield cloudControl.syncTableWithOtherDevices()
      }
    }

		let localCollectionNames = yield localdb.listCollections();
		let remoteCollections = yield remotedb.getAll();
		let remoteCollectionNames = _.map(remoteCollections, item => item.name);
		let onlyLocals = _.difference(localCollectionNames, remoteCollectionNames);
		let onlyRemotes = _.difference(remoteCollectionNames, localCollectionNames);
		let commons = _.intersection(localCollectionNames, remoteCollectionNames);

		console.log('onlyLocals: ', onlyLocals);
		console.log('onlyRemotes: ', onlyRemotes);
		console.log('sameNames: ', commons);


    // we need to store cureent shell, theme before they are overwritten by remote value
    // They will be used for checking if minishell should be restarted
		const curShell = yield localdb.getCollection(meta.shellCollection)
		const curTheme = yield localdb.getCollection(meta.themeCollection)

		// overwrite remote db's collection on local db
		for (let collection of commons) {
			let found = _.findWhere(remoteCollections, { name: collection });
			yield localdb.setCollection(collection, found.value);
		}

		// create newly collections which only exists on remotedb
		for (let collection of onlyRemotes) {
			let found = _.findWhere(remoteCollections, { name: collection });
			yield localdb.setCollection(collection, found.value);
		}
    
    // check shell, theme default config is changed for restart minishell
    function isConfigEqual(current, next, fn) {
      if (current.useDefaultId !== next.useDefaultId) {
        console.log('default config id is different...', current.useDefaultId, next.useDefaultId)
        return false
      }
      let result = false 

      try {
        result = deepEqual(
          _.findWhere(current.configs, { id: current.useDefaultId }),
          _.findWhere(next.configs, { id: next.useDefaultId }),
          fn
        )
      } catch (err) {
        console.log(err)
      }

      return result
    }

    const remoteShell = _.findWhere(remoteCollections, { name: meta.shellCollection }).value
    const remoteTheme = _.findWhere(remoteCollections, { name: meta.themeCollection }).value
    const isShellEqual = isConfigEqual(curShell[0], remoteShell)
    const isThemeEqual = isConfigEqual(curTheme[0], remoteTheme, (s, t) => {
      // additionally we need to check layer sequence is same 
      let result = true
      try {
        for (let i = 0; i < s.layers.length; i++) {
          console.log(s.layers[i].id, t.layers[i].id)
          if (s.layers[i].id !== t.layers[i].id) {
            result = false
            break;
          }
        }
      } catch (err) {
        console.log(err)
        result = false
      }

      return result
    })


    const needToRestartMinishell = !isShellEqual || !isThemeEqual

    console.log('shell default config is equal: ', isShellEqual)
    console.log('theme default config is equal: ', isThemeEqual)

    return { restart: { minishell: needToRestartMinishell} }
	});
	
	return coroutine();
}

function removePackages(self, param) {
	let coroutine = co.wrap(function* () {
		let registeredPackages = yield self._localdb.getCollection(self._args.installedPkgsCollection);
		let registeredPackageNames = registeredPackages.map(doc => doc.pkgname)
		let installedPackageNames = [];

		if (fs.existsSync(self._args.pkgBasePathPrefix)) {
			installedPackageNames = yield fs.readdirAsync(self._args.pkgBasePathPrefix);
		}
	  let removeList = _.difference(installedPackageNames, registeredPackageNames)
		let removeCollections = [];
		for (let pkg of removeList) {
      let result = [] 
			try {
				result = yield requestPackageTask(self, { agent: param.path, cmd: '--remove', arg: pkg })
			} catch (e) {
				console.log(`fail to remove ${pkg}`);
				continue;
			}

      for (let pkgname of result) {
			  removeCollections.push({ name: pkgname });
      }
		}

		yield self._remotedb.unsetAll(removeCollections);
    if (self._launchType === 'cloud') {
      yield cloudControl.syncTableWithOtherDevices()
    }
  })

  return coroutine()
}

function syncPackageIndexes(self, param) {
	let coroutine = co.wrap(function* () {
		const	deviceSettingPath = path.join(self._args.snowDir, self._args.snowDeviceSettingFile);
		let deviceSetting = yield getDeviceSetting(self)

		if (self._launchType === 'local') {
			if (deviceSetting.apis.restore.runOnBoot.package) {
				try {
					yield runPackageAgentCopyUsbPackages(self, param);
				} catch (err) {
					console.log(`cannot copy package: ${err}`)
				}

        deviceSetting.apis.restore.runOnBoot.package = false
        yield setDeviceSetting(self, deviceSetting)
			}
		}

		try {
			yield runPackageAgentSyncPPA(self, param);
      if (self._launchType === 'cloud') {
        let result = yield checkPackageIndexUpdate(self)
        if (result.needToUpdate) {
			    yield runPackageAgentUpdateIndex(self, param);
        }
      } else {
			  yield runPackageAgentUpdateIndex(self, param);
      }
		} catch (err) {
			console.log('update index task: ', err)
      process.exit(1)
		}
  })

  return coroutine()
}

function checkPackageIndexUpdate(self) {
  let coroutine = co.wrap(function* () {
    try {
      const params = {
        TableName: meta.remoteRegistryTableName,
        Key: { 'name': meta.remoteRegistryInfraKeyName }
      }
      const dynamodoc = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient());
      const doc = yield dynamodoc.getAsync(params);

      let indexHashes = []
      for (let key in doc.Item.pkgindex.hash.common) {
        indexHashes.push(doc.Item.pkgindex.hash.common[key])
      }

      const result = yield new Promise((resolve, reject) => {
		    const spawnArgs = [
          '-l', self._args.pkgIndexFile,
          '-m', indexHashes.join(',')
        ]

        let command = path.join(path.dirname(process.execPath), `../${meta.dataDirname}/${meta.ppaHashCheckerFileName}`)
        if (!fs.existsSync(command)) {
          // if this program run by node itself, not zeit-pkg
          console.log('this is not pkg executable..')
          command = path.join(__dirname, `../../${meta.dataDirname}/${meta.ppaHashCheckerFileName}`)
        }

        let child = spawn(command, spawnArgs, {
          stdio: [ 'inherit', 'inherit', 'inherit', 'ipc' ]
        });

        let result = null;
        child.on('exit', (code) => {
          if (code === 0) {
            resolve({ needToUpdate: false })
          } else {
            resolve({ needToUpdate: true })
          }
        })
      })
      return result

    } catch (err) {
      console.log(err)
      return { needToUpdate: true }
    }
  })

  return coroutine()
}

function syncPackages(self, param) {
	let coroutine = co.wrap(function* () {
    let result

		try {
			yield runPackageAgentSyncPackages(self, param);
		} catch (err) {
			console.log('sync package task: ', err)
		}

		try {
		  result = yield runPackageAgentUpgradeAll(self, param);
		} catch (err) {
			console.log('upgrade packages: ', err)
		}

    return result
	});

	return coroutine();
}

function runPackageAgentUpgradeAll(self, param) {
	const localdb = self._localdb;
	const remotedb = self._remotedb;

	let coroutine = co.wrap(function* () {
		let upgradedPkgs = yield requestPackageTask(self, {
			agent: param.path,
			cmd: '--upgrade'
		});

    let upgradedCollections = []
		for (let pkg of upgradedPkgs) {
			let docs = yield localdb.getCollection(pkg);
			docs = docs.map(doc => _.omit(doc, ['_id']))
			upgradedCollections.push({ name: pkg, value: docs[0] });
		}

		if (upgradedPkgs.length) {
			try {
				//yield remotedb.unsetAll(upgradedCollections);
				yield remotedb.setAll(upgradedCollections);

				let docs = yield localdb.getCollection(self._args.installedPkgsCollection);
				docs = docs.map(doc => _.omit(doc, ['_id']))
				yield remotedb.set(self._args.installedPkgsCollection, docs);
				console.log('synced docs (for upgrade): ', docs);
			} catch (err) {
				console.log('error occurs during sync installed pkgs db: ', err);
			}

      if (self._launchType === 'cloud') {
        yield cloudControl.syncTableWithOtherDevices()
      }

      // check reboot is needed
      // only mandatory pkgs are checked for this.
      const params = {
        TableName: self._args.remoteRegistryTable,
        Key: { 'name': meta.remoteRegistryInfraKeyName }
      }
      const dynamodoc = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient());
      const doc = yield dynamodoc.getAsync(params);
      const mandatoryPkgs = doc.Item.mandatorypkgs || []
      const rebootPkgs = mandatoryPkgs.filter(i => i.needToReboot).map(i => i.pkgname)

      if (_.intersection(rebootPkgs, upgradedPkgs).length) {
        // TODO we need to do something here if needs, before reboot
        return { restart: { system: true } }
      }
    }
	});

	return coroutine();
}

function runPackageAgentUpdateIndex(self, param) {
	let coroutine = co.wrap(function* () {
		yield requestPackageTask(self, {
			agent: param.path,
			cmd: '--update-index'
		}, (data) => console.log('message from child: ', data), meta.ppaUpdateTimeout);
	});

	return coroutine();
}

function runPackageAgentCopyUsbPackages(self, param) {

	let coroutine = co.wrap(function* () {
		let result = yield requestPackageTask(self, {
			agent: param.path,
			cmd: '--copy-usb-packages'
		}); 
	});

	return coroutine();
}

function runPackageAgentRestoreUsbConfigs(self, param) {

	let coroutine = co.wrap(function* () {
		let result = yield requestPackageTask(self, {
			agent: param.path,
			cmd: '--restore-usb-configs'
		}); 
		return result 
	});

	return coroutine();
}

function runPackageAgentSyncPPA(self, param) {
	const localdb = self._localdb;
	const remotedb = self._remotedb;

	let coroutine = co.wrap(function* () {
		let result = yield requestPackageTask(self, {
			agent: param.path,
			cmd: '--sync-ppa-list'
		}); 

		if (result.needToCheckRemote) {
			let remoteDocs = yield remotedb.get(self._args.pkgIndexCollection);
			if (!remoteDocs.length) {
				//TODO how to handle collection exists in remotedb but dosen't have value
				yield remotedb.set(self._args.pkgIndexCollection, result.value);
			}
		}
	});

	return coroutine();
}

function runPackageAgentSyncPackages(self, param) {
	console.log('-- sync packages --');
	let coroutine = co.wrap(function* () {
		let deviceSetting = yield getDeviceSetting(self)
		let registeredPackages = yield self._localdb.getCollection(self._args.installedPkgsCollection);

    // set mandatory pkgs using Registry common mandatory pkgs and customer specific ones
    let infraRegistry = yield self._remotedb.getInfraRegistry()
    let deviceCommonMandatoryPkgs = infraRegistry.mandatorypkgs || []
		let deviceSpecificMandatoryPkgs = yield self._localdb.getCollection(meta.mandatoryPkgsCollection);
    let mandatoryPackageNames = [...deviceCommonMandatoryPkgs, ...deviceSpecificMandatoryPkgs].map(item => item.pkgname)
    mandatoryPackageNames = [ ...new Set(mandatoryPackageNames) ]

		let registeredPackageNames = registeredPackages.map(doc => doc.pkgname)
		let installedPackageNames = [];

		if (fs.existsSync(self._args.pkgBasePathPrefix)) {
			installedPackageNames = yield fs.readdirAsync(self._args.pkgBasePathPrefix);
		}

		let pkgReq = {
			installList: _.difference(registeredPackageNames, installedPackageNames),
			removeList: _.difference(installedPackageNames, registeredPackageNames),
		}

    // add mandatory pkgs not installed yet
    pkgReq.installList = pkgReq.installList.concat(_.difference(mandatoryPackageNames, registeredPackageNames))

		// check reinstallable packages with only 'pkgname' in registered list
		for(let pkg of registeredPackages.map(pkg => _.omit(pkg, ['_id']))) {
			let keys = Object.keys(pkg)
			if (keys.length === 1 && keys.includes(self._args.installedPkgNameAttr)) {
				pkgReq.removeList.push(pkg[self._args.installedPkgNameAttr])
				pkgReq.installList.push(pkg[self._args.installedPkgNameAttr])
			}
		}

		// check if there are config files existed without indicating pkglist
		// in this case, we need to install packages for the config files
    // TODO we need to check this logic on cloud mode
		if (self._launchType === 'local') {
			if (deviceSetting.apis.restore.runOnBoot.config) {
				let restoreCollections = yield runPackageAgentRestoreUsbConfigs(self, param)
				for (let collection of restoreCollections) {
					if (meta.reservedInfraCollections.includes(collection.name) || 
							pkgReq.installList.includes(collection.name)) {
						continue
					}
					if (registeredPackageNames.includes(collection.name)) {
						continue
					}

					pkgReq.installList.push(collection.name)
				}
			}
		}

		// remove duplicated item in pkgReq array
		pkgReq.removeList = [...new Set(pkgReq.removeList)]
		pkgReq.installList = [...new Set(pkgReq.installList)]
		console.log('requested pkgs: ', pkgReq)

		const localdb = self._localdb;
		const remotedb = self._remotedb;

		let needToSync = false;

		let removeCollections = [];
		for (let pkg of pkgReq.removeList) {
      let result = [] 
			try {
				result = yield requestPackageTask(self, {
					agent: param.path,
					cmd: '--remove',
					arg: pkg
				});
			} catch (e) {
				console.log(`fail to remove ${pkg}`);
				continue;
			}

      for (let pkgname of result) {
			  removeCollections.push({ name: pkgname });
      }

			needToSync = true;
		}

		let installCollections = [];
		for (let pkg of pkgReq.installList) {
      let result = [] 
			try {
				result = yield requestPackageTask(self, {
					agent: param.path,
					cmd: '--install',
					arg: pkg
				});
			} catch (e) {
				console.log(`fail to install ${pkg}`);
				continue;
			}

      for (let pkgname of result) {
        let docs = yield localdb.getCollection(pkgname);
        docs = docs.map(doc => _.omit(doc, ['_id']))
        installCollections.push({ name: pkgname, value: docs[0] });
      }
			needToSync = true;
		}

		// restore data from usb configs if needed
		if (self._launchType === 'local') {
			if (deviceSetting.apis.restore.runOnBoot.config) {
				let restoreCollections = yield runPackageAgentRestoreUsbConfigs(self, param)
				for (let collection of restoreCollections) {
					if (meta.reservedInfraCollections.includes(collection.name)) { 
						continue
					}

          const localCollectionValues = yield localdb.getCollection(collection.name)
          if (!localCollectionValues.length) {
            console.log(`fail to restore ${collection.name} collection because it doesn't exist`)
            continue
          }

          let updatedCollection = { name: collection.name, value: null }
          updatedCollection.value = deepMerge(localCollectionValues[0], collection.value)
          yield localdb.dropCollection(collection.name)
          yield localdb.setCollection(collection.name, updatedCollection.value)

          let index = installCollections.findIndex(item => item.name === collection.name)
          if (index < 0) {
            // in case that restore collection doesn't exist in installed list
            installCollections.push(updatedCollection)
          } else {
            installCollections.splice(index, 1)
            installCollections.splice(index, 0, updatedCollection)
          }
        }
			}
		}

		if (needToSync) {
			try {
				yield remotedb.unsetAll(removeCollections);
				yield remotedb.setAll(installCollections);

				let docs = yield localdb.getCollection(self._args.installedPkgsCollection);
				docs = docs.map(doc => _.omit(doc, ['_id']))
				yield remotedb.set(self._args.installedPkgsCollection, docs);
			} catch (err) {
				console.log('error occurs during sync installed pkgs db: ', err);
			}

      if (self._launchType === 'cloud') {
        yield cloudControl.syncTableWithOtherDevices()
      }
		}

    // This config flag setting is deferred because this task use this value
    // we need to set false on the flag
    deviceSetting.apis.restore.runOnBoot.config = false
    yield setDeviceSetting(self, deviceSetting)

	});

	return coroutine();
}

function requestPackageTask(self, param, messagecb, timeoutMsec) {
	return new Promise((resolve, reject) => {
    let timerHandle = null
		let spawnArgs = [param.cmd];
		if (param.arg) {
			spawnArgs.push(param.arg);
		}

		let env = Object.assign(process.env, { 
			'NEMO_PKG_INDEX_COLLECTION': self._args.pkgIndexCollection,
			'NEMO_PKG_INDEX_FILE': self._args.pkgIndexFile,
			'NEMO_INSTALLED_PKGS_COLLECTION': self._args.installedPkgsCollection,
			'NEMO_PKG_BASE_PATH_PREFIX': self._args.pkgBasePathPrefix,
			'NEMO_DB_PATH': self._args.dbPath,
			'NEMO_LAUNCH_TYPE': self._launchType
		})

		if (self._launchType === 'cloud') {
			env['AWS_ACCESS_KEY_ID'] = self._args.secrets.keyId
			env['AWS_SECRET_ACCESS_KEY'] = self._args.secrets.accessKey
		}

		let child = spawn(param.agent, spawnArgs, {
			env: env,
		 	stdio: [ 'inherit', 'inherit', 'inherit', 'ipc' ]
		});

		let result = null;
		child.on('message', (message) => {
			result = message;
			if (messagecb) {
				messagecb(message);
			}
		});
		child.on('exit', (code) => {
      if (timerHandle) {
        clearTimeout(timerHandle)
      }

			if (code === 0) {
				resolve(result);
			} else {
				reject();
			}
		});

    if (timeoutMsec) {
      timerHandle = setTimeout(() => {
        console.log(`${param.agent} ${param.cmd} timeout...`)
        child.kill('SIGHUP')
        return reject()
      }, timeoutMsec)
    }
	});
}

function syncContents(self, param) {
	let coroutine = co.wrap(function* () {
		try {
			if (self._launchType === 'local') {
				let deviceSetting = yield getDeviceSetting(self)
				if (!deviceSetting.apis.restore.runOnBoot.content) {
					return
				}
			}

			try {
				yield requestMediaTask(self, { agent: param.path, cmd: '--sync' });
			} catch (err) {
				console.log(err)
			}

			if (self._launchType === 'local') {
				let deviceSetting = yield getDeviceSetting(self)
				deviceSetting.apis.restore.runOnBoot.content = false
				yield setDeviceSetting(self, deviceSetting)
			}
		} catch (e) {
			console.log(`fail to sync media: ${e}`);
		}
	});

	return coroutine();
}

function requestMediaTask(self, param) {
	return new Promise((resolve, reject) => {
		let spawnArgs = [param.cmd];
		if (param.arg) {
			spawnArgs.push(param.arg);
		}

		let env = Object.assign(process.env, { 
			'NEMO_DB_PATH': self._args.dbPath,
			'NEMO_CONTENT_SYNC_COLLECTION': self._args.contentSyncCollection,
			'NEMO_LAUNCH_TYPE': self._launchType,
			'NEMO_CONTENT_BASE_PREFIX': self._args.contentBasePrefix
		})

		if (self._launchType === 'cloud') {
			env['AWS_ACCESS_KEY_ID'] = self._args.secrets.keyId
			env['AWS_SECRET_ACCESS_KEY'] = self._args.secrets.accessKey
		}

		var child = spawn(param.agent, spawnArgs, {
			env: env,
		 	stdio: [ 'inherit', 'inherit', 'inherit', 'ipc' ]
		});

		child.on('message', (data) => {
			console.log('data from child: ', data);
		});

		child.on('exit', (code) => {
			if (code === 0) {
				resolve();
			} else {
				reject();
			}
		});
	});
}

function syncBigdata(self, param) {
	let coroutine = co.wrap(function* () {
		yield requestLogTask(self, { agent: param.path, cmd: '--sync-logconfig' });
	});

	return coroutine();
}

function requestLogTask(self, param) {
	return new Promise((resolve, reject) => {
		let spawnArgs = [param.cmd];
		if (param.arg) {
			spawnArgs.push(param.arg);
		}

		let env = Object.assign(process.env, { 
			'NEMO_DB_PATH': self._args.dbPath,
			'NEMO_LAUNCH_TYPE': self._launchType,
			'NEMO_SNOW_SW_KEY_FILE': self._args.snowSoftwareKeyFile,
			'NEMO_BIGDATA_COLLECTION': self._args.bigdataCollection,
			'NEMO_LOG_CONFIG_BASE_PREFIX': self._args.bigdataLogConfigBasePathPrefix,
			'NEMO_LOG_FILE_BASE_PREFIX': self._args.bigdataLogFileBasePathPrefix,
			'NEMO_INSTALLED_PKGS_COLLECTION': self._args.installedPkgsCollection,
			'NEMO_SNOW_DIR': self._args.snowDir,
			'NEMO_SNOW_REGISTRY_DEFAULT_FILE': self._args.snowRegistryDefaultFile
		})

		if (self._launchType === 'cloud') {
			env['AWS_ACCESS_KEY_ID'] = self._args.secrets.keyId
			env['AWS_SECRET_ACCESS_KEY'] = self._args.secrets.accessKey
		}

		var child = spawn(param.agent, spawnArgs, {
			env: env,
		 	stdio: [ 'inherit', 'inherit', 'inherit', 'ipc' ]
		});

		child.on('message', (data) => {
			console.log('data from child: ', data);
		});

		child.on('exit', (code) => {
			if (code === 0) {
				resolve();
			} else {
				reject();
			}
		});
	});
}

function getDeviceSetting(self) {
	return co(function* () {
		const	deviceSettingPath = path.join(self._args.snowDir, self._args.snowDeviceSettingFile);
		const deviceSetting = yield fs.readFileAsync(deviceSettingPath, { encoding: 'utf8' })
		return JSON.parse(deviceSetting)
	})
}

function setDeviceSetting(self, newSetting) {
	return co(function* () {
		let data = JSON.stringify(newSetting, null, 2)
		const	deviceSettingPath = path.join(self._args.snowDir, self._args.snowDeviceSettingFile);
		yield fs.writeFileAsync(deviceSettingPath, data, { encoding: 'utf8' })
	})
}

module.exports = {
	syncManifests, // sync between manifest files and localdb
	syncCollections, // sync between localdb and remotedb
	syncPackageIndexes, // sync package index files
  syncPackages, // sync packages
	syncContents, // sync contents
	syncBigdata // sync bigdata log
}
