const co = require("co");
const { LocalDbManager } = require('./localdb-manager');

let param = {
	dbPath: 'localhost:27017/nemodb'
}

const localdb = new LocalDbManager(param);

co(function* () {
	let result;
	yield localdb.initialize();
	result = yield localdb.listCollections();
	console.log('result: ', result);
	result = yield localdb.getAllCollections();
	console.log('result: ', JSON.stringify(result, null, 2));
	result = yield localdb.createCollection('helloworld');
	console.log('result: ', result);
	let docs = [
		{ name: 'chan' },
		{ name: 'hyuk' },
		{ name: 'yong' },
		{ name: 'hwan' }
	];
	result = yield localdb.setCollection('helloworld', docs);
	console.log('result: ', result);
	result = yield localdb.getCollection('helloworld');
	console.log('result: ', result);
	result = yield localdb.dropCollection('helloworld');
	console.log('result: ', result);
});

