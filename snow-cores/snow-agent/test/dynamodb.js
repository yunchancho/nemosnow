const AWS = require('aws-sdk');
const co = require('co');
const Promise = require('bluebird');

AWS.config.update({
	region: 'ap-northeast-1',
//endpoint: 'dynamodb.ap-northeast-1.amazonaws.com'
});

let db = Promise.promisifyAll(new AWS.DynamoDB());

let createParams = {
	TableName: 'TestTable',
	KeySchema: [
		{ AttributeName: 'Id', KeyType: 'HASH' }
	],
	AttributeDefinitions: [
		{ AttributeName: 'Id', AttributeType: 'S' }
	],
	ProvisionedThroughput: {
		ReadCapacityUnits: 5,
		WriteCapacityUnits: 5,
	}
}

let waitParams = {
	TableName: 'TestTable'
}

let listParams = {
}

let deleteParams = {
	TableName: 'TestTable'
}

let describeParams = {
	TableName: 'TestTable'
}

let coroutine = co.wrap(function* () {
	try {
		let result = {};
		result = yield db.createTableAsync(createParams);
		console.log('0: ', JSON.stringify(result, null, 2));
		result = yield db.waitForAsync('tableExists', waitParams);
		console.log('0: ', JSON.stringify(result, null, 2));
		result = yield db.listTablesAsync(listParams);
		console.log('0: ', JSON.stringify(result, null, 2));
		result = yield db.describeTableAsync(describeParams);
		console.log('0: ', JSON.stringify(result, null, 2));
		result = yield db.deleteTableAsync(deleteParams);
		console.log('0: ', JSON.stringify(result, null, 2));
	} catch (err) {
		throw err
	}

	return true;
});

coroutine()
	.then(result => console.log('success: ', result))
	.catch(err => console.log('failure: ', err))

