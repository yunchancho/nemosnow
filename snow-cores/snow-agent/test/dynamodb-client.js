const AWS = require('aws-sdk');
const co = require('co');
const Promise = require('bluebird');

AWS.config.update({
	region: 'ap-northeast-1',
	endpoint: 'dynamodb.ap-northeast-1.amazonaws.com'
});

let db = Promise.promisifyAll(new AWS.DynamoDB());
let client = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient());

let dbParams = {
	TableName: 'ProductCatalog',
	KeySchema: [
		{ AttributeName: 'Id', KeyType: 'HASH' }
	],
	AttributeDefinitions: [
		{ AttributeName: 'Id', AttributeType: 'S' }
	],
	ProvisionedThroughput: {
		ReadCapacityUnits: 5,
		WriteCapacityUnits: 5,
	}
}

let putParams = {
	TableName: 'ProductCatalog',
	Item: {
		Id: 1000,
		Price: 2000,
		ProductCategory: 'House',
		Metadata: {
			name: 'chan',
			goal: 'test',
			phone: '010-0000-0000'
		}
	}
};

let getParams = {
	TableName: 'ProductCatalog',
	Key: {
		Id: 1000
	}
}
 
let updateParams = {
	TableName: 'ProductCatalog',
	Key: {
		Id: 1000
	},
	UpdateExpression: 'set Metadata.goal = :goal, Metadata.actions = :actions',
	ExpressionAttributeValues: {
		':goal': 'we will win this game finally!',
		':actions': {
			types: ['hello', 'world'],
			date: '2016-11-29'
		}
	}
}

let deleteParams = {
	TableName: 'ProductCatalog',
	Key: {
		Id: 1000
	}
}

let scanParams = {
	TableName: 'ProductCatalog',
	ProjectionExpression: 'Id, Price, #pc',
	FilterExpression: '#pc = :val',
	ExpressionAttributeNames: {
		'#pc': 'ProductCategory'
	},
	ExpressionAttributeValues: {
		':val': 'Book' 
	}
}

let coroutine = co.wrap(function* () {
	try {
		let result = {};
		//result = yield db.createTableAsync(dbParams);
		console.log('0: ', JSON.stringify(result, null, 2));
		result = yield client.putAsync(putParams)
		console.log('1: ', JSON.stringify(result, null, 2));
		result = yield client.getAsync(getParams)
		console.log('2: ', JSON.stringify(result, null, 2));
		result = yield client.updateAsync(updateParams)
		console.log('3: ', JSON.stringify(result, null, 2));
		result = yield client.getAsync(getParams)
		console.log('4: ', JSON.stringify(result, null, 2));
		result = yield client.deleteAsync(deleteParams)
		console.log('5: ', JSON.stringify(result, null, 2));
		result = yield client.scanAsync(scanParams)
		console.log('6: ', JSON.stringify(result, null, 2));
	} catch (err) {
		throw err
	}

	return true;
});

coroutine()
	.then(result => console.log('success: ', result))
	.catch(err => console.log('failure: ', err))

