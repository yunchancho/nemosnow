const webpack = require("webpack");
const fs = require("fs");
const path = require("path");

let nodeModules = {};

fs.readdirSync("node_modules")
.filter(x => [".bin"].indexOf(x) === -1)
.forEach(mod => nodeModules[mod] = "commonjs " + mod)

module.exports = {
	entry: "./src/index.js",
	target: "node",
	output: {
		path: path.join(__dirname, "dist"),
		filename: "bundle.js"
	},
	devtool: "source-map",
	externals: nodeModules,
	plugins: [
		new webpack.BannerPlugin("require('source-map-support').install();", {
			raw: true, entryOnly: false
		})
	],
	module: {
		loaders: [
			{ test: /\.json$/, loader: "json" }
		]
	}
}
