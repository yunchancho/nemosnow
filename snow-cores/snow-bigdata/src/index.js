#!/usr/bin/env node

const co = require('co');
const program = require('commander');
const colors = require('colors');
const { LogManager } = require('./lib/log-manager');
const metaInfo = require('./meta.json');

program
.option('-i, --install-logconfig <pkgname>', 'install log configuration for the package')
.option('-r, --remove-logconfig <pkgname>', 'uninstall log configuration for the package')
.option('-s, --sync-logconfig', 'synchronize all log configurations by installed pkgs')
.parse(process.argv)

if (!process.argv.slice(2).length) {
	program.outputHelp((text) => colors.red(text));
	process.exit(1);
}

const config = getConfiguration(metaInfo);
const lm = new LogManager(config);

let taskroutine = co.wrap(function* () {
	try {
		yield lm.initialize();
		yield handleCmdline(lm, program);
	} catch (err) {
		throw err;
	}
	return 'complete task';
});

taskroutine()
	.then(result => {
		console.log(result)
		process.exit(0)
	})
	.catch(err => {
		console.log(err)
		process.exit(1)
	});

function handleCmdline(manager, arg) {
	// in case of manual execution on cmdline
	return co(function* () {
		let result;
		try {
			if (arg.installLogconfig) {
				result = yield manager.installLogconfig({ pkgname: arg.installLogconfig })
			} else if (arg.removeLogconfig) {
				result = yield manager.removeLogconfig({ pkgname: arg.removeLogconfig })
			} else if (arg.syncLogconfig) {
				result = yield manager.syncLogconfig();
				yield manager.restartLogCollector()
			} else {
				program.outputHelp((text) => colors.red(text));
				console.log(arg);
				throw 'invalid argument';
			}
		} catch (err) {
			throw err;
		}

		return result;
	});
}

function getConfiguration(metaInfo) {
	let config = Object.assign({}, metaInfo);

	// set param for package manager
	if (process.env.NEMO_DB_PATH) {
		config.nemodbPath = process.env.NEMO_DB_PATH;
	}

	if (process.env.NEMO_LAUNCH_TYPE) {
		config.launchType = process.env.NEMO_LAUNCH_TYPE;
	}

	if (process.env.NEMO_SNOW_SW_KEY_FILE) {
		config.snowSoftwareKeyFile = process.env.NEMO_SNOW_SW_KEY_FILE;
	}

	if (process.env.NEMO_BIGDATA_COLLECTION) {
		config.bigdataCollection = process.env.NEMO_BIGDATA_COLLECTION;
	}

	if (process.env.NEMO_LOG_CONFIG_BASE_PREFIX) {
		config.bigdataLogConfigBasePathPrefix = process.env.NEMO_LOG_CONFIG_BASE_PREFIX;
	}

	if (process.env.NEMO_LOG_FILE_BASE_PREFIX) {
		config.bigdataLogFileBasePathPrefix = process.env.NEMO_LOG_FILE_BASE_PREFIX;
	}

	if (process.env.NEMO_INSTALLED_PKGS_COLLECTION) {
		config.installedPkgsCollection = process.env.NEMO_INSTALLED_PKGS_COLLECTION;
	}

	if (process.env.NEMO_SNOW_DIR) {
		config.snowDir = process.env.NEMO_SNOW_DIR;
	}

	if (process.env.NEMO_SNOW_REGISTRY_DEFAULT_FILE) {
		config.snowRegistryDefaultFile = process.env.NEMO_SNOW_REGISTRY_DEFAULT_FILE;
	}

	return config;
}
