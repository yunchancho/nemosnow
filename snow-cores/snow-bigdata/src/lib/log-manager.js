const co = require('co');
const Promise = require('bluebird');
const mongo = require('mongodb');
const MongoClient = Promise.promisifyAll(mongo).MongoClient;
const _ = require('underscore');
const fs = Promise.promisifyAll(require('fs'));
const path = require('path');
const moment = require('moment')
const {exec, spawn} = require('child_process')
const meta = require('../meta.json')

class LogManager {
	constructor(params) {
		this._params = params;
		this._db = null;
		this._keyid = null;
		this._secret = null;
		this._bucket = null;
		this._region = null;
		this._prefix = null;
		this._swKey = null;
		this._rootConfigFilePath = path.join(params.bigdataLogConfigBasePathPrefix, meta.rootLogConfigFile)
		this._appConfigDirPrefix = path.join(params.bigdataLogConfigBasePathPrefix, meta.appLogConfigDir)
		this._today = moment().format('YYYY-MM-DD')
		console.log(this._today, moment().format())
	}

	initialize() {
		const coroutine = co.wrap(function* (self) {
			self._db = yield MongoClient.connectAsync(self._params.nemodbPath);
			yield copyRootConfigFile(self)
			yield setKeyPair(self);
			yield setBucketInfo(self);
			yield setSwKey(self);
		});

		return coroutine(this);
	}

	installLogconfig(params) {
		let coroutine = co.wrap(function* (self) {
			console.log(`install log config: ${params.pkgname}`);

			let result = yield isPkgExists(self, params.pkgname);
			if (result) {
				yield self.removeLogconfig(params);
			}

			if (!fs.existsSync(self._appConfigDirPrefix)) {
				fs.mkdirSync(self._appConfigDirPrefix);
			}

			let config = null;
		 	if (self._params.launchType == 'cloud'){
				let templatePath = path.join(__dirname, `../../${meta.configsDir}/${meta.cloudTemplateFile}`)
        console.log(templatePath)
				config = createLogconfigForCloud(self, templatePath, params.pkgname);
			} else {
				let templatePath = path.join(__dirname, `../../${meta.configsDir}/${meta.localTemplateFile}`)
        console.log(templatePath)
				config = createLogconfigForLocal(self, templatePath, params.pkgname);
			}
			let filePath = path.join(self._appConfigDirPrefix, `${params.pkgname}.conf`);

			try {
				yield fs.writeFileAsync(filePath, config, { encoding: 'utf8' });
			} catch (e) {
				console.log(`fail to install log config(${filePath}): ${e}`);
				return false;
			}

			//yield setLogConfigFlag(self, params.pkgname, true);

			return true;
		});

		return coroutine(this);
	}

	removeLogconfig(params) {
		let coroutine = co.wrap(function* (self) {
			console.log(`remove log config: ${params.pkgname}`);

			let filePath = path.join(self._appConfigDirPrefix, `${params.pkgname}.conf`);
			if (!fs.existsSync(filePath)) {
				console.log('this log config was already removed');
				return false;
			}

			try {
				yield fs.unlinkAsync(filePath);
				let result = yield isPkgExists(self, params.pkgname);
				if (!result) {
					return false;
				}
				//yield setLogConfigFlag(self, params.pkgname, false);
			} catch (e) {
				console.log(`fail to remove log config(${filePath}): ${e}`);
				return false;
			}

			return true;
		});

		return coroutine(this);
	}

	syncLogconfig() {
		let coroutine = co.wrap(function* (self) {
			console.log(`sync log config`);
			
			if (!fs.existsSync(self._appConfigDirPrefix)) {
				yield fs.mkdirAsync(self._appConfigDirPrefix);
			}

			let files = fs.readdirSync(self._appConfigDirPrefix);
			console.log('files: ', files)
			for (let file of files) {
				yield fs.unlinkAsync(path.join(self._appConfigDirPrefix, file));
			}

			let collection = self._params.installedPkgsCollection;
			let installedPkgs = yield self._db.collection(collection).find({}).toArrayAsync();

			let filename = path.join(self._params.snowDir, self._params.snowRegistryDefaultFile);
			let defaultValue = fs.readFileSync(filename, { encoding: 'utf8' });
      defaultValue = JSON.parse(defaultValue)

			console.log('localdb: ', installedPkgs)
			console.log('defaultValue: ', defaultValue)
			for (let pkg of installedPkgs) {
				try {
          let docs = yield self._db.collection(pkg.pkgname).find({}).toArrayAsync();
          let doc = docs[0]

					if (doc.registry && doc.registry.bigdata) {
						if (doc.registry.bigdata.logEnabled) {
							yield self.installLogconfig({ pkgname: pkg.pkgname });
						}
					} else {
						if (defaultValue.bigdata.logEnabled) {
							yield self.installLogconfig({ pkgname: pkg.pkgname });
						}
					}
				} catch (e) {
					console.log(`fail to install logconfig: ${pkg.pkgname}: ${e}`);
				}
			}

			yield installDefaultLogconfigs(self)
		});

		return coroutine(this);
	}

	restartLogCollector() {
		let coroutine = co.wrap(function* () {
			yield new Promise((resolve, reject) => {
				let command = `${meta.bigdataLogColletor} restart`
				exec(command, (err, stdout, stderr) => {
					if (err) {
						console.log(stderr)
						return reject(err)
					}
					console.log(stdout)
					resolve()
				})
			})
		})

		return coroutine()
	}
}

function installDefaultLogconfigs(self) {
	let coroutine = co.wrap(function* () {
		// set default configurations such as system, monitoring, ... 
		let defaultTemplateFiles = [] 
		if (self._params.launchType == 'cloud'){
			defaultTemplateFiles.push(meta.systemCloudTemplateFile)
		} else {
			defaultTemplateFiles.push(meta.systemLocalTemplateFile)
		}

		for (let templateFile of defaultTemplateFiles ) {
			let config = null;
      let templatePath = path.join(__dirname, `../../${meta.configsDir}/${templateFile}`)
			if (self._params.launchType == 'cloud') {
				config = createLogconfigForCloud(self, templatePath)
			} else {
				config = createLogconfigForLocal(self, templatePath)
			}
			let filePath = path.join(self._appConfigDirPrefix, templateFile);

			try {
				yield fs.writeFileAsync(filePath, config, { encoding: 'utf8' });
			} catch (e) {
				console.log(`fail to install default config(${filePath}): ${e}`);
				return false;
			}
		}

		yield setSyslogForwarding()
	})
	return coroutine()
}

function setSyslogForwarding() {
	let coroutine = co.wrap(function* () {
		let existed = yield new Promise((resolve, reject) => {
			let command = `grep -rn '${meta.syslogForwardSettingCommand}' /etc/rsyslog.conf | wc -l`
			exec(command, (err, stdout, stderr) => {
				if (err) {
					console.log(stderr)
					return reject(err)
				}
				resolve(stdout)
			})
		})
		console.log('syslog existed: ', parseInt(existed.trim()))
		console.log(meta.syslogForwardSettingCommand)

		if (!parseInt(existed.trim())) {
			console.log('Making syslog be forwarding to fluentd')
			yield new Promise((resolve, reject) => {
				let command = `echo '${meta.syslogForwardSettingCommand}' | cat >> /etc/rsyslog.conf`
				exec(command, (err, stdout, stderr) => {
					if (err) {
						console.log(stderr)
						return reject(err)
					}
					resolve()
				})
			})
		}
	})

	return coroutine()
}

function copyRootConfigFile(self) {
	let coroutine = co.wrap(function* () {
		let rootConfigPath = path.join(__dirname, `../../${meta.configsDir}/${meta.rootLogConfigFile}`)
		let data = fs.readFileSync(rootConfigPath, { encoding: 'utf8' })
		yield fs.writeFileAsync(self._rootConfigFilePath, data, { encoding: 'utf8' })
	})

	return coroutine()
}

function setKeyPair(self) {
	let coroutine = co.wrap(function* () {
		try {
			if (!process.env.AWS_ACCESS_KEY_ID || !process.env.AWS_SECRET_ACCESS_KEY) {
				console.log('no aws credentials ...');
				return;
			}

			self._keyid = process.env.AWS_ACCESS_KEY_ID;
			self._secret = process.env.AWS_SECRET_ACCESS_KEY;
			console.log(`${self._keyid}, ${self._secret}`);
		} catch (e) {
			console.log(`fail to set key pair: ${e}`);
		}
	});

	return coroutine();
}

function setBucketInfo(self) {
	let coroutine = co.wrap(function* () {
		try {
			let docs = yield self._db.collection(self._params.bigdataCollection).find({}).toArrayAsync();
			if (!docs.length) {
				return;
			}
			self._bucket = docs[0].bucket;
			self._region = docs[0].region;
			self._prefix = docs[0].prefix;

			console.log(`${self._bucket}, ${self._region}, ${self._prefix}`);
		} catch (e) {
			console.log(`fail to set bigdata bucket info: ${e}`);
		}
	});

	return coroutine();
}

function setSwKey(self) {
	let coroutine = co.wrap(function* () {
		try {
			let filePath = `${self._params.snowDir}/${self._params.snowSoftwareKeyFile}`;
			let swKey = yield fs.readFileAsync(filePath, { encoding: 'utf8' });
			self._swKey = swKey.trim()
			console.log(`${self._swKey}`);
		} catch (e) {
			console.log(`fail to set swKey: ${e}`);
		}
	});

	return coroutine();
}

function createLogconfigForCloud(self, templatePath, pkgname) {
	let template = fs.readFileSync(templatePath, { encoding: 'utf8' });

	//TODO we need to get all data from local file, db to replace variables
	// aws_key_id, aws_sec_key, s3_bucket, s3_region, path (using bucket prefix)
	//
	//
	
	let reps = [
		{ src: /###KEYID###/g, dest: self._keyid },
		{ src: /###SECRET###/g, dest: self._secret },
		{ src: /###BUCKET###/g, dest: self._bucket },
		{ src: /###REGION###/g, dest: self._region },
		{ src: /###PREFIX###/g, dest: self._prefix },
		{ src: /###SWKEY###/g, dest: self._swKey },
		{ src: /###PKGNAME###/g, dest: pkgname },
		{ src: /###TODAY###/g, dest: self._today},
	];

	console.log('cloud template: ', template)
	let config = template;
	for (let rep of reps) {
		config = config.replace(rep.src, rep.dest);
	}
	console.log('cloud config: ', config)

	return config;
}

function createLogconfigForLocal(self, templatePath, pkgname) {
	let template = fs.readFileSync(templatePath, { encoding: 'utf8' });
	let config = template.replace(/###PKGNAME###/g, pkgname);
	let outputDir = `${self._params.bigdataLogFileBasePathPrefix}/${pkgname}`;
	config = config.replace(/###LOGBASEDIR###/g, outputDir);
	config = config.replace(/###TODAY###/g, self._today);

	return config;
}

function isPkgExists(self, pkgname) {
	let coroutine = co.wrap(function* () {
		console.log(`check pkgname exists: ${pkgname}`);

		let collection = self._params.installedPkgsCollection;
		let docs = yield self._db.collection(collection).find({}).toArrayAsync();
		let found = _.findWhere(docs, { pkgname: pkgname });

		if (!found) {
			console.log(`${pkgname} is not existed`);
			return false;
		}

		return true;
	});

	return coroutine();
}

function setLogConfigFlag(self, pkgname, value) {
	let coroutine = co.wrap(function* () {
		console.log(`set log config flag: ${pkgname}`);

		let result = yield isPkgExists(self, pkgname);
		if (!result) {
			return false;
		}

		let collection = self._params.installedPkgsCollection;
		let setOption = {
			$set: {
				registry: {
					bigdata: {
						logEnabled: value
					}
				}
			}
		}

    // TODO This should be modified to change registry attr in pkgname's collection by deep merging
		try {
			yield self._db.collection(collection).updateAsync({ pkgname: pkgname }, setOption);
		} catch (e) {
			console.log(`fail to update ${collection} for set log flag`);
			return false;
		}

		return true;
	});

	return coroutine();
}

function listLogConfigFlag(self) {
	let coroutine = co.wrap(function* () {
		console.log(`set log config flag: ${pkgname}`);
		let collection = self._params.installedPkgsCollection;
		let installedPkgs = yield self._db.collection(collection).find({}).toArrayAsync();
		let flags = [];
    for (let pkg of installedPkgs) {
			try {
        let docs = yield self._db.collection(pkg.pkgname).find({}).toArrayAsync();
        let doc = docs[0]
				flags.push({
					pkgname: pkg.pkgname,
					registry: doc.registry.bigdata.logEnabled? true: false
				});
			} catch (e) {
				console.log(`fail to add config flag (${pkg.pkgname}): ${e}`);
			}
		}

		return flags;
	});

	return coroutine();
}

module.exports = {
	LogManager
}
