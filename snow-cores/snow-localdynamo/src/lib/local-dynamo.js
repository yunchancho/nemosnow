const co = require('co')
const exec = require('child_process').exec
const spawn = require('child_process').spawn
const path = require('path')
const meta = require('../meta.json')

class LocalDynamo {
	constructor() {
	}

	initialize() {
		return Promise.resolve();
	}

	start(param) {
    console.log('cwd: ', process.cwd())
    console.log('exepath: ', process.execPath)
		let coroutine = co.wrap(function* () {
			yield new Promise((resolve, reject) => {
				let command = 'java'
        let libPath = path.join(path.dirname(process.execPath), `../${meta.dynamoDir}/${meta.javaLibDir}`)
        let jarPath = path.join(path.dirname(process.execPath), `../${meta.dynamoDir}/${meta.javaJarFile}`)
				let args = [
					`-Djava.library.path=${libPath}`,
					'-jar', jarPath,
					'-dbPath', param.dbPath,
					'-port', param.port,
					'-sharedDb'
				]

				try {
					let child = spawn(command, args, { detached: true })
					child.unref()
          console.log('succeed to spawn dynamo server')
				} catch (err) {
          console.log(err)
					return reject(err)
				}
				resolve()
			})
		})

		return coroutine()
	}

	reset(param) {
		let coroutine = co.wrap(function* () {
			yield new Promise((resolve, reject) => {
				let command = path.join(path.dirname(process.execPath), `../${meta.scriptsDir}/${meta.resetTableScript}`)
				let args =	[ param.swKey, param.port ]
				let child = spawn(command, args, {shell: true})

				child.stdout.on('data', (data) => {
					console.log(data.toString())
				})
				child.stderr.on('data', (data) => {
					console.log(data.toString())
				})
				child.on('close', (code) => {
					if (code !== 0) {
            console.log('failed to close')
						reject()
					}
					resolve()
				})
			})
		})

		return coroutine()
	}
}

module.exports = { LocalDynamo }
