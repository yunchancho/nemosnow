#!/usr/bin/env node

const co = require('co');
const program = require('commander');
const colors = require('colors');
const { LocalDynamo } = require('./lib/local-dynamo');
const meta = require('./meta.json');

program
.option('-s, --start', 'start local dynamo db')
.option('-r, --reset', 'reset local dynamo db with device specific values')
.option('-p, --port <port>', 'set port of dynamodb server')
.option('-k, --sw-key <key>', 'set sw key of device')
.option('-d, --db-path <path>', 'set db path of dynamodb server')
.parse(process.argv)

if (!process.argv.slice(2).length) {
	program.outputHelp((text) => colors.red(text));
	process.exit(1);
}

const config = getConfiguration(meta);
const ld = new LocalDynamo(config);

let taskroutine = co.wrap(function* () {
	try {
		yield ld.initialize();
		yield handleCmdline(ld, program);
	} catch (err) {
		throw err;
	}
	return 'complete task';
});

taskroutine()
	.then(result => {
		console.log(result)
		if (program.start && process.connected) {
			process.send(result);
		} else {
			process.exit(0)
		}
	})
	.catch(err => {
		console.log(err)
		process.exit(1)
	});

function handleCmdline(manager, arg) {
	return co(function* () {
		let result;
		try {
			let param = {
				port: arg.port? arg.port: meta.port
			}
			if (arg.start) {
				param.dbPath = arg.dbPath? arg.dbPath: meta.snowLocalDynamodbFilePath
				result = yield manager.start(param)
			} else if (arg.reset) {
				if (!arg.swKey) {
					throw { code: 'no swkey' }
				}
				param.swKey = arg.swKey
				result = yield manager.reset(param)
			} else {
				program.outputHelp((text) => colors.red(text));
				console.log(arg);
				throw 'invalid argument';
			}
		} catch (err) {
			throw err;
		}

		return result;
	});
}

function getConfiguration(metaInfo) {

	let config = Object.assign({}, metaInfo)

	// override passed ENV variables from parent process
	//
	
	return config
}
