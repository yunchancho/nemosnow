#!/bin/bash

help()
{
	echo "Usage: $0 [SW_KEY] [PORT]"
}

if [ $# -ne 2 ]
then
	help
	exit 0
fi

SWKEY=$1
CUSTOMER=${SWKEY%%_*}
REGISTRY_TABLE="Registry"
ENDPOINT="http://localhost:"$2
CURRENT_DIR="$(dirname $0)"

# Registry table creation (This table stores default values for each common options of application)
aws dynamodb delete-table --table-name $REGISTRY_TABLE --endpoint-url $ENDPOINT
aws dynamodb create-table --table-name $REGISTRY_TABLE --attribute-definitions AttributeName=name,AttributeType=S \
 	--key-schema AttributeName=name,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
 	--endpoint-url $ENDPOINT
aws dynamodb batch-write-item --request-items file://$CURRENT_DIR/$REGISTRY_TABLE.json --endpoint-url $ENDPOINT


# Device table creation
sed s/###TABLE###/$SWKEY/g $CURRENT_DIR/Device.json | sed s/###CUSTOMER###/$CUSTOMER/g > "$CURRENT_DIR/$SWKEY".json
sed s/###TABLE###/$SWKEY/g $CURRENT_DIR/DeviceShell.json | sed s/###CUSTOMER###/$CUSTOMER/g > "$CURRENT_DIR/$SWKEY"_shell.json
sed s/###TABLE###/$SWKEY/g $CURRENT_DIR/DeviceTheme.json | sed s/###CUSTOMER###/$CUSTOMER/g > "$CURRENT_DIR/$SWKEY"_theme.json

aws dynamodb delete-table --table-name $SWKEY --endpoint-url $ENDPOINT
aws dynamodb create-table --table-name $SWKEY --attribute-definitions AttributeName=name,AttributeType=S \
 	--key-schema AttributeName=name,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
 	--endpoint-url $ENDPOINT
aws dynamodb batch-write-item --request-items file://$CURRENT_DIR/$SWKEY.json --endpoint-url $ENDPOINT
aws dynamodb batch-write-item --request-items file://"$CURRENT_DIR/$SWKEY"_shell.json --endpoint-url $ENDPOINT
aws dynamodb batch-write-item --request-items file://"$CURRENT_DIR/$SWKEY"_theme.json --endpoint-url $ENDPOINT
aws dynamodb scan --table-name $SWKEY  --endpoint-url $ENDPOINT


# List up current tables
aws dynamodb list-tables --endpoint-url $ENDPOINT

rm "$CURRENT_DIR/$SWKEY"*.json
