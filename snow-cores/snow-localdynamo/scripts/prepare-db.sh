#!/bin/sh

EXTRACT_DIR='./java'
DOWNLOAD_URL='https://s3-us-west-2.amazonaws.com/dynamodb-local/dynamodb_local_latest.zip'
ZIP_FILE=${DOWNLOAD_URL##*/}

if [ ! -d $EXTRACT_DIR ]; then
	if [ ! -f $ZIP_FILE ]; then
		wget $DOWNLOAD_URL
	fi
	unzip $ZIP_FILE -d $EXTRACT_DIR 
	rm $ZIP_FILE
fi
