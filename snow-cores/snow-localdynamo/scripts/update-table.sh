#!/bin/bash

# This script must update only tables which don't affect device specific setting
# Currently, these tables include 'Registry'

help()
{
	echo "Usage: $0 [PORT]"
}

if [ $# -ne 1 ]
then
	help
	exit 0
fi

REGISTRY_TABLE="Registry"
ENDPOINT="http://localhost:"$1
CURRENT_DIR="$(dirname $0)"

# Registry table creation (This table stores default values for each common options of application)
aws dynamodb delete-table --table-name $REGISTRY_TABLE --endpoint-url $ENDPOINT
aws dynamodb create-table --table-name $REGISTRY_TABLE --attribute-definitions AttributeName=name,AttributeType=S \
 	--key-schema AttributeName=name,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
 	--endpoint-url $ENDPOINT
aws dynamodb batch-write-item --request-items file://$CURRENT_DIR/$REGISTRY_TABLE.json --endpoint-url $ENDPOINT
aws dynamodb scan --table-name $REGISTRY_TABLE --endpoint-url $ENDPOINT

