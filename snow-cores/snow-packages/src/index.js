#!/usr/bin/env node 

const co = require('co');
const program = require('commander');
const colors = require('colors');
const { PackageManager } = require('./lib/package-manager');
const metaInfo = require('./meta.json');

// TODO support several packages in one command on install/remove/update
program
.option('-i, --install <pkgname>', 'install package')
.option('-r, --remove <pkgname>', 'uninstall package')
.option('-u, --upgrade [pkgnames]', 'upgrade all packages')
.option('-U, --update-index [deb server url]', 'update package with server url (optional)')
.option('-p, --sync-ppa-list', 'sync ppa list')
.option('-m, --sync-manifests', 'sync all installed manifests with local db')
.option('-l, --list-upgradables', 'list up upgradable only nemo packages')
.option('-c, --copy-usb-packages', 'copy new packages from usb, and update ppa for them')
.option('-o, --restore-usb-configs', 'overwrite configs of packages from usb on local db')
.option('-d, --depends <pkgname>', 'get nemo packages with dependancy by passed package')
.parse(process.argv)

if (!process.argv.slice(2).length) {
	program.outputHelp((text) => colors.red(text));
	process.exit(1);
}

const config = getConfiguration(metaInfo);
const pm = new PackageManager(config);

let taskroutine = co.wrap(function* () {
	try {
		yield pm.initialize();
		let result = yield handleCmdline(pm, program);
		return result
	} catch (err) {
		throw err;
	}
	return 'complete task';
});

taskroutine()
	.then(result => {
    if (result) {
		  console.log(JSON.stringify(result, null, 2))
    }
		if (result && process.connected) {
			process.send(result);
		}
		process.exit(0);
	})
	.catch(err => {
		console.log(err)
		process.exit(-1);
	})

function handleCmdline(manager, arg) {
	// in case of manual execution on cmdline
	return co(function* () {
		let result;
		try {
			if (arg.install) {
				result = yield manager.install({ deb: arg.install })
			} else if (arg.remove) {
				result = yield manager.remove({ deb: arg.remove })
			} else if (arg.upgrade) {
        let pkgs = []
        if (typeof arg.upgrade !== 'boolean') {
          pkgs = arg.upgrade.split(/,| /) 
        }
				result = yield manager.upgrade({ pkgs })
			} else if (arg.updateIndex) {
				result = yield manager.updateIndex({ url: arg.updateIndex })
			} else if (arg.syncPpaList) {
				result = yield manager.syncPpaList();
			} else if (arg.syncManifests) {
				result = yield manager.syncManifests();
			} else if (arg.listUpgradables) {
				result = yield manager.listUpgradables();
			} else if (arg.copyUsbPackages) {
				if (config.launchType === 'local') {
					result = yield manager.copyPackagesFromUsb();
				} else {
					throw 'This command is only permitted on local version'
				}
			} else if (arg.restoreUsbConfigs) {
				if (config.launchType === 'local') {
					result = yield manager.restoreConfigsFromUsb();
				} else {
					throw 'This command is only permitted on local version'
				}
			} else if (arg.depends) {
				result = yield manager.getDepends({ deb: arg.depends })
			} else {
				program.outputHelp((text) => colors.red(text));
				console.log(arg);
				throw 'invalid argument';
			}
		} catch (err) {
			throw err;
		}

		return result;
	});
}

function getConfiguration(metaInfo) {
	let config = Object.assign({}, metaInfo);

	// set param for package manager
	if (process.env.NEMO_INSTALLED_PKGS_COLLECTION) {
		config.installedPkgsCollection = process.env.NEMO_INSTALLED_PKGS_COLLECTION;
	}

	if (process.env.NEMO_PKG_BASE_PATH_PREFIX) {
		config.pkgBasePathPrefix = process.env.NEMO_PKG_BASE_PATH_PREFIX;
	}

	if (process.env.NEMO_DB_PATH) {
		config.nemodbPath = process.env.NEMO_DB_PATH;
	}

	if (process.env.NEMO_PKG_INDEX_COLLECTION) {
		config.pkgIndexCollection = process.env.NEMO_PKG_INDEX_COLLECTION;
	}

	if (process.env.NEMO_PKG_INDEX_FILE) {
		config.pkgIndexFile = process.env.NEMO_PKG_INDEX_FILE;
	}

	if (process.env.NEMO_LAUNCH_TYPE) {
		config.launchType = process.env.NEMO_LAUNCH_TYPE;
	}

	return config;
}
