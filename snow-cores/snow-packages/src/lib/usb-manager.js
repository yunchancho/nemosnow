const co = require('co')
const path = require('path')
const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))
const systeminfo = require('systeminformation')
const exec = require('child_process').exec
const meta = require('../meta.json')

const configDelimiter = '.json'

function getDefaultUsbMountPath() {
	let coroutine = co.wrap(function* () {
		let devices = yield systeminfo.blockDevices()
		let usbdisks = devices.filter(device => {
			return (device.type === 'disk') && device.removable
		})

		let usbparts = []
		for (let disk of usbdisks) {
			let list = devices.filter(device => {
				return (device.type === 'part') && ~device.name.indexOf(disk.name) && device.mount
			})
			usbparts = usbparts.concat(list.map(part => part.mount))
		}

		console.log('mount paths: ', JSON.stringify(usbparts, null, 2))
		return usbparts.length? usbparts[0]: null
	})

	return coroutine()
}

function generateUsbSrcPath(self, src) {
	return co(function* () {
		let usbPath = yield getDefaultUsbMountPath()
		if (!usbPath) {
			return 'failed to get mounted usb path'
		}
		let subPath = src.split(self.usbSrcPrefix)[1]
		let newPath = `${usbPath}/${subPath}`
		console.log(`usb src path: ${newPath}`)

		return newPath
	})
}

function getLocalDestinationMeta() {
	return co(function* () {
		if (!fs.existsSync(meta.pkgIndexFile)) {
			throw 'PPA file for nemosnow does not exists'
		}

		let output = yield fs.readFileAsync(meta.pkgIndexFile, { encoding: 'utf8'})
		let lines = output.split('\n').filter(line => !!line)
		let basePath = lines[0].split(' ')[1].split(':')[1].trim()
		let subComponents = lines.map(line => line.split(' ')[2])

		return { basePath, subComponents }
	})
}

function copyPackages() {
	return co(function* () {
		let usbPath = yield getDefaultUsbMountPath()
		if (!usbPath) {
			throw 'no mount path'
		}
		let dest = yield getLocalDestinationMeta()

		if (!fs.existsSync(`${dest.basePath}`)) {
			yield new Promise((resolve, reject) => {
				exec(`mkdir -p ${dest.basePath}`, (err, stdout, stderr) => {
					if (err) {
						return reject(err)
					}
					resolve()
				})
			})
		}

		for (let component of dest.subComponents) {
			try {
				yield new Promise((resolve, reject) => {
					let srcPath = `${usbPath}/${meta.usbPackagePathPrefix}/${component}`
					exec(`cp -av ${srcPath} ${dest.basePath}`, (err, stdout, stderr) => {
						if (err) {
							return reject(err)
						}
						console.log(`handle ${component}: ${srcPath} => ${dest.basePath}`)
						resolve()
					})
				})
			} catch (err) {
				console.log(`failed to copy ${component} component packages: ${err}`)
			}
		}
	})
}

function updatePpa() {
	return co(function* () {
		let dest = yield getLocalDestinationMeta()

		for (let component of dest.subComponents) {
			try {
				yield new Promise((resolve, reject) => {
					let command = `dpkg-scanpackages ${component} /dev/null`
					command += ` | gzip -9c > ${component.slice(0, -1)}/Packages.gz`
					exec(command, { cwd: dest.basePath }, (err, stdout, stderr) => {
						if (err) {
							return reject(err)
						}
						resolve()
					})
				})
			} catch (err) {
				console.log(`failed to update ppa for ${component}: ${err}`)
			}
		}
	})
}

function restoreConfigs() {
	return co(function* () {
		let usbPath = yield getDefaultUsbMountPath()
		if (!usbPath) {
			throw 'no mount path'
		}
		let files = yield fs.readdirAsync(path.join(usbPath, meta.usbConfigPathPrefix))
		let jsonFiles = files.filter(file => file.includes(configDelimiter))
		let configs = []

		for (let file of jsonFiles) {
			let content = null
			try {
				let configPath = path.join(usbPath, meta.usbConfigPathPrefix, file)
				content = require(configPath)
			} catch (err) {
				console.log('error:', err)
				continue
			}
			console.log('success: ', content)
			configs.push(content)
		}

		console.log(`Success to get restored configs(${configs.length})`)
		return configs
	})
}

module.exports = {
	copyPackages, updatePpa, restoreConfigs
}
