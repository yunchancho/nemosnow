const EventEmitter = require('events').EventEmitter;
const Promise = require('bluebird');
const { spawn, execFile } = require('child_process');
const co = require('co');
const path = require('path')
const fs = Promise.promisifyAll(require('fs'));
const _ = require('underscore');
const mongo = require('mongodb');
const MongoClient = Promise.promisifyAll(mongo).MongoClient;
const { ManifestManager } = require('./manifest-manager');
const { PpaManager } = require('./ppa-manager');
const usbManager = require('./usb-manager')
const meta = require('../meta.json')

const aptDefaultOptions = [
  '--assume-yes',
  '--allow-unauthenticated',
  '--no-install-recommends'
]

const childs = []

process.on('SIGTERM', () => {
  killChildProcesses()
})

process.on('SIGHUP', () => {
  killChildProcesses()
})

function killChildProcesses() {
  for (let child of childs) {
    child.kill()
  }
}

function doAptGet(pkgname, operator, options) {
  let coroutine = co.wrap(function* () {
    let param = null
    if ([ 'update', 'autoremove'].includes(operator)) {
      params = [ ...aptDefaultOptions, operator ]
    } else {
      params = [ ...aptDefaultOptions, operator, pkgname ]
      if (options) {
        params = [ ...options, ...params ]
      }
    }

    yield new Promise((resolve, reject) => {
      let handle = spawn('apt-get', params, { stdio: 'inherit' })
      handle.on('exit', (code) => {
        let handleIdx = childs.indexOf(handle)
        if (handleIdx !== -1) {
          childs.splice(handleIdx, 1)
        }
        if (code !== 0) {
          return reject();
        }

        return resolve()
      })

      childs.push(handle)
    })
  })

  return coroutine()
}


class PackageManager extends EventEmitter {
	constructor(param) {
		super();
		this._param = param;
		this._manifest = new ManifestManager({
			nemodbPath: param.nemodbPath,
			pkgBasePathPrefix: param.pkgBasePathPrefix,
			pkgsCollectionName: param.installedPkgsCollection
		});
		this._ppa  = new PpaManager({
			nemodbPath: param.nemodbPath,
			pkgIndexCollection: param.pkgIndexCollection,
			pkgIndexFile: param.pkgIndexFile
		});
	}

	initialize() {
		return Promise.resolve()
	}

	install(param) {
    let self = this;
		let coroutine = co.wrap(function* () {
			if (!fs.existsSync(self._param.pkgBasePathPrefix)) {
				yield fs.mkdirAsync(self._param.pkgBasePathPrefix);
			}
      
      yield doAptGet(param.deb, 'install')
      let result = yield self.syncManifests()

			console.log(`success to install ${param.deb}`);
      return result.registers
		});

		return coroutine();
	}

	remove(param) {
		let self = this;
		let coroutine = co.wrap(function* () {
      yield doAptGet(param.deb, 'purge', [ '--auto-remove'])
      yield new Promise((resolve, reject) => {
        // in case that some files are added into package directory manually,
        // package directory is not removed by apt-get. 
        // So we need to make sure that package dir is removed as removing it explicitly
        const packageDirPath = path.join(meta.pkgBasePathPrefix, param.deb)
        execFile('rm', [ '-rf', packageDirPath ], (err, stdout, stderr) => {
          if (err) {
            console.log(stderr)
          }
          console.log(stdout)
          return resolve()
        })
      })

      // We need to call 'unregister' function unlike install case,
      // becase pkglist collection has been synced with dynamo before this is handled,
      // so that removed pkg doesn't exist in pkglist collection. Additionally
      // to unregister some packages dependant with this pkg, we call syncManifest function.
			yield self._manifest.unregister(param.deb);
      //let result = yield self.syncManifests()

			console.log(`success to remove ${param.deb}`);
      //return [param.deb, ...result.unregisters]
      return [param.deb]
		});
		return coroutine();
	}

	upgrade(param) {
    console.log('param: ', param)
		let self = this;
    let upgradedPkgs = []
		let coroutine = co.wrap(function* () {
      let upgradables = yield self.listUpgradables({ pkgs: param.pkgs });
      console.log('passed pkgs: ', param.pkgs)
      if (param.pkgs.length) {
			  upgradables = _.intersection(param.pkgs, upgradables)
      } 
      console.log('upgradables: ', upgradables)
			for (let pkg of upgradables) {
        try {
          yield doAptGet(pkg, 'install', [ '--only-upgrade' ])
          try {
            yield self._manifest.register(path.join(meta.pkgBasePathPrefix, pkg, meta.manifestFileName))
          } catch (err) {
            console.log(err)
          }
          upgradedPkgs.push(pkg)
        } catch (err) {
          console.log(err)
        }
			}
      yield doAptGet('', 'autoremove')

      // register dependencies if they exists
      let syncPkgs = yield self.syncManifests()

      // result may include special packages regarding infra without manifest.json
      const result = [ ...upgradedPkgs, ...syncPkgs.registers ]
		  console.log(`success to upgrade ${[ ...new Set(result) ].join(', ')}`);

      return [ ...new Set(result) ]
		});

		return coroutine();
	}

	updateIndex(param) {
		let self = this;
		let coroutine = co.wrap(function* () {
      yield doAptGet('', 'update')
			console.log(`success to update package index`);
		});
		return coroutine();
	}

	syncManifests() {
		let self = this;
		let coroutine = co.wrap(function* () {
			let result = yield self._manifest.sync();
			console.log('success to sync manifests to localdb');
      return result
		});
		return coroutine();
	}

	syncPpaList() {
		let self = this;
		let coroutine = co.wrap(function* () {
			let result = yield self._ppa.syncList();
			console.log('success to sync ppa list with localdb');
			if (process.connected) {
				process.send(result);
			}
			process.exit(0);
		});
		return coroutine();
	}

	listUpgradables(param) {
		let self = this;
		let coroutine = co.wrap(function* () {
			// get all upgradable packages with system 
			let result = yield new Promise((resolve, reject) => {
				execFile('apt', ['list', '--upgradable'], (error, stdout, stderr) => {
					if (error) {
						reject();
					}

					let pkgs = [];
					let lines = stdout.split('\n');
					for (let line of lines) {
						if (line.indexOf('/') < 0) {
							continue;
						}
						pkgs.push(line.split('/')[0]);
					}
					resolve(pkgs);
				});
			});

      let targets = []
      if (param.pkgs && param.pkgs.length) {
        targets = param.pkgs
      } else {
        const db = yield MongoClient.connectAsync(meta.nemodbPath);
        let installedPkgs = yield db.collection(meta.installedPkgsCollection).find({}).toArrayAsync()
        targets = installedPkgs.map(i => i.pkgname)
      }

			return _.intersection(targets, result)
		});

		return coroutine();
	}

	copyPackagesFromUsb() {
		let coroutine = co.wrap(function* () {
			yield usbManager.copyPackages()
			yield usbManager.updatePpa()
		})

		return coroutine()
	}

	restoreConfigsFromUsb() {
		let coroutine = co.wrap(function* () {
      let configs = [];
      try {
			  configs = yield usbManager.restoreConfigs()
      } catch (err) {
        console.log('getting restored configs error: ', err)
      }
			return configs
		})

		return coroutine()
	}

  getDepends(param) {
    let self = this;
		let coroutine = co.wrap(function* () {
			// get all upgradable packages with system 
			let result = yield new Promise((resolve, reject) => {
				execFile('apt-cache', ['depends', param.deb], (error, stdout, stderr) => {
					if (error) {
						reject();
					}

					let pkgs = [];
					let lines = stdout.split('\n');
					for (let line of lines) {
						if (!line.includes('Depends:')) {
							continue;
						}
						pkgs.push(line.split('Depends:').pop().trim());
					}
          console.log('depends: ', pkgs)

					resolve(pkgs)
				})
			})

      const db = yield MongoClient.connectAsync(meta.nemodbPath);
      let installedPkgs = yield db.collection(meta.installedPkgsCollection).find({}).toArrayAsync();
      let dependPkgs = _.intersection(installedPkgs.map(i => i.pkgname), result)

		  return dependPkgs
		});

		return coroutine();
  }
}

module.exports = {
	PackageManager
}
