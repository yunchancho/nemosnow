const co = require('co');
const colors = require('colors');
const Promise = require('bluebird');
const mongo = require('mongodb');
const MongoClient = Promise.promisifyAll(mongo).MongoClient;
const ObjectID = mongo.ObjectID;
const fs = Promise.promisifyAll(require('fs'));
const _ = require('underscore');
const deepMerge = require('deepmerge')
const path = require('path');
const meta = require('../meta.json')

const manifestFile = 'manifest.json';

class ManifestManager {
	constructor(param) {
		this._param = param;
		this._nemodbPath = param.nemodbPath;
		this._pkgsCollectionName = param.pkgsCollectionName;
		this._pkgBasePathPrefix = param.pkgBasePathPrefix;
	}

	register(manifestPath) {
		let self = this;
		let coroutine = co.wrap(function* () {
			//TODO if one of generators makes exception or error, 
			//we need to roll back regarding previous tasks
			try {
				yield fs.accessAsync(manifestPath);
				let manifest = require(manifestPath);
				//console.log("manifest path: ", JSON.stringify(manifest, null, 2));

				removeManifestUnknownAttrs(self, manifest);
				setManifestCommonConfig(self, manifest);

				yield registerStaticManifest(self, manifest);
				yield registerDynamicManifest(self, manifest);
			} catch (err) {
				console.log(err);
				throw err;
			}
		});	

		return coroutine();
	}

	unregister(pkgname) {
		let self = this;
		let coroutine = co.wrap(function* () {
			//TODO if one of generators makes exception or error, 
			//we need to roll back regarding previous tasks
			try {
				yield unregisterStaticManifest(self, pkgname);
			} catch (err) {
				console.log(err);
			}
			try {
				yield unregisterDynamicManifest(self, pkgname);
			} catch (err) {
				console.log(err);
      }
		});	

		return coroutine();
	}

	sync() {
		let self = this;
		let coroutine = co.wrap(function* () {
			let installedPkgs = [];
			if (fs.existsSync(self._pkgBasePathPrefix)) {
				installedPkgs = yield fs.readdirAsync(self._pkgBasePathPrefix);
			}

			const db = yield MongoClient.connectAsync(self._nemodbPath);
			let collections = yield db.collection(self._pkgsCollectionName).find({}).toArrayAsync();
			let registeredPkgs = collections.filter(c => Object.keys(c).length > 1).map(c => c.pkgname)

			let onlyInstalledPkgs = _.difference(installedPkgs, registeredPkgs);
			let onlyRegisteredPkgs = _.difference(registeredPkgs, installedPkgs); 
			for (let pkgName of onlyInstalledPkgs) {
        // After device verification, this logic register platform packages to local db
        // And then, nemosnow-agent overwrites them to remote db
				yield self.register(path.join(self._pkgBasePathPrefix, pkgName, manifestFile));
			}

			for (let pkgName of onlyRegisteredPkgs) {
				yield self.unregister(pkgName);
			}

      return { 
        registers: onlyInstalledPkgs,
        unregisters: onlyRegisteredPkgs
      }
		});

		return coroutine();
	}

}

function registerStaticManifest(self, manifest) 
{
	const coroutine = co.wrap(function* () {
		try {
			const db = yield MongoClient.connectAsync(self._nemodbPath);
			let appsCollection = db.collection(self._pkgsCollectionName);
			if (!appsCollection) {
				appsCollection = yield db.createCollectionAsync(self._pkgsCollectionName);
				console.log('create collection: ', self._pkgsCollectionName);
			}

			let staticManifest = Object.assign({}, manifest);

			delete staticManifest.config;
			delete staticManifest.graph;
			delete staticManifest.registry;

			let newObjectId = new ObjectID(); 
			// we should convert ObjectID instance to string for saving on aws dynamodb
			staticManifest._id = newObjectId.toHexString();
      //console.log('manifest new _id: ', staticManifest._id);

			yield appsCollection.deleteOneAsync({ 'pkgname': manifest.pkgname });
			yield appsCollection.insertOneAsync(staticManifest);
		} catch (err) {
			throw err;
		}
	});

	return coroutine();
}


function removeManifestUnknownAttrs(self, manifest) {
	// remove unknown manifest attrs
}


function setManifestCommonConfig(self, manifest) {
	// TODO we need to add app-common configs
	// if app has such configs, they should be used,
	// Otherwise, we need to set default value for that configs

	// 1. read default settings 
	// 2. overwrite this manifest on it.
}

function registerDynamicManifest(self, manifest) 
{
	const coroutine = co.wrap(function* () {
		try {
			const db = yield MongoClient.connectAsync(self._nemodbPath);
			const collectionNames = yield db.listCollections({ name: manifest.pkgname }).toArrayAsync();

      let value = {}
			let configCollection = {};

      let newDynamicManifest = { 
        config: manifest.config || {},
        graph: manifest.graph || {},
        registry: manifest.registry || {}
      }

			if (!collectionNames.length) { 
				configCollection = yield db.createCollectionAsync(manifest.pkgname);
        value = Object.assign({}, newDynamicManifest);
			} else {
				console.log('already existed collection: ', manifest.pkgname)
				// if config collection for this app already exists,
				// this task is caused by app upgarde request
				// so we need to keep existing config collection without change.
				// but how about case of changed config structure?
				// For this case, we first overwrite existing config on new config
				configCollection = db.collection(manifest.pkgname);
		    let docs = yield configCollection.find({}).toArrayAsync()
        value = deepMerge(newDynamicManifest, docs[0])
			}

      // we need to check config is array with objects or just one object
      let newObjectId = new ObjectID(); 

      // we should convert ObjectID instance to string for saving on aws dynamodb
      value._id = newObjectId.toHexString();

      yield configCollection.deleteManyAsync();
      yield configCollection.insertOneAsync(value);
		} catch (err) {
      console.log(err)
			throw err;
		}
	});

	return coroutine();
}

function unregisterStaticManifest(self, pkgname) 
{
	const coroutine = co.wrap(function* () {
		try {
			const db = yield MongoClient.connectAsync(self._nemodbPath);
			let appsCollection = db.collection(self._pkgsCollectionName);

			const collectionNames = yield db.listCollections({ name: self._pkgsCollectionName }).toArrayAsync();
			if (!collectionNames.length) { 
				console.log(`${self._pkgsCollectionName} collection does not exist`);
				return true;
			}

			yield appsCollection.deleteOneAsync({ 'pkgname': pkgname });
		} catch (err) {
			throw err;
		}
	});

	return coroutine();
}

function unregisterDynamicManifest(self, pkgname) 
{
	const coroutine = co.wrap(function* () {
		try {
			const db = yield MongoClient.connectAsync(self._nemodbPath);
			const collectionNames = yield db.listCollections({ name: pkgname }).toArrayAsync();

			if (!collectionNames.length) { 
        console.log('coresponding collection doesnot exist')
				return true;
			}

      console.log(`remove ${pkgname} collection from mongodb`)
			db.collection(pkgname).drop();
		} catch (err) {
			throw err;
		}
	});

	return coroutine();
}

module.exports = {
	ManifestManager
}
