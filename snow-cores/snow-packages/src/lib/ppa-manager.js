const co = require('co');
const _ = require('underscore');
const Promise = require('bluebird');
const mongo = require('mongodb');
const MongoClient = Promise.promisifyAll(mongo).MongoClient;
const fs = Promise.promisifyAll(require('fs'));
const exec = require('child_process').exec

const fileScheme = 'file:'

class PpaManager {
	constructor(param) {
		this._param = param;
		this._nemodbPath = param.nemodbPath;
		this._pkgIndexCollection = param.pkgIndexCollection;
		this._pkgIndexFile = param.pkgIndexFile;
	}

	syncList() {
		let self = this;
		let coroutine = co.wrap(function* () {
			const db = yield MongoClient.connectAsync(self._nemodbPath);
			let docs = yield db.collection(self._pkgIndexCollection).find({}).toArrayAsync();
			if (docs.length) {
				// overwrite nemo apt list file
				let data = '';
				for (let doc of docs) {
					if (!doc.type || !doc.host || !doc.dir) {
						continue;
					} 
					data += `${doc.type} ${doc.host} ${doc.dir}\n`;
					if (doc.host.includes(fileScheme)) {
						// create directory in case of local repository
						try {
							yield new Promise((resolve, reject) => {
								exec(`mkdir -p ${doc.host.split(fileScheme)[1]}/${doc.dir}`, (err, stdout, stderr) => {
									if (err) {
										reject(err)
									}
									resolve()
								})
							})
						} catch (err) {
							console.log(err)
						}
					}
				}
				yield fs.writeFileAsync(self._pkgIndexFile, data, { encoding: 'utf8' });
				return { needToCheckRemote: false };
			} 

			let data = yield fs.readFileAsync(self._pkgIndexFile, { encoding: 'utf8' });
			let items = data.split('\n');
			let fileDocs = [];
			for (let item of items) {
				if (item[0] === '#' || (item.indexOf(/\s+/) >= 0)) {
					// this is comment
					continue;
				}
				let doc = _.object(['type', 'host', 'dir'], item.split(/\s+/));
				if (!doc.type || !doc.host || !doc.dir) {
					continue;
				} 
				yield db.collection(self._pkgIndexCollection).saveAsync(doc);
				fileDocs.push(doc);
			}

			return { needToCheckRemote: true, value: fileDocs };
		});

		return coroutine();
	}
}

module.exports = {
	PpaManager
};
