const proc = require('child_process');

setInterval(() => {
	console.log('.');
}, 100);

let args = [
	'../src/index.js',
	'--install',
	'lottetheme'
];

let result = proc.spawnSync('node', args, { stdio: 'inherit' });
if (result.status !== 0) {
	console.error('install fail: ', result.stderr);
} else {
	console.log('install success: ', result.stdout);
}

args = [
	'../src/index.js',
	'--remove',
	'lottetheme'
];

result = proc.spawnSync('node', args, { stdio: 'inherit' });
if (result.status !== 0) {
	console.error('remove fail: ', result.stderr);
} else {
	console.log('remove success: ', result.stdout);
}

args = [
	'../src/index.js',
	'--update-index',
];

result = proc.spawnSync('node', args, { stdio: 'inherit' });
if (result.status !== 0) {
	console.error('update fail: ', result.stderr);
} else {
	console.log('update success: ', result.stdout);
}

