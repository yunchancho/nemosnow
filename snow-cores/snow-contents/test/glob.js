const glob = require('glob');

let options = {
	cwd: '/opt/contents/nemoux',
	absolute: true
}

glob('**/*.@(mkv|mp4|avi|webm)', options, function (err, files) {
	if (err) {
		console.log('err: ', err);
		return false;
	}	
	console.log(files);
	return true;
});
