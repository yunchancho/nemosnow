#!/usr/bin/env node

const co = require('co');
const program = require('commander');
const colors = require('colors');
const { ContentManager } = require('./lib/content-manager');
const metaInfo = require('./meta.json');

program
.option('-s, --sync [source,destination]', 'sync content directories with remote file server', list)
.parse(process.argv)

if (!process.argv.slice(2).length) {
	program.outputHelp((text) => colors.red(text));
	process.exit(1);
}

const config = getConfiguration(metaInfo);
const cm = new ContentManager(config);

let taskroutine = co.wrap(function* () {
	try {
		yield cm.initialize();
		yield handleCmdline(cm, program);
	} catch (err) {
		throw err;
	}
	return 'complete task';
});

taskroutine()
	.then(result => {
		console.log(result)
		process.exit(0)
	})
	.catch(err => {
		console.log(err)
		process.exit(1)
	});

function handleCmdline() {
	let coroutine = co.wrap(function* () {
		if (program.sync) {
			if (program.sync.length) {
				// sync specific sources, destinations from cmdline
				yield cm.syncFromCmdline(program.sync);
			} else {
				// sync source, destination from remote db 
				yield cm.syncFromRemoteDB();
			}
		}
	});

	return coroutine();
}

function getConfiguration(metaInfo) {
	let config = Object.assign({}, metaInfo);

	// set param for package manager
	if (process.env.NEMO_DB_PATH) {
		config.nemodbPath = process.env.NEMO_DB_PATH;
	}

	if (process.env.NEMO_CONTENT_SYNC_COLLECTION) {
		config.contentSyncCollection = process.env.NEMO_CONTENT_SYNC_COLLECTION;
	}

	if (process.env.NEMO_LAUNCH_TYPE) {
		config.launchType = process.env.NEMO_LAUNCH_TYPE;
	}

	if (process.env.NEMO_CONTENT_MJPEG_CONVERT_PROGRAM) {
		config.mjpegConvertProgram = process.env.NEMO_CONTENT_MJPEG_CONVERT_PROGRAM;
	}

	if (process.env.NEMO_CONTENT_BASE_PREFIX) {
		config.contentBasePrefix = process.env.NEMO_CONTENT_BASE_PREFIX;
	}

	return config;
}

function list(val) {
	return val.split(',');
}
