const co = require('co');
const Promise = require('bluebird');
const { spawn } = require('child_process');
const mongo = require('mongodb');
const MongoClient = Promise.promisifyAll(mongo).MongoClient;
const path = require('path');
const glob = require('glob');
const systeminfo = require('systeminformation')

class ContentManager {
	constructor(param) {
		this._param = param;
		this._syncCollection = param.contentSyncCollection;
		this._launchType = param.launchType;
		this._mjpegConvertProgram = param.mjpegConvertProgram;
		this._mjpegExtension = param.mjpegExtension;
		this._dbPath = param.nemodbPath;
		this._destBasePrefix = param.contentBasePrefix;
		this._usbSrcPrefix = param.usbSrcPrefix;
	}

	initialize() {
		return Promise.resolve();
	}

	syncFromCmdline(list) {
		let self = this;
		let coroutine = co.wrap(function* () {
			for (let i = 0; i < list.length; i = i + 2) {
				try {
					let src = list[i]
					if (self._launchType !== 'cloud' && src.includes(self._usbSrcPrefix)) {
						src = yield generateUsbSrcPath(self, list[i])
					}
					yield requestMediaSyncTask(self, src, list[i + 1]);
          // TODO This converting task canb be dependent with use case
          // so we don't run this task as default
					//yield requestMediaConvertTask(self, list[i + 1])
				} catch (err) {
					console.log(err)
				}
			}
		});
		return coroutine();
	}

	syncFromRemoteDB() {
		let self = this;
		let coroutine = co.wrap(function* () {
			const db = yield MongoClient.connectAsync(self._dbPath);
			let docs = yield db.collection(self._syncCollection).find({}).toArrayAsync();
			for (let doc of docs) {
				// In case that src is local path
				try {
					let src = doc.src
					if (self._launchType !== 'cloud' && src.includes(self._usbSrcPrefix)) {
						src = yield generateUsbSrcPath(self, doc.src)
					}
					yield requestMediaSyncTask(self, src, path.join(self._destBasePrefix, doc.dest))
          // TODO This converting task canb be dependent with use case
          // so we don't run this task as default
					//yield requestMediaConvertTask(self, path.join(self._destBasePrefix, doc.dest))
				} catch (err) {
					console.log(err)
				}
			}
		});
		return coroutine();
	}
}

function requestMediaSyncTask(self, src, dest) {
	let program = 'aws';
	if (self._launchType != 'cloud') {
		program = 'rsync';
	}

	let spawnArgs;
	if (program == 'aws') {
		spawnArgs = [ 's3', 'sync', src, dest, '--delete' ];
	} else if (program == 'rsync') {
		// TODO we need to test more rsync
		spawnArgs = [ '-avzh', '--progress', '--delete', `${src}/`, dest ];
	}

	return new Promise((resolve, reject) => {
		let child = spawn(program, spawnArgs, { stdio: 'inherit' });
		child.once('exit', (code) => {
			if (code === 0) {
				resolve();
			} else {
				reject();
			}
		});
	});
}

function requestMediaConvertTask(self, dest) {
	let options = {
		cwd: dest,
		absolute: true
	}

	let coroutine = co.wrap(function* () {
		let founds = yield new Promise((resolve, reject) => {
			glob('**/*.@(mkv|mp4|avi|webm)', options, function (err, files) {
				if (err) {
					console.log('err: ', err);
					reject();
				}	
				resolve(files);
			});
		});
		console.log(founds);
		for (let found of founds) {
			try {
				yield requestMjpegConvertTask(self, found);
			} catch (err) {
				console.log(err);
			}
		}
	});

	return coroutine();
}

function requestMjpegConvertTask(self, file) {
	let program = self._mjpegConvertProgram;

	let dir = path.dirname(file);
	let ext = path.extname(file);
	let filename = path.basename(file, ext);

	let dest = path.join(dir, `${filename}.${self._mjpegExtension}`);
	let spawnArgs = [ '-re', '-i', file, '-q', '1', '-vcodec', 'mjpeg', dest ];

	console.log(`${file} ==> ${dest} : ${spawnArgs}`);
	return new Promise((resolve, reject) => {
		let child = spawn(program, spawnArgs, { stdio: 'inherit' });
		child.once('exit', (code) => {
			if (code === 0) {
				resolve();
			} else {
				reject('failed to convert to mjpeg');
			}
		});
	});
}

function getDefaultUsbMountPath() {
	let coroutine = co.wrap(function* () {
		let devices = yield systeminfo.blockDevices()
		let usbdisks = devices.filter(device => {
			return (device.type === 'disk') && device.removable
		})

		let usbparts = []
		for (let disk of usbdisks) {
			let list = devices.filter(device => {
				return (device.type === 'part') && ~device.name.indexOf(disk.name) && device.mount
			})

			usbparts = usbparts.concat(list.map(part => part.mount))
		}

		console.log(JSON.stringify(usbparts, null, 2))
		return usbparts[0]
	})

	return coroutine()
}

function generateUsbSrcPath(self, src) {
	return co(function* () {
		let usbPath = yield getDefaultUsbMountPath()
		if (!usbPath) {
			throw 'failed to get mounted usb path'
		}
		let subPath = src.split(self._usbSrcPrefix)[1]
		let newPath = `${usbPath}/${subPath}`
		console.log(`usb src path: ${newPath}`)

		return newPath
	})
}


module.exports = {
	ContentManager
}
