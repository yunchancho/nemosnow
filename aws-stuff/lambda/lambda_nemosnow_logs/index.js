'use strict'
let AWS = require('aws-sdk');

const meta = {
	userPoolId: process.env.USER_POOL_ID,
	attributes: {
		permission: process.env.ATTRIBUTE_PERMISSION,
		customer: process.env.ATTRIBUTE_CUSTOMER,
	},
  logGroup: process.env.LOG_GROUP_NAME,
	ops: process.env.OPERATORS.split('/')
}

let cloudWatchLogs = new AWS.CloudWatchLogs();
let cognitoProvider = new AWS.CognitoIdentityServiceProvider();

function getCognitoAttributes(token) {
	let params = {
		AccessToken: token
	}
  let username

	return cognitoProvider.getUser(params).promise()
	.then(result => {
		params = {
			UserPoolId: meta.userPoolId,
			Username: result.Username
		};
    username = result.Username
		// Call adminGetUser to get hidden(permission) attributes
		return cognitoProvider.adminGetUser(params).promise()
	})
	.then(result => {
		console.log('Access user: ', result);
		let attributes = {}
		for (let attribute of result.UserAttributes) {
			attributes[attribute.Name] = attribute.Value;
		}
		return {username, attributes}
	})
}

function getLogs(accessToken) {
  return getCognitoAttributes(accessToken)
  .then(result => {
		let username = result.username;
    let customer = result.attributes[meta.attributes.customer];
		
    let params = {
      logGroupName: meta.logGroup,
      logStreamNames: [ customer ],
			filterPattern: `{ $.username = ${username} }`,
			limit: 1000
    }
		console.log(`get logs: ${JSON.stringify(params)}`)
    
    return cloudWatchLogs.filterLogEvents(params).promise()
    .then(result => {
      console.log(`get log result: ${JSON.stringify(result)}`)
      return result
    })
    .catch(err => {
      console.error(`get log error: ${JSON.stringify(err)}`)
      throw new Error('ERROR_LAMBDA_LOG_INTERNAL_ERROR')
    })
  })
}

// Log structure
// {
//   status: inserted by client,
//   customer: inserted by lambda,
//   prefix: inserted by client,
//   username: inserted by lambda,
//   message: inserted by client {
//     menu,
//     target,
//     ops
//   }
// }
function putLog(accessToken, log) {
	let customer;
	let permission;
	
	let validOps = meta.ops.filter(operator => {
		return operator === log.message.ops;
	})
	if (validOps.length !== 1) {
		return Promise.reject(new Error('ERROR_LAMBDA_LOG_INVALID_OPERATOR'))
	}
	
  return getCognitoAttributes(accessToken)
  .then(result => {
    customer = result.attributes[meta.attributes.customer];
    permission = result.attributes[meta.attributes.permission];
    
		log.prefix = permission;
    log.customer = customer;
    log.username = result.username;
		
		let params = {
			logGroupName: meta.logGroup,
			logStreamNamePrefix: customer,
			orderBy: 'LogStreamName',
		}
		return cloudWatchLogs.describeLogStreams(params).promise()
	})
	.then(result => {
		console.log(`Log stream: ${JSON.stringify(result)}`)
		if (result.logStreams.length === 0) {
			// Log stream doesn't exist. create and redescribe stream
			let params = {
				logGroupName: meta.logGroup,
				logStreamName: customer,
			}
			return cloudWatchLogs.createLogStream(params).promise()
			.then(() => {
				let params = {
					logGroupName: meta.logGroup,
					logStreamNamePrefix: customer,
					orderBy: 'LogStreamName',
				}
				return cloudWatchLogs.describeLogStreams(params).promise()
			})
			// throw new Error('ERROR_LAMBDA_LOG_NO_LOG_STREAM')
		} else {
			return result
		}
	})
	.then(result => {
    let params = {
      logEvents: [
        {
          message: JSON.stringify(log),
          timestamp: new Date().getTime()
        }
      ],
      logGroupName: meta.logGroup,
      logStreamName: customer,
			sequenceToken: result.logStreams[0].uploadSequenceToken
    }
		console.log(`put log: ${JSON.stringify(params)}`)
    
    return cloudWatchLogs.putLogEvents(params).promise()
    .then(result => {
      console.log(`put log result: ${JSON.stringify(result)}`)
      return { result: true }
    })
    .catch(err => {
      console.error(`put log error: ${JSON.stringify(err)}`)
      throw new Error('ERROR_LAMBDA_LOG_INTERNAL_ERROR')
    })
  })
}

exports.handler = (event, context, callback) => {
	console.log('clientContext: ', context.clientContext);
	let prom;

	if (context.clientContext.api === 'getLogs') {
		prom = getLogs(context.clientContext.accessToken)
	} else if (context.clientContext.api === 'putLog') {
		prom = putLog(context.clientContext.accessToken, context.clientContext.log)
	} else {
		console.log(context.clientContext.api, ' called')
		return callback(new Error('ERROR_LAMBDA_LOG_INVALID_API'))
	}

	prom.then(result => callback(null, result))
	.catch(err => {
		console.error(err)
		if (err.message.startsWith('ERROR_LAMBDA_LOG_')) {
			return callback(err)
		} else {
			callback(new Error('ERROR_LAMBDA_LOG_INTERNAL_ERROR'))
		}
	});
	return;
};
