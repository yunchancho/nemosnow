'use strict'

const AWS = require('aws-sdk');
const Promise = require('bluebird');

const meta = {
	bucket: {
		backup: process.env.BUCKET_BACKUP
	},
	userPoolId: process.env.USER_POOL_ID,
	attributes: {
		customer: process.env.ATTRIBUTE_CUSTOMER,
	}
}

const cognitoProvider = new AWS.CognitoIdentityServiceProvider();
const docClient = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient());
const s3 = new AWS.S3();

function getCustomer(token) {
	let params = {
		AccessToken: token
	}

	return cognitoProvider.getUser(params).promise()
	.then(result => {
		params = {
			UserPoolId: meta.userPoolId,
			Username: result.Username
		};
		return cognitoProvider.adminGetUser(params).promise()
	})
	.then(result => {
		console.log('Access user: ', result);
		for (let attribute of result.UserAttributes) {
			if (attribute.Name === meta.attributes.customer) {
				return attribute.Value
			}
		}
		// Permission not assigned
		throw new Error('ERROR_LAMBDA_SYNC_DEVICE_CUSTOMER_NOT_ASSIGNED')
	})
}

// Backup DynamoDB Config to s3 bucket
function backupConfig(context) {
	let token = context.token;
	let swKey = context.swKey;

	let params = {}
	let table = {}
	let customer = ''
	let utc = new Date().getTime() + (9 * 60 * 60000) // Korea Standard Time
	let now = new Date(utc).toISOString().replace(/T/, '_').replace(/:/g, '-').replace(/\..+/g, '')
	console.log(`TimeString: ${now}`)

	return getCustomer(token)
	.catch(err => {
		console.log(err)
		throw new Error('ERROR_LAMBDA_BACKUP_DEVICE_GET_CUSTOMER')
	})
	.then(result => {
		customer = result
		if (!swKey.startsWith(`${customer}_`)) {
			throw new Error('ERROR_LAMBDA_BACKUP_DEVICE_PERMISSION_DENIED')
		}

		params = {
			TableName: swKey
		}
		return docClient.scan(params).promise()
	})
	.then(result => {
		// Create backup folder
		table = result.Items;
		console.log(`DB items: ${JSON.stringify(table)}`)

		params = {
			Bucket: meta.bucket.backup,
			Key: `${customer}/${swKey}/configs/${now}/`,
			Body: 'folder'
		}
		let options = {
			partSize: 10 * 1024 * 1024,
			queueSize: 1
		}

		return s3.upload(params, options).promise()
	})
	.then(result => {
		// Upload files
		let requests = []
		for (let item of table) {
			let uploadParams = {
				Bucket: meta.bucket.backup,
				Key: `${customer}/${swKey}/configs/${now}/${item.name}.json`,
				Body: JSON.stringify(item, null, 2)
			}
			let options = {
				partSize: 10 * 1024 * 1024,
				queueSize: 1
			}

			requests.push(s3.upload(uploadParams, options).promise())
		}
		return Promise.all(requests)
	})
	.then(result => {
		return { result: true }
	})
	.catch(err => {
		if (!err.message.startsWith('ERROR_LAMBDA')) {
			console.log(err)
			throw new Error('ERROR_LAMBDA_BACKUP_DEVICE_BACKUP_CONFIG')
		} else {
			throw err
		}
	})
}

// Restore Config from s3 backups bucket to dynamodb device table
function restoreConfig(context) {
	let token = context.token;
	let swKey = context.swKey;
	let prefix = context.prefix;

	let params = {}
	let customer = ''
	let datas = []

	return getCustomer(token)
	.catch(err => {
		console.log(err)
		throw new Error('ERROR_LAMBDA_BACKUP_DEVICE_GET_CUSTOMER')
	})
	.then(result => {
		customer = result
		if (!swKey.startsWith(`${customer}_`)) {
			throw new Error('ERROR_LAMBDA_BACKUP_DEVICE_PERMISSION_DENIED')
		}

		// Get backup item list
		params = {
			Bucket: meta.bucket.backup,
			Prefix: prefix,
			Delimiter: '/'
		}

		return s3.listObjectsV2(params).promise()
	})
	.then(datas => {
		// Remove itself
		for (let i = 0; i < datas.Contents.length; i++) {
			if (datas.Contents[i].Key === prefix) {
				datas.Contents.splice(i, 1);
			}
		}

		// Get backup items(json)
		let requests = datas.Contents.map(item => {
			let getParams = {
	      Bucket: meta.bucket.backup,
	      Key: item.Key,
	      ResponseCacheControl: 'no-cache'
			}
			return s3.getObject(getParams).promise()
		})
		return Promise.all(requests);
	})
	.then(result => {
		datas = result.map(item => JSON.parse(item.Body.toString()))
		console.log(`backup items: ${JSON.stringify(datas)}`)
		return overwriteDB(swKey, datas)
	})
	.then(result => {
		console.log(`RESULT: ${JSON.stringify(result)}`)
		return {
			result: true,
			items: datas
		}
	})
	.catch(err => {
		if (!err.message.startsWith('ERROR_LAMBDA')) {
			console.log(err)
			throw new Error('ERROR_LAMBDA_BACKUP_DEVICE_RESTORE_CONFIG')
		} else {
			throw err
		}
	})
}

// Restore Config from s3 backups bucket to dynamodb device table
function restoreConfigLocal(context) {
	let token = context.token;
	let swKey = context.swKey;
	let items = context.items;

	let params = {}

	return getCustomer(token)
	.catch(err => {
		console.log(err)
		throw new Error('ERROR_LAMBDA_BACKUP_DEVICE_GET_CUSTOMER')
	})
	.then(customer => {
		if (!swKey.startsWith(`${customer}_`)) {
			throw new Error('ERROR_LAMBDA_BACKUP_DEVICE_PERMISSION_DENIED')
		}
	})
	.then(result => {
		console.log(`backup items: ${JSON.stringify(items)}`)
		return overwriteDB(swKey, items)
	})
	.then(result => {
		console.log(`RESULT: ${JSON.stringify(result)}`)
		return {
			result: true,
			items
		}
	})
	.catch(err => {
		if (!err.message.startsWith('ERROR_LAMBDA')) {
			console.log(err)
			throw new Error('ERROR_LAMBDA_BACKUP_DEVICE_RESTORE_CONFIG')
		} else {
			throw err
		}
	})
}

// Make delete request by comparing target and source device table
function getDeleteRequest(device, source) {
	let params = {
		TableName: device
	}
	return docClient.scan(params).promise()
	.then(result => {
		let deleteRequests = result.Items.filter(item => {
			for (let src of source) {
				if (src.name === item.name) {
					return false;
				}
			}
			return true;
		})
		.map(item => {
			return {
				DeleteRequest: {
					Key: {
						'name': item.name
					}
				}
			}
		})

		return deleteRequests;
	})
}

function overwriteDB(tableName, items) {
	if (items.length === 0) {
		throw new Error('ERROR_LAMBDA_BACKUP_DEVICE_NO_BACKUP_DATA')
	}

	let putRequests = items.map(item => {
		return {
			PutRequest: {
				Item: item
			}
		}
	})
	// Make requests
	return getDeleteRequest(tableName, items)
	.then(deleteRequests => {
		let requestItems = deleteRequests.concat(putRequests)
		let limit = 25;
		let requests = []

		for (let i = 0; i < requestItems.length; i += limit) {
			let params = {
				RequestItems: {}
			}
			params.RequestItems[tableName] = requestItems.slice(i, i + limit)
			requests.push(docClient.batchWriteAsync(params))
		}
		console.log(`batchwrite request Items: ${JSON.stringify(requestItems)}`)
		return Promise.all(requests)
	})
}

exports.handler = (event, context, callback) => {
	console.log('args: ', context.clientContext)
	let prom;
	if (context.clientContext.api === 'backupConfig') {
		prom = backupConfig(context.clientContext)
	} else if (context.clientContext.api === 'restoreConfig') {
		prom = restoreConfig(context.clientContext)
	} else if (context.clientContext.api === 'restoreConfigLocal') {
		prom = restoreConfigLocal(context.clientContext)
	} else {
		console.log(context.clientContext.api, ' called')
		prom = Promise.reject(new Error('ERROR_LAMBDA_BACKUP_DEVICE_INVALID_API'))
	}

	prom.then(result => callback(null, result))
	.catch(err => callback(err))
}
