'use strict';

const path = require('path');
const AWS = require('aws-sdk');
const co = require('co');
const Promise = require('bluebird');
const proc = require('child_process')
const url = require('url');
const policy = require('./policy');

let s3 = Promise.promisifyAll(new AWS.S3());
let lambda = new AWS.Lambda()
let dynamodb = Promise.promisifyAll(new AWS.DynamoDB());
let dynamodoc = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient());
let iam = Promise.promisifyAll(new AWS.IAM({ apiVersion: '2010-05-08' }));
let cognitoIdentity = Promise.promisifyAll(new AWS.CognitoIdentity({ apiVersion: '2014-06-30' }));
let cloudWatchLogs = new AWS.CloudWatchLogs();

let defaultCollectionValue = {}

const contentSyncDefaultDir = 'default'

function getDefaultPkgindex(meta) {
	return [{
		host: `s3://s3-${meta.bucketRegion}.amazonaws.com/${meta.bucket.package}`,
		dir: 'system/',
		type: 'deb',
		public: true
	}, {
		host: `s3://s3-${meta.bucketRegion}.amazonaws.com/${meta.bucket.package}`,
		dir: 'shell/',
		type: 'deb',
		public: true
	}, {
		host: `s3://s3-${meta.bucketRegion}.amazonaws.com/${meta.bucket.package}`,
		dir: 'theme/',
		type: 'deb',
		public: true
	}, {
		host: `s3://s3-${meta.bucketRegion}.amazonaws.com/${meta.bucket.package}`,
		dir: 'application/',
		type: 'deb',
		public: true
	}];
}

function getDefaultContentsync(meta) {
	return [{
		name: meta.defaultContentName,
		src: `s3://${meta.bucket.content}/${meta.name}/${contentSyncDefaultDir}`,
		dest: `/${contentSyncDefaultDir}`
	}];
}

function getDefaultBigdata(meta) {
	return [{
		bucket: meta.bucket.bigdata,
		region: meta.bucketRegion,
		prefix: `${meta.name}/`
	}];
}

function getDefaultMandatoryPkgs() {
  return []
}

function getDefaultTheme() {
  return defaultCollectionValue.theme || {}
}

function getDefaultShellConfig() {
  return defaultCollectionValue.shell || {}
}

function getDefaultPower(meta) {
	let week = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']
	return week.map(day => {
		return { day }
	})
}

function getS3Params(meta, type) {
	let param = {};

	if (type == 'content') {
		param.Bucket = meta.bucket.content;
		param.Key = `${meta.name}/`;
	} else if (type == 'bigdata') {
		param.Bucket = meta.bucket.bigdata;
		param.Key = `${meta.name}/`;
	} else if (type == 'package') {
		param.Bucket = meta.bucket.package;
		param.Key = `customer/${meta.name}/`;
	} else if (type == 'backup') {
		param.Bucket = meta.bucket.backup;
		param.Key = `${meta.name}/`;
	} else {
		console.log('unknown type for s3 params');
	}

	return param;
}

function removeS3Folder(s3path) 
{
	let parseObject = url.parse(s3path);

	console.log('parsed url object: ', parseObject);

	let params = {
		Bucket: parseObject.hostname,
		Prefix: `${parseObject.pathname.slice(1)}/`
	}
	
	let coroutine = co.wrap(function* () {
		let result = yield s3.listObjectsAsync(params);
		if (!result.Contents.length) {
			return;
		}
		params = {
			Bucket: parseObject.hostname,
			Delete: {
				Objects: []
			}
		}

		for (let content of result.Contents) {
			console.log('Deleted object key: ', content.Key);
			params.Delete.Objects.push({ Key: content.Key });
		}

		try {
			result = yield s3.deleteObjectsAsync(params);
			console.log('success to delete objects: ', result.Deleted.length);
		} catch (e) {
			console.log(`fail to delete folder: ${e}`);
		}
	});

	return coroutine();
}

function createRole(meta) {
	let coroutine = co.wrap(function* () {
		let params = {};
		let result = null;
		let roleArn = null;
		
		// create customer role with trust relationship
		try {
			let trustRelationship = policy.getTrustRelationship(meta.cognito.identityPoolId);
			
			params = {
				AssumeRolePolicyDocument: JSON.stringify(trustRelationship),
				RoleName: `${meta.rolePrefix}_${meta.name}`
			};
			
			result = yield iam.createRoleAsync(params);
			roleArn = result.Role.Arn;
		} catch (e) {
			console.log(`fail to create role: ${e}`)
		}
		
		// create inline policies
		try {
			for (let inlinePolicy of policy.getInlinePolicies(meta)) {
				params = {
					PolicyDocument: JSON.stringify(inlinePolicy.document),
				  PolicyName: inlinePolicy.name,
				  RoleName: `${meta.rolePrefix}_${meta.name}`
				}
				yield iam.putRolePolicyAsync(params);
			}
		} catch (e) {
			console.log(`fail to put inline policies: ${e}`);
		}
		
		// attach managed policies
		try {
			for (let managedPolicy of meta.cognito.managedPolicies) {
				params = {
					PolicyArn: managedPolicy, 
					RoleName: `${meta.rolePrefix}_${meta.name}`
				}
				yield iam.attachRolePolicyAsync(params);
			}
		} catch (e) {
			console.log(`fail to attach managed policies: ${e}`);
		}
		
		// add federated identity rule
		// TODO: concurrency problem can occur
		try {
			params = {
				IdentityPoolId: meta.cognito.identityPoolId
			}
			result = yield cognitoIdentity.getIdentityPoolRolesAsync(params);
			let rules = result.RoleMappings[`cognito-idp.${meta.cognito.region}.amazonaws.com/${meta.cognito.userPoolId}:${meta.cognito.clientId}`].RulesConfiguration.Rules;
			console.log('IdentityPool Roles:', JSON.stringify(rules))
			let rule = {
        Claim: meta.cognito.attribute.customer,
        MatchType: 'Equals',
        RoleARN: roleArn,
        Value: meta.name
			}
			rules.push(rule);
			
			yield cognitoIdentity.setIdentityPoolRolesAsync(result)
		} catch (e) {
			console.log(`fail to add federated identity rule: ${e}`);
		}
	});
	
	return coroutine();
}

function createIamUser(userName, iamGroup) {
	let coroutine = co.wrap(function* () {
		let params = {};
		let result = null;

		try {
			params = {
				UserName: userName
			};
			result = yield iam.getUserAsync(params);
			console.log('exists user: ', result);
			yield removeIamUser(userName);
		} catch (e) {
			console.log(`not existing user: ${e}`);
		}

		try {
			params = {
				UserName: userName
			};
			yield iam.createUserAsync(params);
		} catch (e) {
			console.log(`fail to create user: ${e.stack}`);
		}

		try {
			params = {
				GroupName: iamGroup,
				UserName: userName
			};
			yield iam.addUserToGroupAsync(params);
		} catch (e) {
			console.log(`fail to add user to group: ${e.stack}`);
		}

		try {
			params = {
				UserName: userName
			};
			result = yield iam.listAccessKeysAsync(params);
			console.log(`access key list: ${result}`);
			for (let key of result.AccessKeyMetadata) {
				try {
					params = {
						AccessKeyId: key.AccessKeyId,
						UserName: userName
					};
					yield iam.deleteAccessKeyAsync(params);
					console.log(`success to remove key: ${key.AccessKeyId}`);
				} catch (e) {
					console.log(`fail to remove key(${key.AccessKeyId}): ${e}`);
				}
			}

			params = {
				UserName: userName
			};
			result = yield iam.createAccessKeyAsync(params);
			console.log(`create key: ${result}`);
			return {
				awskey: {
					accessKeyId: result.AccessKey.AccessKeyId,
					secretAccessKey: result.AccessKey.SecretAccessKey
				}
			};
		} catch (e) {
			console.log(`fail to set aws key: ${e.stack}`);
			return {
				awskey: {}
			}
		}
	});

	return coroutine();
}

function removeIamUser(userName) {
	let coroutine = co.wrap(function* () {
		let params = {};
		let result = null;

		// remove user from its groups
		params = {
			UserName: userName
		};
		result = yield iam.listGroupsForUserAsync(params);
		for (let group of result.Groups) {
			try {
				params = {
					GroupName: group.GroupName,
					UserName: userName 
				};
				yield iam.removeUserFromGroupAsync(params);
				console.log(`success to remove user from group: ${group.GroupName}`);
			} catch (e) {
				console.log(`fail to remove user from group(${group.GroupName}): ${e}`);
			}
		}

		// remove user access keys
		params = {
			UserName: userName
		};
		result = yield iam.listAccessKeysAsync(params);
		console.log(`access key list: ${result}`);
		for (let key of result.AccessKeyMetadata) {
			try {
				params = {
					AccessKeyId: key.AccessKeyId,
					UserName: userName
				};
				yield iam.deleteAccessKeyAsync(params);
				console.log(`success to remove key: ${key.AccessKeyId}`);
			} catch (e) {
				console.log(`fail to remove key(${key.AccessKeyId}): ${e}`);
			}
		}

		//TODO we need to check this user has certificates and (inline) polices
		//if so, remove those.

		// remove iam user finally
		params = {
			UserName: userName
		};
		yield iam.deleteUserAsync(params);
		console.log(`success to remove iam user: ${userName}`);
	});

	return coroutine();
}

function createLambdaBigdataTrigger(meta) {
  let coroutine = co.wrap(function* () {
		let params = {};
		let result = null;

    const arns = meta.lambdaS3ToEsArn.trim().split(':')
    params = {
      Action: "lambda:InvokeFunction",
      FunctionName: arns[arns.length - 1],
      Principal: "s3.amazonaws.com",
      SourceAccount: arns[4],
      SourceArn: `arn:aws:s3:::${meta.bucket.backup}`,
      StatementId: meta.bucket.backup
    }

    try {
      yield new Promise((resolve, reject) => {
        lambda.addPermission(params, (err, data) => {
          if (err) {
            console.log(err, err.stack)
            return reject(err)
          }
          console.log(data)
          resolve()
        })
      })
    } catch (err) {
      console.log(err)
    }

    params = {
      Bucket: meta.bucket.backup
    }
    let existingTriggers = yield new Promise((resolve, reject) => {
      s3.getBucketNotificationConfiguration(params, (err, data) => {
        if (err) {
          console.log(err, err.stack)
          return reject(err)
        }
        console.log(data)
        resolve(data.LambdaFunctionConfigurations)
      })
    })
   
    console.log(existingTriggers)
  
    params = {
      Bucket: meta.bucket.backup,
      NotificationConfiguration: {
        LambdaFunctionConfigurations: [
          {
            Events: [ "s3:ObjectCreated:*" ],
            LambdaFunctionArn: `${meta.lambdaS3ToEsArn}`,
            Filter: {
              Key: {
                FilterRules: [
                  {
                    Name: "prefix",
                    Value: getS3Params(meta, 'backup').Key
                  },
                  {
                    Name: "suffix",
                    Value: "gz"
                  }
                ]
              }
            },
            Id: meta.name
          }
        ].concat(existingTriggers)
      }
    }
    console.log(JSON.stringify(params, null, 2))
    yield new Promise((resolve, reject) => {
      s3.putBucketNotificationConfiguration(params, (err, data) => {
        if (err) {
          console.log(err, err.stack)
          return reject(err)
        }
        console.log(data)
        resolve()
      })
    })
  })

  return coroutine()
}

function removeLambdaBigdataTrigger(meta) {
  let coroutine = co.wrap(function* () {
		let params = {};
		let result = null;

    /* 
     * This is util code for removing unnecessary lambda permission 
     *
    try {
      const tests = ['s3lambdainvoke']
      for (let test of tests) {
        const arns = meta.lambdaS3ToEsArn.split(':')
        params = {
          FunctionName: arns[arns.length -1],
          StatementId: test
        }

        yield new Promise((resolve, reject) => {
          lambda.removePermission(params, (err, data) => {
            if (err) {
              console.log(err, err.stack)
              return reject(err)
            }
            console.log(data)
            resolve()
          })
        })
      }
    } catch (err) {
      console.log(err)
    }
    */

    params = {
      Bucket: meta.bucket.backup
    }
    let existingTriggers = yield new Promise((resolve, reject) => {
      s3.getBucketNotificationConfiguration(params, (err, data) => {
        if (err) {
          console.log(err, err.stack)
          return reject(err)
        }
        console.log(data)
        resolve(data.LambdaFunctionConfigurations)
      })
    })

    let triggers = existingTriggers.filter((trigger) => trigger.Id !== meta.name)
   
    console.log(existingTriggers)
    params = {
      Bucket: meta.bucket.backup,
      NotificationConfiguration: {
        LambdaFunctionConfigurations: triggers
      }
    }
    console.log(JSON.stringify(params, null, 2))
    yield new Promise((resolve, reject) => {
      s3.putBucketNotificationConfiguration(params, (err, data) => {
        if (err) {
          console.log(err, err.stack)
          return reject(err)
        }
        console.log(data)
        resolve()
      })
    })
  })

  return coroutine()
}

function getDefaultCollectionValue(meta) {
	let coroutine = co.wrap(function* () {
    const params = {
      TableName: meta.registryTable,
      Key: {
        'name': 'infra'
      }
    };

    let collectionValue = {}
    try {
      let registry = yield dynamodoc.getAsync(params);
      console.log('registry: ', registry)
      collectionValue = registry.Item
    } catch (err) {
      console.log(err)
    }

    return collectionValue
  })

  return coroutine()
}

function copyS3ToS3(srcBucket, srcDirKey, destBucket, destDirKey) {
  const coroutine = co.wrap(function* () {
    let data = yield s3.listObjectsAsync({ Bucket: srcBucket, Prefix: srcDirKey })
    for (let content of data.Contents) {
      let param = {
        Bucket: destBucket,
        Key: `${destDirKey}${content.Key}`,
        CopySource: `${srcBucket}/${content.Key}`
      }
      try {
        let result = yield s3.copyObjectAsync(param)
      } catch (err) {
        console.log(err)
      }
    }
  })

  return coroutine()
}

function copyCommonResources(meta, commonResource) {
	let coroutine = co.wrap(function* () {
    let params = getS3Params(meta, 'content');
    params.Key += `${contentSyncDefaultDir}/`
    yield copyS3ToS3(commonResource.bucket, commonResource.themeDirKey, params.Bucket, params.Key)
    yield copyS3ToS3(commonResource.bucket, commonResource.mediaDirKey, params.Bucket, params.Key)
    yield copyS3ToS3(commonResource.bucket, commonResource.splashscreenDirKey, params.Bucket, params.Key)
  })

  return coroutine()
}


function initialize(meta) {
	AWS.config.update({
	 	region: meta.region
 	});


	let coroutine = co.wrap(function* () {
		let params = {};
		let result = null;

		try {
      // 0. get common default values from Registry table
      defaultCollectionValue = yield getDefaultCollectionValue(meta)

			// 1. create contents folder with default in bucket
			params = getS3Params(meta, 'content');
			params.Key += `${contentSyncDefaultDir}/`
			yield s3.putObjectAsync(params);

			// 2. create bigdata log folder in bucket
			params = getS3Params(meta, 'bigdata');
			yield s3.putObjectAsync(params);

			// 3. create apt package repo in bucket
			params = getS3Params(meta, 'package');
			yield s3.putObjectAsync(params);

			// 4. create backup repo in bucket
			params = getS3Params(meta, 'backup');
			yield s3.putObjectAsync(params);
		} catch (e) {
			console.log(`fail to create folder: ${e.stack}`);
		}

    // 5. add s3 bigdata trigger to lambda
    yield createLambdaBigdataTrigger(meta)

		// 6. create iam user for this customer
		result = yield createIamUser(meta.name, meta.iamGroup);
		meta.awskey = result.awskey;

		// check if customer table exists
		try {
			params = { TableName: meta.customerTable };
			yield dynamodb.describeTableAsync(params);
		} catch (e) {
			console.log(`fail to read customer table: ${e}`);
		}

		// update existing item of this customer
		meta.pkgindex = getDefaultPkgindex(meta);
		meta.contentsync = getDefaultContentsync(meta);
		meta.bigdata = getDefaultBigdata(meta);
		meta.power = getDefaultPower(meta)
    meta.mandatorypkgs = getDefaultMandatoryPkgs()
    meta.theme = getDefaultTheme()
    meta.shell = getDefaultShellConfig()

		params = {
			TableName: meta.customerTable,
			Key: {
				"name": meta.name
			},
      UpdateExpression: "set pkgindex = :p, contentsync = :c, bigdata = :b, mandatorypkgs = :m, awskey = :k, prefixmeta = :f, power = :pw, theme = :t, shell = :s",
			ExpressionAttributeValues: {
				":p": meta.pkgindex,
				":c": meta.contentsync,
				":b": meta.bigdata,
				":m": meta.mandatorypkgs,
				":k": meta.awskey,
				":f": [],
				":pw": meta.power,
        ":t": meta.theme,
        ":s": meta.shell
			},
			ReturnValues: "UPDATED_NEW"
		};

		try {
			result = yield dynamodoc.updateAsync(params);
			console.log('update item: ', JSON.stringify(result, null, 2));
		} catch (e) {
			console.log(`fail to update item: ${e}`);
		}
		
		// 5. create IAM role
		yield createRole(meta);

		// 6. create CloudWatch log stream
		params = {
			logGroupName: meta.logGroup,
			logStreamName: meta.name,
		}
		yield cloudWatchLogs.createLogStream(params).promise()

    // copy default common used resources
    // This may takes long time to be processed by big size data
    copyCommonResources(meta, defaultCollectionValue.meta.commonResource)
	});

	return coroutine();
}

function deinitialize(meta) {
	AWS.config.update({
	 	region: meta.region
 	});

	let coroutine = co.wrap(function* () {
		// remove contents s3 folder
		let params = {};
		let result = null;
		for (let item of meta.contentsync) {
			console.log('contentsync item: ', item);
			yield removeS3Folder(item.M.src.S);
		}

		// remove bigdata s3 folder
		for (let item of meta.bigdata) {
			console.log('bigdata item: ', item);
			yield removeS3Folder(`s3://${item.M.bucket.S}/${item.M.prefix.S.slice(0, -1)}`);
		}

		// remove apt repository s3 folder
		/* This SHOULD NOT RUN because this customer might use some group's s3 apt repo.
		 *
		let privatePkgindex = meta.pkgindex.filter(item => !item.M.public.BOOL)
		for (let item of privatePkgindex) {
			console.log('private package item: ', item);
			let bucket = item.M.host.S.split('/')[-1]
			yield removeS3Folder(`s3://${bucket}/${item.M.dir.S.slice(0, -1)}`);
		}
		*/
		params = getS3Params(meta, 'package');
		yield removeS3Folder(`s3://${params.Bucket}/${params.Key.slice(0, -1)}`)

		params = getS3Params(meta, 'backup');
		yield removeS3Folder(`s3://${params.Bucket}/${params.Key.slice(0, -1)}`)

    yield removeLambdaBigdataTrigger(meta)

		// remove iam user
		try {
			yield removeIamUser(meta.name);
		} catch (e) {
			console.log(`fail to remove user: ${e}`);
		}
		
		// remove iam role
		try {
			// list attached(managed) policies
			params = {
			  RoleName: `${meta.rolePrefix}_${meta.name}`,
			}
			result = yield iam.listAttachedRolePoliciesAsync(params);
			
			// detach attached policies
			for (let attachedPolicy of result.AttachedPolicies) {
				params = {
					PolicyArn: attachedPolicy.PolicyArn,
				  RoleName: `${meta.rolePrefix}_${meta.name}`
				}
				yield iam.detachRolePolicyAsync(params);
			}
			
			// list inline policies
			params = {
			  RoleName: `${meta.rolePrefix}_${meta.name}`,
			}
			result = yield iam.listRolePoliciesAsync(params);
			
			// delete inline policies
			for (let PolicyName of result.PolicyNames) {
				params = {
					PolicyName,
				  RoleName: `${meta.rolePrefix}_${meta.name}`
				}
				yield iam.deleteRolePolicyAsync(params);
			}
			
			// delete role
			params = {
				RoleName: `${meta.rolePrefix}_${meta.name}`
			}
			yield iam.deleteRoleAsync(params);
		} catch (e) {
			console.log(`fail to remove role: ${e}`);
		}
		
		// remove identity pool rule
		try {
			params = {
				IdentityPoolId: meta.cognito.identityPoolId
			}
			result = yield cognitoIdentity.getIdentityPoolRolesAsync(params);
			let rules = result.RoleMappings[`cognito-idp.${meta.cognito.region}.amazonaws.com/${meta.cognito.userPoolId}:${meta.cognito.clientId}`].RulesConfiguration.Rules;
			console.log('IdentityPool Roles:', JSON.stringify(rules))
			for (let i = 0; i < rules.length; i++) {
				if (rules[i].Value === `${meta.name}`) {
					rules.splice(i, 1);
					break;
				}
			}
					
			yield cognitoIdentity.setIdentityPoolRolesAsync(result)

			// delete log stream
			params = {
				logGroupName: meta.logGroup,
				logStreamName: meta.name,
			}
			yield cloudWatchLogs.deleteLogStream(params).promise()
		} catch(e) {
			console.log(`fail to remove identity poll rule: ${e}`)
		}
 	});

	return coroutine();
}

module.exports = {
	initialize,
	deinitialize
}
