'use strict';

const customer = require('./customer');

exports.handler = function(event, context) {
	let params = {
		name: null,
		customerTable: process.env.CUSTOMER_TABLE,
		registryTable: process.env.REGISTRY_TABLE,
		bucketRegion: process.env.BUCKET_REGION,
	 	bucket: {
			package: process.env.PACKAGE_BUCKET,
			content: process.env.CONTENT_BUCKET,
			bigdata: process.env.BIGDATA_BUCKET,
			backup: process.env.BACKUP_BUCKET
		},
		pkgindex: [],
		contentsync: [],
		defaultContentName: process.env.DEFAULT_CONTENT_NAME,
		bigdata: [],
		iamGroup: process.env.IAM_DEVICE_GROUP,
		awskey: {},
		cognito: {
			clientId: process.env.IDENTITY_CLIENT_ID,
			identityPoolId: process.env.IDENTITY_POOL_ID,
			region: process.env.COGNITO_REGION,
			managedPolicies: process.env.MANAGED_POLICIES.split(','),
			userPoolId: process.env.USER_POOL_ID,
			attribute: {
				customer: process.env.CUSTOMER_ATTRIBUTE
			}
		},
		iotArn: process.env.IOT_ARN,
    lambdaS3ToEsArn: process.env.LAMBDA_S3_TO_ES_ARN,
		dynamoDBArn: process.env.DYNAMODB_ARN,
		rolePrefix: process.env.ROLE_PREFIX,
		logGroup: process.env.LOG_GROUP
	};


	for (let record of event.Records) {
		console.log(`record: ${JSON.stringify(record)}`);
		params.name = record.dynamodb.Keys.name.S;
		console.log('passed params: ', params);
		if (record.eventName == 'INSERT') {
			customer.initialize(params)
		} else if (record.eventName == 'REMOVE') {
			if (record.dynamodb.OldImage.contentsync) {
				params.contentsync = record.dynamodb.OldImage.contentsync.L;
			}
			if (record.dynamodb.OldImage.bigdata) {
				params.bigdata = record.dynamodb.OldImage.bigdata.L;
			}
			if (record.dynamodb.OldImage.pkgindex) {
				params.pkgindex = record.dynamodb.OldImage.pkgindex.L;
			}
			customer.deinitialize(params)
		} else {
			console.log(`unhandled event name: ${record.eventName}`);
		}
	}
}
