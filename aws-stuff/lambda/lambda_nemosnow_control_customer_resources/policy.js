'use strict'

function getAccessDeviceTable(meta) {
  return {
    name: "AccessDeviceTable",
    document: {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": [
            "dynamodb:*"
          ],
          "Effect": "Allow",
          "Resource": `${meta.dynamoDBArn}/${meta.name}_*`
        }
      ]
    }
  }
}

function getIotFullAccess(meta) {
  return {
    name: "IotFullAccess",
    document: {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Sid": "Stmt1485414481000",
          "Effect": "Allow",
          "Action": [
            "iot:*"
          ],
          "Resource": [
            `${meta.iotArn}/${meta.name}_*`
          ]
        }
      ]
    }
  }
}

function getTrustRelationship(identityPoolId) {
  return {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "Federated": "cognito-identity.amazonaws.com"
        },
        "Action": "sts:AssumeRoleWithWebIdentity",
        "Condition": {
          "StringEquals": {
            "cognito-identity.amazonaws.com:aud": identityPoolId
          },
          "ForAnyValue:StringLike": {
            "cognito-identity.amazonaws.com:amr": "authenticated"
          }
        }
      }
    ]
  }
}

function updateS3Objects(meta) {
  return {
    name: "UpdateS3Objects",
    document: {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Sid": "AllowListingOfUserFolder",
          "Action": [
            "s3:ListBucket"
          ],
          "Effect": "Allow",
          "Resource": [
            `arn:aws:s3:::${meta.bucket.content}`,
            `arn:aws:s3:::${meta.bucket.backup}`
          ],
          "Condition": {
            "StringLike": {
              "s3:prefix": [
                `${meta.name}/*`
              ]
            }
          }
        },
        {
          "Sid": "AllowAllS3ActionsInUserFolder",
          "Effect": "Allow",
          "Action": [
            "s3:*"
          ],
          "Resource": [
            `arn:aws:s3:::${meta.bucket.content}/${meta.name}/*`,
            `arn:aws:s3:::${meta.bucket.backup}/${meta.name}/*`
          ]
        },
        {
          "Sid": "AllowListingOfUserPackageFolder",
          "Action": [
            "s3:ListBucket"
          ],
          "Effect": "Allow",
          "Resource": [
            `arn:aws:s3:::${meta.bucket.package}`
          ],
          "Condition": {
            "StringLike": {
              "s3:prefix": [
                `customer/${meta.name}/*`
              ]
            }
          }
        },
        {
          "Sid": "AllowAllS3ActionsInUserPackageFolder",
          "Effect": "Allow",
          "Action": [
            "s3:*"
          ],
          "Resource": [
            `arn:aws:s3:::${meta.bucket.package}/customer/${meta.name}/*`
          ]
        }
      ]
    }
  }
}

function getInlinePolicies(meta) {
  let policies = [];
  
  policies.push(getAccessDeviceTable(meta));
  policies.push(getIotFullAccess(meta));
  policies.push(updateS3Objects(meta));
  
  return policies;
}

module.exports = {
  getTrustRelationship,
  getInlinePolicies
}
