'use strict'
let AWS = require('aws-sdk');
let appConfig = require('./config')

let dynamoDB = new AWS.DynamoDB();
let docClient = new AWS.DynamoDB.DocumentClient();

function createDeviceTable(deviceName) {
  let params = {
    AttributeDefinitions: [
      {
        AttributeName: 'name',
        AttributeType: 'S'
      }
    ],
    KeySchema: [
      {
        AttributeName: 'name',
        KeyType: 'HASH'
      }
    ],
    ProvisionedThroughput: {
      ReadCapacityUnits: 5,
      WriteCapacityUnits: 5
    },
    TableName: deviceName,
  }
  console.log('createDeviceTable param', params)

  return dynamoDB.createTable(params).promise()
}

function deleteCertificate(deviceName) {
  let params = {
    TableName: appConfig.tables.certificate,
    Key: {
      'name': deviceName
    }
  };

  return docClient.delete(params).promise();
}

function deleteTable(tableName) {
  let params = {
    TableName: tableName
  }
  console.log('deleteTable param', deleteTable)
  return dynamoDB.deleteTable(params).promise()
}

function getCertificate(deviceName) {
  let params = {
    TableName: appConfig.tables.certificate,
    Key: {
      name: deviceName
    }
  }
  console.log('getCertificate param', params)
  
  return docClient.get(params).promise()
  .then(data => data.Item);
}

function getCustomer(name) {
  let params = {
    TableName: appConfig.tables.customer,
    Key: {
      name
    }
  }
  console.log('getCustomer param', params)

  return docClient.get(params).promise()
  .then(data => data.Item)
}

function initDeviceTable(customer, deviceName) {
  let params = {
    RequestItems: {}
  }
  params.RequestItems[deviceName] = [];
  for (let key of Object.keys(appConfig.deviceItems)) {
    let name = appConfig.deviceItems[key].slice(1, appConfig.deviceItems[key].length - 1)
    let requestParam = {
      PutRequest: {
        Item: {
          'name': `_${name}_`,
          'value': customer[name] || []
        }
      }
    };
    params.RequestItems[deviceName].push(requestParam)
  }
  console.log('init params:', JSON.stringify(params))
  return docClient.batchWrite(params).promise()
}

function putCertificate(deviceName, certificate) {
  let params = {
    TableName: appConfig.tables.certificate,
    Item: {
      name: deviceName,
      certificate
    }
  }

  return docClient.put(params).promise()
}

// Initiate prefix metadata
function putPrefix(customerName, prefix) {
  let params = {
    TableName: appConfig.tables.customer,
    Key: {
      name: customerName
    },
    ConditionExpression: `attribute_exists(#p)`,
    UpdateExpression: `set #p = list_append(#p, :new)`,
    ExpressionAttributeNames: {
      '#p': appConfig.customerItems.prefixMeta
    },
    ExpressionAttributeValues: {
      ':new': [{
        name: prefix,
        lastindex: 0
      }]
    },
    ReturnValues: 'UPDATED_NEW'
  };

  return docClient.update(params).promise();
}

// Update user's lastindex for each prefix
function updatePrefixMeta(customer, prefix, index) {
  let params = {
    TableName: appConfig.tables.customer,
    Key: {
      name: customer.name
    },
    ConditionExpression: `#p[${index}].#name = :prefix`,
    UpdateExpression: `set #p[${index}].#lastindex = :lastindex`,
    ExpressionAttributeNames: {
      '#p': 'prefixmeta',
      '#name': 'name',
      '#lastindex': 'lastindex'
    },
    ExpressionAttributeValues: {
      ':prefix': prefix,
      ':lastindex': customer.prefixmeta[index].lastindex
    },
    ReturnValues: 'UPDATED_NEW'
  };
  console.log('updatePrefixMeta param', params)

  return docClient.update(params).promise()
}

function waitTableCreate(tableName) {
  let params = {
    TableName: tableName
  }

  return dynamoDB.waitFor('tableExists', params).promise()
}

module.exports = {
  createDeviceTable,
  deleteTable,
  deleteCertificate,
  getCertificate,
  getCustomer,
  initDeviceTable,
  putCertificate,
  putPrefix,
  updatePrefixMeta,
  waitTableCreate,
}