'use strict'
module.exports = {
  bucket: {
    backups: process.env.BUCKET_BACKUP, // 'nemosnow-backups'
  },
  tables: {
    customer: process.env.TABLE_CUSTOMER, //'Customer',
    certificate: process.env.TABLE_CERTIFICATE, //'Certificate',
  },
  customerItems: {
    prefixMeta: process.env.CUSTOMER_ITEM_PREFIX_META, //'prefixmeta'
  },
  deviceItems: {
    bigdata: process.env.DEVICE_ITEM_BIGDATA, //'_bigdata_',
    contentSync: process.env.DEVICE_ITEM_CONTENT_SYNC, //'_contentsync_',
    packageList: process.env.DEVICE_ITEM_PACKAGE_LIST, // '_pkglist_',
    packageIndex: process.env.DEVICE_ITEM_PACKAGE_INDEX, //'_pkgindex_',
    mandatoryPackages: process.env.DEVICE_ITEM_MANDATORY_PACKAGES, //'_mandatorypkgs_'
    power: process.env.DEVICE_ITEM_POWER, //'_power_'
    theme: process.env.DEVICE_ITEM_THEME, //'_theme_'
    shell: process.env.DEVICE_ITEM_SHELL, //'_shell_'
  },
  status: {
    initial: process.env.STATUS_INITIAL, // 'initial',
    deactivated: process.env.STATUS_DEACTIVATED, // 'deactivated',
    activated: process.env.STATUS_ACTIVATED, // 'activated', 
    waitConfirm: process.env.STATUS_WAIT_CONFIRM, // 'wait_confirm'
  },
  mode: {
    local: process.env.MODE_LOCAL, // 'local',
    cloud: process.env.MODE_CLOUD, // 'cloud'
  },
  policy: process.env.POLICY, //'NemoSnowBasicIoT',
  deviceCodeLength: process.env.DEVICE_CODE_LENGTH, // 6
}
