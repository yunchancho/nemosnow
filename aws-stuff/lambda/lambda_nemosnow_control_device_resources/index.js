'use strict'
let AWS = require('aws-sdk');
let iot = require('./iot')

exports.handler = (event, context, callback) => {
	console.log('clientContext: ', context.clientContext);
  let params = context.clientContext
	let prom;
	if (params.api === 'createDevices') {
		prom = iot.createDevices(params)
	} else if (params.api === 'deleteDevices') {
		prom = iot.deleteDevices(params)
	} else {
		console.log(params.api, ' called')
		return callback(new Error('ERROR_LAMBDA_CONTROL_DEVICE_INVALID_API'))
	}

	prom.then(result => {
    console.log('result: ', result)
    callback(null, result)
  })
	.catch(err => {
		console.error(err);
		callback(err)
	});
	return;
};
