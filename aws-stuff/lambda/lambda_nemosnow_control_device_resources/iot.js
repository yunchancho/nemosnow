'use strict'
let AWS = require('aws-sdk');
let appConfig = require('./config')
let dynamoDB = require('./dynamoDB')
let s3 = require('./s3')

let iot = new AWS.Iot();

function attachCertificate(thingName, certificate) {
  let params = {
    principal: certificate.certificateArn,
    thingName
  }

  return iot.attachThingPrincipal(params).promise()
}

// Attach principal(certificate) with policy
// Insert certificate arn to principal
function attachPrincipalPolicy(principal) {
  let params = {
    policyName: appConfig.policy,
    principal
  }

  return iot.attachPrincipalPolicy(params).promise()
}

function createCertificate(deviceName) {
  let params = {
    setAsActive: true
  }
  
  let principal = null;
  return iot.createKeysAndCertificate(params).promise()
  .then(data => {
    principal = data;
    return attachPrincipalPolicy(principal.certificateArn)
  })
  .then(() => dynamoDB.putCertificate(deviceName, principal))
  .then(() => principal)
}

// Create and init thing device and dynamoDB table
//
// 1. Create a AWS IoT thing
// 2. Create AWS IoT certificate(and attach policy) for the thing
// 3. Attach certificate to thing
// 4. Create DynamoDB device table
// 5. Notify creating is complete, and wait until DynamoDB table creating complete
// 5. Put initial variables to table according to customer data
function createDevice(customer, deviceName, mode) {
  console.log('create Device: ', customer, deviceName, mode)
  let thing = {
    thingName: deviceName,
    attributes: {
      status: appConfig.status.initial,
      mode,
      cloudAllowed: (mode === appConfig.mode.cloud)?'1':'0',
    },
    thingTypeName: 'wall'
  }
  
  return iot.listThings({}).promise()
  .then(result => {
    for (let  item of result.things) {
      if (thing.thingName === item.thingName) {
        throw new Error('ERROR_LAMBDA_DEVICE_RESOURCE_THING_ALREADY_EXIST')
      }
    }
  })
  .then(() => createThing(thing))
  .then(() => createCertificate(deviceName))
  .then(certificate => attachCertificate(thing.thingName, certificate))
  .then(() => dynamoDB.createDeviceTable(deviceName))
  .then(() => dynamoDB.waitTableCreate(deviceName))
  .then(() => dynamoDB.initDeviceTable(customer, deviceName))
  .then(() => {
    let key = `${customer.name}/${deviceName}/`;
    return s3.uploadBackupFile(key, 'void body')
  })
  .then(result => {
    console.log('Initiating device table complete: ', deviceName);
  })
  .catch(reason => {
    if (reason.message !== 'ERROR_LAMBDA_DEVICE_RESOURCE_THING_ALREADY_EXIST') {
      // If creating table fails, rollback created thing
      console.error('Create device fails', reason)
      deleteDevice(thing.thingName);
    }
    throw reason;
  })
}

// Create number of devices
// Device name is consisted of customerName_prefix_index
// Index starts from last index of prefix
//
// 1. Get customer information (include prefix's last index)
// 2. Create device
// 3. Set customer lastindex attributes
// TODO: Error: Subscriber limit exceeded: Only 10 tables can be created, updated, or deleted simultaneously
function createDevices(params) {
  let customerName = params.customerName;
  let prefix = params.prefix;
  let mode = params.mode;
  let number = params.number;
  
  let pad = (num, size) => {
    // Insert '0' in front of num until string length matches to size
    let str = '' + num;
    while(str.length < size) {
      str = '0' + str;
    }
    return str;
  }

  let findIndex = (customer) => {
    let index = -1;
    let prefixIndex = 0;
    for (; prefixIndex < customer.prefixmeta.length; prefixIndex++) {
      let meta = customer.prefixmeta[prefixIndex];
      if (meta.name === prefix) {
        index = meta.lastindex + 1;
        break;
      }
    }

    return [index, prefixIndex];
  }

  return new Promise((resolve, reject) => {
    if (number <= 0) {
      return reject(new Error('ERROR_LAMBDA_DEVICE_RESOURCE_WRONG_INPUT_NUMBER'));
    }

    let i = 1;
    let customer = {};
    let index = -1;
    let prefixIndex = 0;
    let newDevices = []

    // Get start index
    dynamoDB.getCustomer(customerName)
    .then(item => {
      // Starts creating devices and wait until finished
      customer = item;
      let result = findIndex(customer);
      index = result[0]
      prefixIndex = result[1]

      if (index === -1) {
        return initPrefix(customerName, prefix);
      } else {
        return false;
      }
    })
    .then(prefixmeta => {
      // Handle initPrefix return value
      // Skip sequence if prefix already initiated
      if (!prefixmeta) {
        return;
      }

      // Called after initPrefix() resolved
      customer.prefixmeta = prefixmeta;
      let result = findIndex(customer);
      index = result[0]
      prefixIndex = result[1]

      if (index === -1) {
        throw new Error('ERROR_LAMBDA_DEVICE_RESOURCE_INIT_INDEX_FAIL');
      }
    })
    .then(() => {
      console.log(`start position: ${index}`);
      
      // Create devices five by five
      let createDevicesOrder = (index, end) => {
        let requests = []
        let max = (index + 5 > end)?end:(index + 5);
        
        if (index === end) {
          return Promise.resolve(index);
        }
        
        for (let i = index; i < max; i++) {
          let swKey = `${customer.name}_${prefix}_${pad(i, appConfig.deviceCodeLength)}`
          newDevices.push(swKey)
          requests.push(createDevice(customer, swKey, mode))
        }
        
        return Promise.all(requests)
        .then(() => createDevicesOrder(max, end))
      }
      return createDevicesOrder(index, index + number);
      
      // Create Device sequencially
      // let list = []
      // for (let i = index; i < index + number; i++) {
      //   list.push(`${customer.name}_${prefix}_${pad(i, appConfig.deviceCodeLength)}`)
      // }
      // return list.reduce((previousValue, currentValue) => {
      //   return previousValue.then(() => {
      //     console.log(currentValue)
      //     return createDevice(customer, currentValue, mode)
      //   })
      // }, Promise.resolve())

    })
    .then(index => {
      // Update prefix's last index
      // TODO: Handle errors
      console.log(`last index: ${index - 1}`);
      customer.prefixmeta[prefixIndex].lastindex = index - 1;
    })
    .then(() => dynamoDB.updatePrefixMeta(customer, prefix, prefixIndex))
    .then(() => resolve(newDevices))
    .catch((err) => reject(err))
  });
}

// Create thing and dynamodb table
function createThing(thing) {
  let params = {
    thingName: thing.thingName,
    attributePayload: {
      attributes: thing.attributes
    },
    thingTypeName: thing.thingTypeName
  };

  // Doesn't check things have same names
  return iot.createThing(params).promise()
}

// Must detach things(and policy) and set status to INACTIVE before delete certificate
function deleteCertificate(deviceName, certificate) {
  let params = {}
  
  return Promise.resolve()
  .then(result => {
    params = {
      principal: certificate.certificateArn,
      thingName: deviceName
    }
    return iot.detachThingPrincipal(params).promise()
  })
  .then(result => {
    params = {
      policyName: appConfig.policy,
      principal: certificate.certificateArn
    }
    return iot.detachPrincipalPolicy(params).promise()
  })
  .then(result => {
    params = {
      certificateId: certificate.certificateId,
      newStatus: 'INACTIVE'
    }
    return iot.updateCertificate(params).promise()
  })
  .then(result => {
    params = {
      certificateId: certificate.certificateId
    }
    return iot.deleteCertificate(params).promise()
  })
}


// TODO: handle failure
// TODO: 400 error invoked
function deleteDevice(deviceName) {
  console.log('Delete device:', deviceName)
  return dynamoDB.getCertificate(deviceName)
  .then(data => {
    if (!data) {
      throw new Error('ERROR_LAMBDA_DEVICE_RESOURCE_NO_CERTIFICATE');
    }
    return data.certificate;
  })
  .then(certificate => deleteCertificate(deviceName, certificate))
  .then(() => dynamoDB.deleteCertificate(deviceName))
  .catch(err => console.error('deleteCertificate error', err))
  .then(() => deleteThing(deviceName))
  .then(() => dynamoDB.deleteTable(deviceName))
  .then(() => s3.deleteDeviceBackup(deviceName))
}

// TODO: Cross origin error if device length is bigger than 25
function deleteDevices(params) {
  // Delete devices ten by ten
  // step: index + 5 or end of list
  let deleteDevicesOrder = (index, list) => {
    let requests = []
    let step = (index + 5 > list.length)?list.length:(index + 5)
    
    if (index === list.length) {
      return Promise.resolve(index);
    }
    
    for (let i = index; i < step; i++) {
      requests.push(deleteDevice(list[i].thingName))
    }
    
    return Promise.all(requests)
    .then(() => deleteDevicesOrder(step, list))
  }
  
  let thingList = params.thingList
  return deleteDevicesOrder(0, thingList)
  
  // Delete things sequencially
  // let thingList = params.thingList
  // return thingList.reduce((previousValue, currentValue) => {
  //   return previousValue.then(() => deleteDevice(currentValue.thingName))
  // }, Promise.resolve())
}

// If specified thing name doesn't exist or deleting success, resolve empty object
function deleteThing(thingName) {
  let params = {
    thingName: thingName
  }
  return iot.deleteThing(params).promise()
}

// Return thing's information
// If specified thing name doesn't exist, reject 404 error
function describeThing(thingName) {
  let params = {
    thingName
  }
  return iot.describeThing(params).promise()
}

// TODO: Get Network Failure when describe multiple thing at once
function describeDevice(device) {
  let params = {
    thingName: device.name
  }
  return iot.describeThing(params).promise()
  .then(thing => {
    device.attributes = thing.attributes;
    device.thingTypeName = thing.thingTypeName;
    return device;
  })
}

function initPrefix(customerName, prefix) {
  // Resolve inserted prefix index
  return dynamoDB.putPrefix(customerName, prefix)
  .then((result) => result.Attributes.prefixmeta)
}

module.exports = {
	createDevices,
	deleteDevices
}
