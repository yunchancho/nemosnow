'use strict'
let AWS = require('aws-sdk');
let appConfig = require('./config')

let s3 = new AWS.S3();

function deleteDeviceBackup(deviceName) {
  let prefix = `${deviceName.slice(0, deviceName.indexOf('_'))}/${deviceName}/`
  let bucket = appConfig.bucket.backups
  return listObjects(bucket, prefix)
  .then(files => {
    let baseParams = {
      Bucket: bucket,
      Delete: {
        Objects: []
      }
    }
    let requests = []
    let params = JSON.parse(JSON.stringify(baseParams));
    // Devide delete request
    for (let i = 0; i < files.length; i++) {
      params.Delete.Objects.push({ Key: files[i].Key })
      if (i % 1000 === 999) {
        requests.push(s3.deleteObjects(params).promise())
        params = JSON.parse(JSON.stringify(baseParams));
      }
    }
    // push remain objects
    if (params.Delete.Objects.length !== 0) {
      requests.push(s3.deleteObjects(params).promise())
    }
    return Promise.all(requests)
  })
}

// List all objects in contents Bucket
// Maximum listing Count is 1000. call listing api recusively
function listObjects(bucket, prefix) {
  // Function declaration
  let recursiveCall = (list, ContinuationToken, prefix) => {
    let params = {
      Bucket: bucket,
      ContinuationToken,
      Prefix: prefix
    }

    return s3.listObjectsV2(params).promise()
    .then(result => {
      list = list.concat(result.Contents);

      if (result.KeyCount === 1000) {
        return recursiveCall(list, result.NextContinuationToken);
      } else {
        return list;
      }
    })
  }

  // Function call
  return recursiveCall([], null, prefix);
}

function uploadBackupFile(fileName, fileStream) {
  let params = {
    Bucket: appConfig.bucket.backups,
    Key: fileName,
    Body: fileStream
  }
  let options = {
    partSize: 10 * 1024 * 1024,
    queueSize: 1
  }

  return s3.upload(params, options).promise()
}

module.exports = {
  deleteDeviceBackup,
  uploadBackupFile,
}