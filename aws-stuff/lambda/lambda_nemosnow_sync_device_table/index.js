'use strict'
let Promise = require('bluebird');
let AWS = require('aws-sdk');
let _ = require('underscore')

const meta = {
	userPoolId: process.env.USER_POOL_ID,
	attributes: {
		customer: process.env.ATTRIBUTE_CUSTOMER,
	},
	iot: {
		attribute: {
			mode: {
				name: process.env.IOT_ATTRIBUTE_MODE_NAME,
				cloud: process.env.IOT_ATTRIBUTE_MODE_CLOUD,
			}	
		}
	}
}

let dynamoDB = Promise.promisifyAll(new AWS.DynamoDB());
let docClient = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient());
let cognitoProvider = Promise.promisifyAll(new AWS.CognitoIdentityServiceProvider());
let iot = Promise.promisifyAll(new AWS.Iot());

// List IoT things and return target devices include source device
function filterCloudDevices(customer) {
	let params = {
		maxResults: 100
	}
	return iot.listThingsAsync(params)
	.then(result => {
		return result.things.filter(thing => {
			return thing.thingName.startsWith(customer)
				&& thing.attributes.mode === meta.iot.attribute.mode.cloud;
		})
	})
}


function getCustomer(token) {
	let params = {
		AccessToken: token
	}

	return cognitoProvider.getUserAsync(params)
	.then(result => {
		params = {
			UserPoolId: meta.userPoolId,
			Username: result.Username
		};
		return cognitoProvider.adminGetUserAsync(params)
	})
	.then(result => {
		console.log('Access user: ', result);
		for (let attribute of result.UserAttributes) {
			if (attribute.Name === meta.attributes.customer) {
				return attribute.Value
			}
		}
		// Permission not assigned
		throw new Error('ERROR_LAMBDA_SYNC_DEVICE_CUSTOMER_NOT_ASSIGNED')
	})
}

// Make delete request by comparing target and source device table
function getDeleteRequest(device, source) {
	let params = {
		TableName: device
	}
	return docClient.scanAsync(params)
	.then(result => {
		let deleteRequests = result.Items.filter(item => {
			for (let src of source) {
				if (src.name === item.name) {
					return false;
				}
			}
			return true;
		})
		.map(item => {
			return {
				DeleteRequest: {
					Key: {
						'name': item.name
					}
				}
			}
		})

		return deleteRequests;
	})
}

// Request sync device tables with source device
function writeSyncRequest(devices, swKey) {
	let params = {
		TableName: swKey,
	}
	return docClient.scanAsync(params)
	.then(result => {
		let source = result.Items;
		let putRequests = source.map(item => {
			return {
				PutRequest: {
					Item: item
				}
			}
		})

		let requests = devices.map(device => {
			if (device === swKey) {
				// skip itself
				return Promise.resolve()
			}
			
			return getDeleteRequest(device, source)
			.then(deleteRequests => {
				let requestItems = deleteRequests.concat(putRequests)
				let limit = 25;
				let requests = []
				for (let i = 0; i < requestItems.length; i += limit) {
					let params = {
						RequestItems: {}
					}
					params.RequestItems[device] = requestItems.slice(i, i + limit)
					requests.push(docClient.batchWriteAsync(params))
				}

				console.log(`batchwrite request Items: ${JSON.stringify(requestItems)}`)
				return Promise.all(requests)
			})
			.then(result => console.log(`DB Write result: ${JSON.stringify(result)}`))
		})

		return Promise.all(requests)
	})
}

// From frontend
// 1. Get customer attribute of user
// 2. Get device list of customer
// 3. Check device is in cloud mode
// 4. Scan source device's table items
// 5. Put and delete items in target device tables
function syncDeviceTableByUser(token, swKey) {
	return getCustomer(token)
	.then(customer => {
		if (!customer.endsWith('_')) {
			customer = `${customer}_`
		}

		if (!swKey.startsWith(customer)) {
			throw new Error('ERROR_LAMBDA_SYNC_DEVICE_PERMISSION_DENIED')
		}

		return filterCloudDevices(customer)
	})
	.then(result => {
		// target device are included 
		let devices = result.map(thing => thing.thingName);
		console.log('target devices:', devices)

		if (devices.indexOf(swKey) === -1) {
			throw new Error('ERROR_LAMBDA_SYNC_DEVICE_NO_DEVICE_OR_IN_LOCAL_MODE')
		}
		
		return writeSyncRequest(devices, swKey)
	})
}

// From standalone backend
// 1. Get device list of customer
// 2. Check device is in cloud mode
// 3. Check swKey is matched with hwKey
// 4. Scan source device's table items
// 5. Put and delete items in target device tables
function syncDeviceTableByDevice(swKey, hwKey) {
	let customer = swKey.split('_')[0] + '_';
	if (!customer) {
		return Promise.reject(new Error('ERROR_LAMBDA_SYNC_DEVICE_WRONG_SW_KEY'))
	}

	return filterCloudDevices(customer)
	.then(things => {
		let devices = things.map(thing => thing.thingName);
		console.log('target devices:', devices)

		let targetIndex = devices.indexOf(swKey);
		if (targetIndex === -1) {
			throw new Error('ERROR_LAMBDA_SYNC_DEVICE_NO_DEVICE_OR_IN_LOCAL_MODE')
		}

		if (things[targetIndex].attributes.hwKey !== hwKey) {
			throw new Error('ERROR_LAMBDA_SYNC_DEVICE_WRONG_HW_KEY')
		}
		
		return writeSyncRequest(devices, swKey)
	})
}

function syncDeviceTable(param) {
  let swKey = param.swKey;
  let hwKey = param.hwKey;
  let accessToken = param.accessToken;

	if (!swKey) {
    return Promise.reject(new Error('ERROR_LAMBDA_SYNC_DEVICE_INVALID_PARAM'))
	} else if (accessToken) {
		return syncDeviceTableByUser(accessToken, swKey)
  } else if (hwKey) {
		return syncDeviceTableByDevice(swKey, hwKey)
  } else {
    return Promise.reject(new Error('ERROR_LAMBDA_SYNC_DEVICE_INVALID_PARAM'))
  }
}

function copyDeviceTableForNewDevices(param) {
  let swKeyPrefix = param.swKeyPrefix
  let newDevices = param.newDevices

	let customer = swKeyPrefix.split('_')[0] + '_';
	if (!customer) {
		return Promise.reject(new Error('ERROR_LAMBDA_COPY_DEVICE_TABLE_WRONG_SW_KEY_PREFIX'))
	}

	return filterCloudDevices(customer)
	.then(things => {
		let allDevices = things.map(thing => thing.thingName)
    console.log('allDevices: ', allDevices)
    let targetDevices = _.intersection(allDevices, newDevices)
    let existingDevices = _.difference(allDevices, newDevices)
		return writeSyncRequest(targetDevices, existingDevices[0])
	})
}

exports.handler = (event, context, callback) => {
	console.log('clientContext: ', context.clientContext);
	let prom;
  let param = context.clientContext;

	if (context.clientContext.api === 'syncDeviceTable') {
		prom = syncDeviceTable(param)
  } else if (context.clientContext.api === 'copyDeviceTableForNewDevices') {
		prom = copyDeviceTableForNewDevices(param)
	} else {
		console.log(context.clientContext.api, ' called')
		return callback(new Error('ERROR_LAMBDA_SYNC_DEVICE_INVALID_API'))
	}

	prom.then(result => callback(null, result))
	.catch(err => {
		console.error(err)
		if (err.message.startsWith('ERROR_LAMBDA_SYNC_DEVICE')) {
			callback(err)
		} else {
			callback(new Error('ERROR_LAMBDA_SYNC_DEVICE'))
		}
	});
	return;
};
