'use strict'
let AWS = require('aws-sdk');

const meta = {
	nonDeviceTables: process.env.NON_DEVICE_TABLES,
	userPoolId: process.env.USER_POOL_ID,
	managerUserPoolId: process.env.MANAGER_USER_POOL_ID,
	attributes: {
		customer: process.env.ATTRIBUTE_CUSTOMER,
		permission: process.env.ATTRIBUTE_PERMISSION,
		type: process.env.ATTRIBUTE_TYPE,
	},
	bucket: {
		backup: process.env.BUCKET_BACKUP
	},
	registryTable: process.env.REGISTRY_TABLE,
	registryInfra: process.env.REGISTRY_INFRA,
}

let iot = new AWS.Iot();
let cognitoProvider = new AWS.CognitoIdentityServiceProvider();
let docClient = new AWS.DynamoDB.DocumentClient();
let s3 = new AWS.S3();
let tableNames = meta.nonDeviceTables.split('/');

function getCognitoAttributes(token) {
	let params = {
		AccessToken: token
	}

	return cognitoProvider.getUser(params).promise()
	.then(result => {
		params = {
			UserPoolId: meta.userPoolId,
			Username: result.Username
		};
		// Call adminGetUser to get hidden(permission) attributes
		return cognitoProvider.adminGetUser(params).promise()
	})
	.then(result => {
		console.log('Access user: ', result);
		let attributes = {}
		for (let attribute of result.UserAttributes) {
			attributes[attribute.Name] = attribute.Value;
		}
		// Permission not assigned
		return attributes
	})
}

function listDevices(permission, includeLocal) {
	if (permission === '') {
		return Promise.resolve([]);
	}
	
	// TODO: handle exceed 250 things using nextToken
	let params = {
		maxResults: 250
	}
	return iot.listThings(params).promise()
	.then(data => {
		console.log('all things: ', JSON.stringify(data.things))
		return data.things.filter(thing => {
			let matches = thing.thingName.match(permission);
			if (matches !== null
			 && matches.length === 1
			 && thing.thingName.startsWith(matches[0])) {
				if (includeLocal || thing.attributes.mode === 'cloud') {
					return true;
				}
			}
			return false
		})
		.map(thing => thing.thingName);
	})
}

// list 2-depth folders has prefix
// ex) listIndex('packages', 'nemoux/') -> return folders in nemoux folder (in packages bucket)
function listS3Index(bucket, prefix) {
	return new Promise((resolve, reject) => {
		let params = {
			Bucket: bucket,
			Prefix: prefix,
			Delimiter: '/'
		}

		s3.listObjectsV2(params, (err, result) => {
			if (err) reject(err)
			else resolve(result)
		})
	})
	.then(data => {
		// Remove itself
		for (let i = 0; i < data.Contents.length; i++) {
			if (data.Contents[i].Key === prefix) {
				data.Contents.splice(i, 1);
			}
		}
		return data.CommonPrefixes;
	})
}

function listDeviceBackup(accessToken) {
	let permission;
	let customer;
	return getCognitoAttributes(accessToken)
	.then(result => {
		customer = result[meta.attributes.customer];
		permission = result[meta.attributes.permission];
		console.log(`customer: ${customer}`)
		console.log(`permission: ${permission}`);
		return listS3Index(meta.bucket.backup, `${customer}/`)
	})
	.then(indexList => {
		console.log(`Customer indexes: ${JSON.stringify(indexList)}`)
		let userList = indexList.filter((value) => {
			return !!value.Prefix.startsWith(`${customer}/`)
				&& !!value.Prefix.slice(customer.length).match(permission)
		})
		console.log(`result: ${JSON.stringify(userList)}`);
		return userList
	})
}

function listUserDevice(accessToken, manager) {
	if (manager) {
	  meta.userPoolId = meta.managerUserPoolId
	}
	let includeLocal = false;
	return getCognitoAttributes(accessToken)
	.then(attributes => {
		includeLocal = (attributes[meta.attributes.type] === 'installer')
		return listDevices(attributes[meta.attributes.permission], includeLocal)
	})
}

function listCommonMandatoryPackages() {
	let params = {
		TableName: meta.registryTable,
		Key: {
			name: meta.registryInfra
		}
	}
	return docClient.get(params).promise()
	.then(result => {
		console.log(`infra: ${JSON.stringify(result)}`)
		let mandatories = result.Item.infrapkgs.concat(result.Item.mandatorypkgs);
		console.log(`mandatory packages: ${JSON.stringify(mandatories)}`)
		return mandatories
	})
	.catch(err => {
		console.error(`list mandatory packages error: ${err.message}`, err)
		throw err
	})
}

exports.handler = (event, context, callback) => {
	console.log('clientContext: ', context.clientContext);
	let prom;

	if (context.clientContext.api === 'listDeviceBackup') {
		prom = listDeviceBackup(context.clientContext.accessToken, context.clientContext.manager)
	} else if (context.clientContext.api === 'listUserDevice') {
		prom = listUserDevice(context.clientContext.accessToken, context.clientContext.manager)
	} else if (context.clientContext.api === 'listCommonMandatoryPackages') {
		prom = listCommonMandatoryPackages(context.clientContext.accessToken)
	} else {
		console.log(context.clientContext.api, ' called')
		return callback(new Error('ERROR_LAMBDA_LIST_DATAS_INVALID_API'))
	}

	prom.then(result => callback(null, result))
	.catch(err => {
		console.error(err)
		if (err.message.startsWith('ERROR_LAMBDA_LIST_DATAS_')) {
			return callback(err)
		} else {
			callback(new Error('ERROR_LAMBDA_LIST_DATAS_INTERNAL_ERROR'))
		}
	});
	return;
};
