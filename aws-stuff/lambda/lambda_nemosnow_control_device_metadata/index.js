'use strict'

const co = require('co')
const AWS = require('aws-sdk')
const Promise = require('bluebird')

const iot = Promise.promisifyAll(new AWS.Iot())
const dynamodoc = Promise.promisifyAll(new AWS.DynamoDB.DocumentClient());

const initialStatus = 'initial'
const allowedStatuses = ['wait_confirm']
const verifiedStatuses = ['activated', 'deactivated'];
const checkStatusInterval = 1000
const maxCheckStatusCount = 180

function getDeviceStatus(deviceInfo, callback) {
	let coroutine = co.wrap(function* () {
		let params = {
			thingName: deviceInfo.swKey
		}
		let thing = yield iot.describeThingAsync(params)

		if (thing.attributes.hwKey !== deviceInfo.hwKey) {
			throw { code: 'ERROR_LAMBDA_CONTROL_DEVICE_INVALID_HW_KEY' }
		}

		return callback(null, { status: thing.attributes.status })
	})

	return coroutine()
}

function updateDeviceStatus(deviceInfo) {
	return co(function* () {
		
		if (!~allowedStatuses.indexOf(deviceInfo.status)) {
			throw { code: 'ERROR_LAMBDA_CONTROL_DEVICE_VERIFY_INVALID_STATUS' }
		}

		let params = {
			thingName: deviceInfo.swKey
		}
		let thing = yield iot.describeThingAsync(params)

		if (deviceInfo.hwKey !== thing.attributes.hwKey) {
			throw { code: 'ERROR_LAMBDA_CONTROL_DEVICE_VERIFY_INVALID_HW_KEY' }
		}

		let thingStatus = thing.attributes.status
		if (~verifiedStatuses.indexOf(thingStatus)) {
			throw { code: 'ERROR_LAMBDA_CONTROL_DEVICE_VERIFY_ALREADY_DONE' }
		}

		if (thingStatus === initialStatus) {
			let params = {
				thingName: deviceInfo.swKey,
				attributePayload: {
					attributes: {
						status: deviceInfo.status
					},
					merge: true
				}
			}
			yield iot.updateThingAsync(params)
		}
	})
}

function responseVerification(deviceInfo, callback) {
	let count = 0
	let interval = setInterval(() => {
		co(function* () {
			let params = {
				thingName: deviceInfo.swKey
			}
			let thing = yield iot.describeThingAsync(params)
			let thingStatus = thing.attributes.status

			console.log(`current thing status: ${thingStatus}`)
			if (~verifiedStatuses.indexOf(thingStatus)) {
				clearInterval(interval)
				console.log(`exit timer: ${thingStatus}`)
				let mode = thing.attributes.mode? thing.attributes.mode: 'local'
				callback(null, { status: thingStatus, mode: thing.attributes.mode })
			}

			if (maxCheckStatusCount < count++) {
				clearInterval(interval)
				callback({ code: 'ERROR_LAMBDA_CONTORL_DEVICE_VERIFY_TIMEOUT' })
			}
		})
	}, checkStatusInterval)
}

function getDeviceMode(deviceInfo, callback) {
	let coroutine = co.wrap(function* () {
		let params = {
			thingName: deviceInfo.swKey
		}
		let thing = yield iot.describeThingAsync(params)

		if (thing.attributes.hwKey !== deviceInfo.hwKey) {
			throw { code: 'ERROR_LAMBDA_CONTROL_DEVICE_INVALID_HW_KEY' }
		}

		return callback(null, { 
			mode: thing.attributes.mode, 
			cloudAllowed: thing.attributes.cloudAllowed
	 	})
	})

	return coroutine()
}

function switchDeviceMode(deviceInfo, callback) {
	let coroutine = co.wrap(function* () {
		let params = {
			thingName: deviceInfo.swKey
		}
		let thing = yield iot.describeThingAsync(params)

		if (thing.attributes.hwKey !== deviceInfo.hwKey) {
			throw { code: 'ERROR_LAMBDA_CONTROL_DEVICE_INVALID_HW_KEY' }
		}

		if (thing.attributes.mode === deviceInfo.mode) {
			throw { code: 'ERROR_LAMBDA_CONTORL_DEVICE_SET_MODE_ALREADY_DONE' }
		}

		if (deviceInfo.mode === 'cloud') {
			if (!parseInt(thing.attributes.cloudAllowed)) {
				throw { code: 'ERROR_LAMBDA_CONTORL_DEVICE_SET_MODE_NOT_ALLOWED' }
			}
		}

		params = {
			thingName: deviceInfo.swKey,
			attributePayload: {
				attributes: {
					mode: deviceInfo.mode
				},
				merge: true
			}
		}

		yield iot.updateThingAsync(params)
		return callback(null, { mode: deviceInfo.mode })
	})

	return coroutine()
}

function getAccessSecrets(deviceInfo, callback) {
	let coroutine = co.wrap(function* () {
		let thing = yield iot.describeThingAsync({ thingName: deviceInfo.swKey })
		if (thing.attributes.hwKey !== deviceInfo.hwKey) {
			throw { code: 'ERROR_LAMBDA_CONTROL_DEVICE_INVALID_HW_KEY' }
		}

		if (!~verifiedStatuses.indexOf(thing.attributes.status)) {
			throw { code: 'ERROR_LAMBDA_CONTROL_DEVICE_GET_ACCESS_SECRETS_INVALID_STATUS' }
		}

		/*
		 * TODO we should permit both of local and cloud mode 
		 * local mode device could request backup to cloud 
		if (thing.attributes.mode !== 'cloud') {
			throw { code: 'ERROR_LAMBDA_CONTROL_DEVICE_GET_ACCESS_SECRETS_INVALID_MODE' }
		}
		*/

		let params = {
			customer: {
				TableName: process.env.CUSTOMER_TABLE,
				Key: { 'name': deviceInfo.swKey.split(/[\_-]+/)[0] }
			}, 
			iot: {
				TableName: process.env.IOT_META_TABLE,
				Key: { 'name': deviceInfo.swKey }
			}, registry: {
				TableName: process.env.REGISTRY_TABLE,
				Key: { 'name': 'iot' }
			}
		}

		let accessSecrets = {}

		try {
			const customer = yield dynamodoc.getAsync(params.customer);
			const iot = yield dynamodoc.getAsync(params.iot);
			const registry = yield dynamodoc.getAsync(params.registry);

			// check this device is valid or not
			if (!Object.keys(customer).length || !Object.keys(iot).length) {
				throw `${deviceInfo.swKey} device has invalid data`;
			}

			let region = iot.Item.certificate.certificateArn.split(':')[3]
			let iotKeepAlive = registry.Item.keepAlive? registry.Item.keepAlive: process.env.IOT_DEFAULT_KEEPALIVE

			accessSecrets = {
				keyId: customer.Item.awskey.accessKeyId,
				accessKey: customer.Item.awskey.secretAccessKey,
				region: region,
				iot: {
					host: registry.Item.endpoints.filter(endpoint => endpoint.region === region )[0].host,
					region: region,
					privateKey: iot.Item.certificate.keyPair.PrivateKey,
					clientCert: iot.Item.certificate.certificatePem,
					rootCA: registry.Item.rootCA,
					keepAlive: iotKeepAlive 
				}
			}

			console.log(`accessSecrets: ${accessSecrets}`)
		} catch (err) {
			console.log(err)
			throw { code: 'ERROR_LAMBDA_CONTROL_DEVICE_GET_ACCESS_SECRETS' }
		}

		return callback(null, accessSecrets)
	})

	return coroutine()
}

exports.handler = (event, context, callback) => {
	co(function* () {
		console.log('args: ', context.clientContext)
		if (context.clientContext.api == 'getDeviceStatus') {
			yield getDeviceStatus(context.clientContext, callback)
		} else if (context.clientContext.api == 'verifyDeviceStatus') {
			yield updateDeviceStatus(context.clientContext)
			responseVerification(context.clientContext, callback)
		} else if (context.clientContext.api == 'getDeviceMode') {
			yield getDeviceMode(context.clientContext, callback)
		} else if (context.clientContext.api == 'switchDeviceMode') {
			yield switchDeviceMode(context.clientContext, callback)
		} else if (context.clientContext.api == 'getAccessSecrets') {
			yield getAccessSecrets(context.clientContext, callback)
		} else {
			console.log(context.clientContext.api, ' called')
			throw { code: 'ERROR_LAMBDA_CONTROL_DEVICE_INVALID_API' }
		}
	})
	.catch(err => {
		console.log(`err: ${JSON.stringify(err)}`)
		callback(new Error(JSON.stringify(err)))
	})
}
